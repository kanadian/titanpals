local L = LibStub( "AceLocale-3.0" ):NewLocale( "TitanPals", "ruRU" );
if ( not L ) then return end

L["TITAN_PALS_ABOUT"] = "Об аддоне"
L["TITAN_PALS_ABOUT_AUTHOR"] = "|cffffd700Автор :|r |cffff8c00 %s|r"
L["TITAN_PALS_ABOUT_CATEGORY"] = "|cffffd700Категория :|r |cffff8c00 %s|r"
L["TITAN_PALS_ABOUT_DESC"] = "Аддон для Titan Panel, Этот аддон отображает список ваших друзей, отсортированный по типу подключения на Titan Panel"
L["TITAN_PALS_ABOUT_EMAIL"] = "|cffffd700Email :|r |cffff8c00 %s|r"
L["TITAN_PALS_ABOUT_MEMORY"] = "|cffffd700Использование памяти :|r |cffffffff%skb|r"
L["TITAN_PALS_ABOUT_TRANSLATION"] = "|cffffd700Переводы :|r |cffff8c00 %s|r"
L["TITAN_PALS_ABOUT_VERSION"] = "|cffffd700Версия :|r |cff20ff20 %s|r"
L["TITAN_PALS_ABOUT_WEBSITE"] = "|cffffd700Вебсайт :|r |cff00ccff%s|r"
L["TITAN_PALS_ADDON_LABEL"] = "Titan [|cffeda55fPals|r]"
L["TITAN_PALS_ADDON_UPDATE"] = "Titan Pals был обновлён до %s!"
L["TITAN_PALS_ARRAY"] = "|cffeda55f%s|r исправлено!"
L["TITAN_PALS_ARRAY_FIXED"] = "TitanPals.|cffeda55f%s|r исправлено!"
L["TITAN_PALS_ARRAY_UPDATE"] = "Titan Pals Данные только что были обновлены до %s!"
L["TITAN_PALS_BUTTON_LABEL"] = "Друзья: "
L["TITAN_PALS_CONFIG_ADDAUTHOR"] = "|cff00ff00%s|r Добавлен в список Авторов"
L["TITAN_PALS_CONFIG_ALTS"] = "Альт"
L["TITAN_PALS_CONFIG_ALTS_DESC"] = "Это укажет альт статус "
L["TITAN_PALS_CONFIG_AWAY"] = "Отошёл"
L["TITAN_PALS_CONFIG_BANNER"] = "Отображать Load Banner"
L["TITAN_PALS_CONFIG_BANNER_DESC"] = "Показать информацию о версии аддона и использовании памяти во время загрузки аддона в чате."
L["TITAN_PALS_CONFIG_BANNER_FORMAT"] = "%s |cff00ff00%s|r Загружен, используется |cff00ff00%s|r памяти."
L["TITAN_PALS_CONFIG_BATTLENET"] = "Battlenet" -- Requires localization
L["TITAN_PALS_CONFIG_BUSY"] = "Занят"
L["TITAN_PALS_CONFIG_CLASS"] = "Класс"
L["TITAN_PALS_CONFIG_CLASS_DESC"] = "Установить класс этого персонажа"
L["TITAN_PALS_CONFIG_CUSTOM_REALID_TOOLTIP"] = "Добавить стиль |cff00ffffRealID|r подсказки:"
L["TITAN_PALS_CONFIG_CUSTOM_REALID_TOOLTIP_DESC"] = "Здесь вы можете добавить свои стили подсказки"
L["TITAN_PALS_CONFIG_CUSTOM_REALID_TOOLTIP_INST"] = [=[|cff20ff20Вы можете использовать :|r
     |cff20ff20!cn|r для имени персонажа
     |cff20ff20!fn|r для полного имени
     |cff20ff20!rn|r для названия сервера
     |cff20ff20!gn|r для Клиента Игры|r]=]
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP"] = "Добавить формат подсказки :"
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_DESC"] = "Здесь вы можете задать свой формат отображения подсказки"
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_INST"] = [=[|cff20ff20Вы можете использовать :|r
     |cff20ff20!s|r для Статуса
     |cff20ff20!l|r для Уровня
     |cff20ff20!p|r для Игрока
     |cff20ff20!z|r для Зоны
     |cff20ff20!n|r для Заметки
     |cff20ff20!c|r для Класса
     |cff20ff20!uc|r для Цветных Имён Игроков

|cffff9900Правая сторона должна быть отделена от левой через ~(тильду).|r]=]
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_REALID"] = "|cff00ffffRealID|r Формат подсказки :"
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_REALID_DESC"] = "Несколько |cff00ffffRealID|r предустановок для изменения имён |cff00ffffRealID|r друзей"
L["TITAN_PALS_CONFIG_DEBUG"] = "Отладка"
L["TITAN_PALS_CONFIG_DEBUG_DESC"] = "Разрешить режим отладки"
L["TITAN_PALS_CONFIG_DEBUG_EVENT"] = "События"
L["TITAN_PALS_CONFIG_DEBUG_EVENT_DESC"] = "Отображать событие в чате когда оно происходит"
L["TITAN_PALS_CONFIG_DEBUG_HEADER"] = "Отладка"
L["TITAN_PALS_CONFIG_DEBUG_HEADER_PRINT"] = "Вывести список Команд"
L["TITAN_PALS_CONFIG_DEBUG_LOGEVENT"] = "Записывать Лог"
L["TITAN_PALS_CONFIG_DEBUG_LOGEVENT_DESC"] = "Записывать события для просмотра в дальнейшем"
L["TITAN_PALS_CONFIG_DEBUG_PRINT_ALTS"] = "Список Альтов"
L["TITAN_PALS_CONFIG_DEBUG_PRINT_ALTS_DESC"] = "Это выведет список ваших Альтов в окно чата"
L["TITAN_PALS_CONFIG_DEBUG_PRINT_FRIENDS"] = "Список Друзей"
L["TITAN_PALS_CONFIG_DEBUG_PRINT_FRIENDS_DESC"] = "Это выведет список ваших Друзей в окно чата"
L["TITAN_PALS_CONFIG_DEBUG_REMAUTH"] = "Удалить Автора"
L["TITAN_PALS_CONFIG_DEBUG_REMAUTH_DESC"] = "Удаляет |cffff0000%s|r из Авторов Аддона"
L["TITAN_PALS_CONFIG_DIABLO"] = "Diablo III"
L["TITAN_PALS_CONFIG_DND"] = "Не Беспокоить"
L["TITAN_PALS_CONFIG_DNDLABEL"] = "Не Беспокоить"
L["TITAN_PALS_CONFIG_DND_TYPES"] = "Стиль для \"Занят\""
L["TITAN_PALS_CONFIG_DND_TYPES_DESC"] = "Стиль, который вы хотите чтобы отображался у друзей со статусом DND / Busy."
L["TITAN_PALS_CONFIG_DND_TYPES_INST"] = [=[|cff20ff20Это позволит вам изменить текст <DND> в тултипе, ваш выбор :|r
     |cffff8c00<Не беспокоить>|r
     |cffff8c00<Занят>|r
     |cffff8c00[Не беспокоить]|r
     |cffff8c00[Занят]|r
     |cffff8c00(Не беспокоить)|r
     |cffff8c00(Занят)|r]=]
L["TITAN_PALS_CONFIG_EDITCHAR"] = "Редактировать Персонаж"
L["TITAN_PALS_CONFIG_EDITCHAR_DESC"] = "Редактировать данные персонажа"
L["TITAN_PALS_CONFIG_FRIEND"] = "Персонаж"
L["TITAN_PALS_CONFIG_FRIEND_DESC"] = "Выбрать персонажа для редактирования"
L["TITAN_PALS_CONFIG_FRIENDEDIT"] = "Редактировать друга"
L["TITAN_PALS_CONFIG_HEADER_ADD"] = "Новые форматы"
L["TITAN_PALS_CONFIG_HEADER_REALID"] = "|cff00ffffRealID Формат|r"
L["TITAN_PALS_CONFIG_HEADER_REALID_ADD"] = "Новые |cff00ffffRealID|r стили"
L["TITAN_PALS_CONFIG_HEADER_REALID_REMOVE"] = "Удалить |cff00ffffRealID|r стили"
L["TITAN_PALS_CONFIG_HEADER_REMOVE"] = "Удалить формат подсказки"
L["TITAN_PALS_CONFIG_HEADER_SETTINGS"] = "Настройки"
L["TITAN_PALS_CONFIG_HEADER_STATUS"] = "Типы статусов"
L["TITAN_PALS_CONFIG_HEADER_TOOLTIP"] = "Форматы подсказок"
L["TITAN_PALS_CONFIG_HEARTHSTONE"] = "Hearthstone"
L["TITAN_PALS_CONFIG_LEVEL"] = "Уровень"
L["TITAN_PALS_CONFIG_LEVEL_DESC"] = "Установить уровень для этого персонажа"
L["TITAN_PALS_CONFIG_NO"] = "Нет"
L["TITAN_PALS_CONFIG_NOALTS"] = "Убрать альтов из Подсказки"
L["TITAN_PALS_CONFIG_NOALTS_DESC"] = "Это предотвратит отображение альтов на подсказке."
L["TITAN_PALS_CONFIG_NONE"] = "Никого"
L["TITAN_PALS_CONFIG_PRESETS"] = "Предустановки подсказок :"
L["TITAN_PALS_CONFIG_PRESETS_DESC"] = "Несколько различных форматов отображения подсказок"
L["TITAN_PALS_CONFIG_PRESETS_PREVIEW"] = [=[Для просмотра наведите мышкой на [|cffeda55fPals|r] кнопку.
]=]
L["TITAN_PALS_CONFIG_REALID_NOTE1_OFF"] = "|cffff2020Отключить|r Это отключит отображение |cff00ffffRealID Друзей|r на всплывающей подсказке."
L["TITAN_PALS_CONFIG_REALID_NOTE1_ON"] = "|cff20ff20Включить|r Это включит отображение |cff00ffffRealID Друзей|r на всплывающей подсказке."
L["TITAN_PALS_CONFIG_REALID_ONOFF"] = "Показать |cff00ffffRealID|r в подсказке"
L["TITAN_PALS_CONFIG_REALID_ONOFF_DESC"] = "Эта опция покажет друзей |cff00ffffRealID|r в подсказке."
L["TITAN_PALS_CONFIG_REALID_PRESETS"] = "|cff00ffffRealID|r Настроенные стили подсказки :"
L["TITAN_PALS_CONFIG_REALID_PRESETS_DESC"] = "Немного других стилей |cff00ffffRealID|r для части подсказки"
L["TITAN_PALS_CONFIG_REALID_REMOVE"] = "Удалить |cff00ffffRealID|r"
L["TITAN_PALS_CONFIG_REALID_REMOVE_DESC"] = "Это удалит выбранные |cff00ffffRealID|r стили из набора"
L["TITAN_PALS_CONFIG_REALID_REMOVE_PRESETS_DESC"] = "Выберите |cff00ffffRealID|r стиль для удаления!"
L["TITAN_PALS_CONFIG_REALID_WARNING"] = "Когда realID активирован, этот код заменяет !uc или !p часть кода подсказки"
L["TITAN_PALS_CONFIG_REMAUTHOR"] = "|cffff0000%s|r удалён из списка Авторов Аддона"
L["TITAN_PALS_CONFIG_REMOVE"] = "Удалить"
L["TITAN_PALS_CONFIG_REMOVE_DESC"] = "Это опция предназначена для удаления формата из списков форматов"
L["TITAN_PALS_CONFIG_REMOVE_PRESETS_DESC"] = "Выберите формат для удаления!"
L["TITAN_PALS_CONFIG_SELECT"] = "Выбрать"
L["TITAN_PALS_CONFIG_SHOWIGNORED"] = "Показывать игнорируемых"
L["TITAN_PALS_CONFIG_SHOWIGNORED_DESC"] = "Это будет отображать всех кого вы игнорировали в подсказке."
L["TITAN_PALS_CONFIG_SHOWMEM"] = "Показывать использование памяти"
L["TITAN_PALS_CONFIG_SHOWMEM_DESC"] = "Это покажет сколько памяти использует аддон."
L["TITAN_PALS_CONFIG_SHOWOFFLINE"] = "Показывать оффлайн друзей"
L["TITAN_PALS_CONFIG_SHOWOFFLINE_DESC"] = "Это позволит отобразить некоторую детализированную информацию о друзьях в подсказке"
L["TITAN_PALS_CONFIG_STARCRAFT"] = "Starcraft II"
L["TITAN_PALS_CONFIG_STATUS"] = "Выбрать Категорию"
L["TITAN_PALS_CONFIG_STATUS_DESC"] = "Выбрать категорию для редактирования"
L["TITAN_PALS_CONFIG_STATUS_TYPES"] = "Стиль отображения статуса"
L["TITAN_PALS_CONFIG_STATUS_TYPES_DESC"] = "Стиль отображения друзей, которые сейчас не за компьютером (AFK)"
L["TITAN_PALS_CONFIG_STATUS_TYPES_INST"] = [=[|cff20ff20Это позволит поменять стиль текста <Away> в подсказке на выбранный вами:|r
     |cffff8c00<Отошёл>|r
     |cffff8c00<AFK>|r
     |cffff8c00<A>|r
     |cffff8c00(Отошёл)|r
     |cffff8c00(AFK)|r
     |cffff8c00(A)|r
     |cffff8c00[Отошёл]|r
     |cffff8c00[AFK]|r
     |cffff8c00[A]|r]=]
L["TITAN_PALS_CONFIG_UNKNOWN"] = "Неизвестно"
L["TITAN_PALS_CONFIG_WARCRAFT"] = "World of Warcraft"
L["TITAN_PALS_CONFIG_YES"] = "Да"
L["TITAN_PALS_FACTION_ERROR"] = "Извините, %s из %s, вы только можете приглашать людей из %s."
L["TITAN_PALS_GAME_ERROR"] = "Извините %s играет %s, вы можете приглашать только людей, которые играют %s."
L["TITAN_PALS_HELP_ADDAUTH"] = "  |cffff0000/%s AddAuth|r - Добавить Автора Аддона в Базу Данных"
L["TITAN_PALS_HELP_CMD_UNKNOWN"] = "Неправильная команда!"
L["TITAN_PALS_HELP_ERROR"] = "Извините, но |cffff0000%s|r - некорректная команда!"
L["TITAN_PALS_HELP_LINE1"] = "Использование:"
L["TITAN_PALS_HELP_LINE2"] = "  |cff00ff00/%s help|r - Показывает это меню"
L["TITAN_PALS_HELP_LINE3"] = "  |cff00ff00/%s [ options | config ]|r - Открывает панель настроек"
L["TITAN_PALS_HELP_LINE4"] = "  |cff00ff00/%s reset|r - [ cmd ] [ pram ] Сбрасывает аддон по-умолчанию и перезагружает интерфейс"
L["TITAN_PALS_HELP_LINE5"] = "  |cff00ff00/%s checkDB|r - Проверяет базу данных на ошибки"
L["TITAN_PALS_HELP_REMAUTH"] = "  |cffff0000/%s RemAuth|r - Удалить Автора Аддона из Базы Данных"
L["TITAN_PALS_LOCALSARRAY_FIXED2"] = "TitanPalsLocals.|cffeda55f%s|r исправлены!"
L["TITAN_PALS_MENU_ALLIANCE"] = "Альянс"
L["TITAN_PALS_MENU_BN"] = "|cff00ffffBattlenet|r" -- Requires localization
L["TITAN_PALS_MENU_CHECKDB"] = "Проверить Базу Данных на ошибки"
L["TITAN_PALS_MENU_CHECKDB_FINSIHED"] = "Проверка Базы Данных завершена!"
L["TITAN_PALS_MENU_CLOSE"] = "Закрыть"
L["TITAN_PALS_MENU_D3"] = "|cff00ffffDiablo III|r"
L["TITAN_PALS_MENU_HIDE"] = "Hide Button" -- Requires localization
L["TITAN_PALS_MENU_HORDE"] = "Орда"
L["TITAN_PALS_MENU_INVITE"] = "Пригласить"
L["TITAN_PALS_MENU_NEUTRAL"] = "Нейтральный"
L["TITAN_PALS_MENU_NORMAL"] = "Друзья"
L["TITAN_PALS_MENU_NOTE"] = "Заметка"
L["TITAN_PALS_MENU_PLAYING"] = "|cff00ffff%s|r играет %s"
L["TITAN_PALS_MENU_REALID"] = "|cff00ffffRealID Друзья|r"
L["TITAN_PALS_MENU_REMOVE"] = "Удалить"
L["TITAN_PALS_MENU_REMOVE_ALT"] = "Удалить альта"
L["TITAN_PALS_MENU_REMOVE_ALTDATA"] = "Удалить альта из базы данных"
L["TITAN_PALS_MENU_REMOVEDATA"] = "Утилиты базы данных"
L["TITAN_PALS_MENU_REMOVE_FRIEND"] = "Удалить друга"
L["TITAN_PALS_MENU_REMOVE_FRIENDDATA"] = "Удалить друга из базы данных"
L["TITAN_PALS_MENU_S2"] = "|cff00ffffStarcraft II|r"
L["TITAN_PALS_MENU_SC2"] = "|cff00ffffStarcraft II|r"
L["TITAN_PALS_MENU_SETTINGS"] = "Настройки"
L["TITAN_PALS_MENU_SUB_REMOVE"] = "Удалить "
L["TITAN_PALS_MENU_SYNC"] = "Синхронизация"
L["TITAN_PALS_MENU_SYNCALTS"] = "Синхронизовать альтов в список друзей"
L["TITAN_PALS_MENU_SYNCALTSFRIENDS"] = "Синхронизовать альтов друзей в список друзей"
L["TITAN_PALS_MENU_TEXT"] = "Titan Pals"
L["TITAN_PALS_MENU_WHISPER"] = "Шепнуть"
L["TITAN_PALS_MENU_WOW"] = "|cff00ffffWorld of Warcraft|r"
L["TITAN_PALS_MENU_WTCG"] = "|cff00ffffHearthstone|r"
L["TITAN_PALS_REAL_TOOLTIP"] = "|cff00ffffRealID|r друзья"
L["TITAN_PALS_STATIC_ADDNOTE"] = "Пожалуйста, добавьте для друзей заметку о %s :"
L["TITAN_PALS_STATICARRAY_FIXED2"] = "TitanPalsStatisArray.|cffeda55f%s|r исправлено!"
L["TITAN_PALS_SYNC_ALTS"] = "Добавляю Alt %s в список друзей"
L["TITAN_PALS_SYNC_ALTS_LABEL"] = "Альт"
L["TITAN_PALS_SYNC_FINISHED"] = "Синхронизация %s закончена!"
L["TITAN_PALS_SYNC_FRIENDS"] = "Добавление %s в список друзей"
L["TITAN_PALS_SYNC_FRIENDS_LABEL"] = "Друзья Альта"
L["TITAN_PALS_TOOLTIP"] = "Друзья онлайн"
L["TITAN_PALS_TOOLTIP_BN"] = "BN" -- Requires localization
L["TITAN_PALS_TOOLTIP_D3"] = "D3"
L["TITAN_PALS_TOOLTIP_EMPTY"] = "Ваш список друзей пуст!"
L["TITAN_PALS_TOOLTIP_IGNORE"] = "Игнорируемые игроки"
L["TITAN_PALS_TOOLTIP_IGNORE_EMPTY"] = "  Ваш список игнорируемых пуст!"
L["TITAN_PALS_TOOLTIP_MEM"] = "Использование памяти"
L["TITAN_PALS_TOOLTIP_NO_OFF"] = " Нет друзей оффлайн"
L["TITAN_PALS_TOOLTIP_NOPALS"] = "  Нет друзей онлайн"
L["TITAN_PALS_TOOLTIP_NOREALID_FRIENDS"] = "У вас нет |cff00ffffДрузей RealID|r!!!"
L["TITAN_PALS_TOOLTIP_NOREALPALS"] = "  Нет друзей |cff00ffffRealID|r онлайн"
L["TITAN_PALS_TOOLTIP_OFF"] = "Друзья оффлайн"
L["TITAN_PALS_TOOLTIP_OFFLINE"] = "Оффлайн"
L["TITAN_PALS_TOOLTIP_OFFLINE_IGNORED"] = "Игнорируется"
L["TITAN_PALS_TOOLTIP_S2"] = "S2"
L["TITAN_PALS_TOOLTIP_SC2"] = "S2"
L["TITAN_PALS_TOOLTIP_WOW"] = "WoW"
L["TITAN_PALS_TOOLTIP_WTCG"] = "HS"
L["TITAN_PALS_TRIAL_ACCOUNT"] = [=[|cffff0000Эта функция отключена для триал аккаунтов,

Триал аккаунты не могут добавлять |r|cff00ffffRealID|r|cffff0000 Друзей.|r]=]
