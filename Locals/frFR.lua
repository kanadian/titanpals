local L = LibStub( "AceLocale-3.0" ):NewLocale( "TitanPals", "frFR" );
if ( not L ) then return; end

L["TITAN_PALS_ABOUT"] = "About" -- Requires localization
L["TITAN_PALS_ABOUT_AUTHOR"] = "|cffffd700Author :|r |cffff8c00 %s|r" -- Requires localization
L["TITAN_PALS_ABOUT_CATEGORY"] = "|cffffd700Category :|r |cffff8c00 %s|r" -- Requires localization
L["TITAN_PALS_ABOUT_DESC"] = "Addon for Titan Panel, This addon displays your friends list sorted by connection status on Titan Panel" -- Requires localization
L["TITAN_PALS_ABOUT_EMAIL"] = "|cffffd700Email :|r |cffff8c00 %s|r" -- Requires localization
L["TITAN_PALS_ABOUT_MEMORY"] = "|cffffd700Memory Usage :|r |cffffffff%skb|r" -- Requires localization
L["TITAN_PALS_ABOUT_TRANSLATION"] = "|cffffd700Translations :|r |cffff8c00 %s|r" -- Requires localization
L["TITAN_PALS_ABOUT_VERSION"] = "|cffffd700Version :|r |cff20ff20 r%s|r" -- Requires localization
L["TITAN_PALS_ABOUT_WEBSITE"] = "|cffffd700Website :|r |cff00ccff%s|r" -- Requires localization
L["TITAN_PALS_ADDON_LABEL"] = "Titan [|cffeda55fPals|r]"
L["TITAN_PALS_ADDON_UPDATE"] = "Titan Pals has just been updated to %s!" -- Requires localization
L["TITAN_PALS_ARRAY"] = "|cffeda55f%s|r fixed!" -- Requires localization
L["TITAN_PALS_ARRAY_FIXED"] = "TitanPals.|cffeda55f%s|r fixed!" -- Requires localization
L["TITAN_PALS_ARRAY_UPDATE"] = "Titan Pals Data Array has just been Updated to %s!" -- Requires localization
L["TITAN_PALS_BUTTON_LABEL"] = "Pals: "
L["TITAN_PALS_CONFIG_ADDAUTHOR"] = "|cff00ff00%s|r Has been added to the Author's list" -- Requires localization
L["TITAN_PALS_CONFIG_ALTS"] = "Alt" -- Requires localization
L["TITAN_PALS_CONFIG_ALTS_DESC"] = "This will set this character alt status" -- Requires localization
L["TITAN_PALS_CONFIG_AWAY"] = "Away" -- Requires localization
L["TITAN_PALS_CONFIG_BANNER"] = "Display Load Banner" -- Requires localization
L["TITAN_PALS_CONFIG_BANNER_DESC"] = "This will display addon information and memory usage when the addon is loaded in the chat frame." -- Requires localization
L["TITAN_PALS_CONFIG_BANNER_FORMAT"] = "%s |cff00ff00r%s|r Loaded, using |cff00ff00%s|r of memory." -- Requires localization
L["TITAN_PALS_CONFIG_BATTLENET"] = "Battlenet" -- Requires localization
L["TITAN_PALS_CONFIG_BUSY"] = "Busy" -- Requires localization
L["TITAN_PALS_CONFIG_CLASS"] = "Class" -- Requires localization
L["TITAN_PALS_CONFIG_CLASS_DESC"] = "This will set this characters class" -- Requires localization
L["TITAN_PALS_CONFIG_CUSTOM_REALID_TOOLTIP"] = "Add a |cff00ffffRealID|r Tooltip Format :"
L["TITAN_PALS_CONFIG_CUSTOM_REALID_TOOLTIP_DESC"] = "Here you can add custom formats for the tooltip display"
L["TITAN_PALS_CONFIG_CUSTOM_REALID_TOOLTIP_INST"] = [=[|cff20ff20You can use :|r
     |cff20ff20!cn|r for Character Name
     |cff20ff20!fn|r for Full Name
     |cff20ff20!sn|r for Sir Name
     |cff20ff20!ln|r for Last Name
     |cff20ff20!rn|r for Realm Name
     |cff20ff20!gn|r for Game Client|r]=]
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP"] = "Add a Tooltip Format :"
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_DESC"] = "Here you can add custom formats for the tooltip display"
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_INST"] = [=[|cff20ff20You can use :|r
     |cff20ff20!s|r for Status
     |cff20ff20!l|r for Level
     |cff20ff20!p|r for Player
     |cff20ff20!z|r for Zone
     |cff20ff20!n|r for Note
     |cff20ff20!c|r for Class
     |cff20ff20!uc|r for Colored Players Name

 |cffff9900The RIGHT side must be seperated from the LEFT with a ~ (tilda).|r]=]
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_REALID"] = "|cff00ffffRealID|r Tooltip Format :"
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_REALID_DESC"] = "A few |cff00ffffRealID|r presets for changeing the name format of |cff00ffffRealID Pals|r that show up in the tooltip."
L["TITAN_PALS_CONFIG_DEBUG"] = "|cff808080Debug Mode|r"
L["TITAN_PALS_CONFIG_DEBUG_DESC"] = "This enables debug mode"
L["TITAN_PALS_CONFIG_DEBUG_EVENT"] = "Events" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_EVENT_DESC"] = "Displays event as they happen in the chat frame" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_HEADER"] = "Debugging" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_HEADER_PRINT"] = "Print Functions" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_LOGEVENT"] = "Log Event" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_LOGEVENT_DESC"] = "This will log the events as they happen for later veiwing" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_PRINT_ALTS"] = "Print Alts" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_PRINT_ALTS_DESC"] = "This will print a list of your alts to the chat frame" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_PRINT_FRIENDS"] = "Print Friends" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_PRINT_FRIENDS_DESC"] = "This will print a list of your friends to the chat frame" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_REMAUTH"] = "Remove Author" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_REMAUTH_DESC"] = "Removes |cffff0000%s|r as a Addon Author" -- Requires localization
L["TITAN_PALS_CONFIG_DIABLO"] = "Diablo III" -- Requires localization
L["TITAN_PALS_CONFIG_DND"] = "DND" -- Requires localization
L["TITAN_PALS_CONFIG_DNDLABEL"] = "Do Not Disturb" -- Requires localization
L["TITAN_PALS_CONFIG_DND_TYPES"] = "Busy Display Style" -- Requires localization
L["TITAN_PALS_CONFIG_DND_TYPES_DESC"] = "The style that you would like Pals that are DND / Busy to show up as." -- Requires localization
L["TITAN_PALS_CONFIG_DND_TYPES_INST"] = [=[

|cff20ff20This allow you to change the default text of <DND> in the tooltip, your choices are :|r
     |cffff8c00<DND>|r
     |cffff8c00<Busy>|r
     |cffff8c00[DND]|r
     |cffff8c00[Busy]|r
     |cffff8c00(DND)|r
     |cffff8c00(Busy)|r]=] -- Requires localization
L["TITAN_PALS_CONFIG_EDITCHAR"] = "Edit Character" -- Requires localization
L["TITAN_PALS_CONFIG_EDITCHAR_DESC"] = "This will edit the characters info" -- Requires localization
L["TITAN_PALS_CONFIG_FRIEND"] = "Character" -- Requires localization
L["TITAN_PALS_CONFIG_FRIEND_DESC"] = "Select character to edit" -- Requires localization
L["TITAN_PALS_CONFIG_FRIENDEDIT"] = "Friend Edit" -- Requires localization
L["TITAN_PALS_CONFIG_HEADER_ADD"] = "Add a New Format"
L["TITAN_PALS_CONFIG_HEADER_REALID"] = "|cff00ffffRealID|r"
L["TITAN_PALS_CONFIG_HEADER_REALID_ADD"] = "New |cff00ffffRealID|r Formats"
L["TITAN_PALS_CONFIG_HEADER_REALID_REMOVE"] = "Remove |cff00ffffRealID|r Formats"
L["TITAN_PALS_CONFIG_HEADER_REMOVE"] = "Remove a Format"
L["TITAN_PALS_CONFIG_HEADER_SETTINGS"] = "Settings" -- Requires localization
L["TITAN_PALS_CONFIG_HEADER_STATUS"] = "Status Types" -- Requires localization
L["TITAN_PALS_CONFIG_HEADER_TOOLTIP"] = "Tooltip Format" -- Requires localization
L["TITAN_PALS_CONFIG_HEARTHSTONE"] = "Hearthstone" -- Requires localization
L["TITAN_PALS_CONFIG_LEVEL"] = "Level" -- Requires localization
L["TITAN_PALS_CONFIG_LEVEL_DESC"] = "This will set this characters level" -- Requires localization
L["TITAN_PALS_CONFIG_NO"] = "No" -- Requires localization
L["TITAN_PALS_CONFIG_NOALTS"] = "Supprimer les rerolls du tooltip"
L["TITAN_PALS_CONFIG_NOALTS_DESC"] = "Cela empêchera d'afficher les autres persos du compte sur le tooltip."
L["TITAN_PALS_CONFIG_NONE"] = "None" -- Requires localization
L["TITAN_PALS_CONFIG_PRESETS"] = "Tooltip Presets :"
L["TITAN_PALS_CONFIG_PRESETS_DESC"] = "A few diffrent preset format for the tooltip display"
L["TITAN_PALS_CONFIG_PRESETS_PREVIEW"] = "To preview, mouse over Titan [|cffeda55fPals|r] button."
L["TITAN_PALS_CONFIG_REALID_NOTE1_OFF"] = "|cffff2020Disabling|r This will prevent any |cff00ffffRealID Pals|r info from showing on the tooltip."
L["TITAN_PALS_CONFIG_REALID_NOTE1_ON"] = "|cff20ff20Enabling|r this will show |cff00ffffRealID Pals|r info in tooltip."
L["TITAN_PALS_CONFIG_REALID_ONOFF"] = "Show |cff00ffffRealID Pals|r in Tooltip"
L["TITAN_PALS_CONFIG_REALID_ONOFF_DESC"] = "This will show |cff00ffffRealID Pals|r info in tooltip."
L["TITAN_PALS_CONFIG_REALID_PRESETS"] = "|cff00ffffRealID|r Tooltip Presets :"
L["TITAN_PALS_CONFIG_REALID_PRESETS_DESC"] = "A few diffrent preset format for the |cff00ffffRealID|r part of the tooltip"
L["TITAN_PALS_CONFIG_REALID_REMOVE"] = "Remove |cff00ffffRealID|r"
L["TITAN_PALS_CONFIG_REALID_REMOVE_DESC"] = "This will remove the selected |cff00ffffRealID|r format from the presets tooltip table"
L["TITAN_PALS_CONFIG_REALID_REMOVE_PRESETS_DESC"] = "Please select a |cff00ffffRealID|r format to remove!"
L["TITAN_PALS_CONFIG_REALID_WARNING"] = "When realID is enabled this code replaces the !uc or !p part of the tooltip code" -- Requires localization
L["TITAN_PALS_CONFIG_REMAUTHOR"] = "|cffff0000%s|r Has been removed from the Author's list" -- Requires localization
L["TITAN_PALS_CONFIG_REMOVE"] = "Remove"
L["TITAN_PALS_CONFIG_REMOVE_DESC"] = "This will remove the selected format from the presets tooltip table"
L["TITAN_PALS_CONFIG_REMOVE_PRESETS_DESC"] = "Please select the format to remove!"
L["TITAN_PALS_CONFIG_SELECT"] = "Select" -- Requires localization
L["TITAN_PALS_CONFIG_SHOWIGNORED"] = "Afficher les ignorés"
L["TITAN_PALS_CONFIG_SHOWIGNORED_DESC"] = "Cela affichera tous les joueurs ignorés sur le tooltip."
L["TITAN_PALS_CONFIG_SHOWMEM"] = "Afficher l'utilisation mémoire"
L["TITAN_PALS_CONFIG_SHOWMEM_DESC"] = "This will show you the amount of memory that TitamPals is using on the Tooltip."
L["TITAN_PALS_CONFIG_SHOWOFFLINE"] = "Montre les amis hors ligne"
L["TITAN_PALS_CONFIG_SHOWOFFLINE_DESC"] = "Cela affichera des infos détaillées dans le tooltip sur vos amis."
L["TITAN_PALS_CONFIG_STARCRAFT"] = "Starcraft II" -- Requires localization
L["TITAN_PALS_CONFIG_STATUS"] = "Catagory Select" -- Requires localization
L["TITAN_PALS_CONFIG_STATUS_DESC"] = "Selects a catgory to edit" -- Requires localization
L["TITAN_PALS_CONFIG_STATUS_TYPES"] = "Status Display Style"
L["TITAN_PALS_CONFIG_STATUS_TYPES_DESC"] = "The style that you would like Pals that are away to show up as."
L["TITAN_PALS_CONFIG_STATUS_TYPES_INST"] = [=[
|cff20ff20This allow you to change the default text of <Away> in the tooltip, you choices are :|r
     |cffff8c00<Away>|r
     |cffff8c00<AFK>|r
     |cffff8c00<A>|r
     |cffff8c00(Away)|r
     |cffff8c00(AFK)|r
     |cffff8c00(A)|r
     |cffff8c00[Away]|r
     |cffff8c00[AFK]|r
     |cffff8c00[A]|r]=]
L["TITAN_PALS_CONFIG_UNKNOWN"] = "Unknown" -- Requires localization
L["TITAN_PALS_CONFIG_WARCRAFT"] = "World of Warcraft" -- Requires localization
L["TITAN_PALS_CONFIG_YES"] = "Yes" -- Requires localization
L["TITAN_PALS_FACTION_ERROR"] = "Sorry %s is from the %s, you can only invite people from the %s." -- Requires localization
L["TITAN_PALS_GAME_ERROR"] = "Sorry %s is playing %s, you can only invite people that are playing %s." -- Requires localization
L["TITAN_PALS_HELP_ADDAUTH"] = "  |cffff0000/%s AddAuth|r - Add a AddOn Author to the Data Array" -- Requires localization
L["TITAN_PALS_HELP_CMD_UNKNOWN"] = "Invalid Command!" -- Requires localization
L["TITAN_PALS_HELP_ERROR"] = "Sorry but |cffff0000%s|r is not a valid command!" -- Requires localization
L["TITAN_PALS_HELP_LINE1"] = "Usage:" -- Requires localization
L["TITAN_PALS_HELP_LINE2"] = "  |cff00ff00/%s help|r - Shows this menu" -- Requires localization
L["TITAN_PALS_HELP_LINE3"] = "  |cff00ff00/%s [ options | config ]|r - Opens options panel" -- Requires localization
L["TITAN_PALS_HELP_LINE4"] = "  |cff00ff00/%s reset|r - [ cmd ] [ pram ] Resets Utilities" -- Requires localization
L["TITAN_PALS_HELP_LINE5"] = "  |cff00ff00/%s checkDB|r - Checks Data Array for errors" -- Requires localization
L["TITAN_PALS_HELP_REMAUTH"] = "  |cffff0000/%s RemAuth|r - Remove a AddOn Author from the Data Array" -- Requires localization
L["TITAN_PALS_LOCALSARRAY_FIXED2"] = "TitanPalsLocals.|cffeda55f%s|r fixed!" -- Requires localization
L["TITAN_PALS_MENU_ALLIANCE"] = "Alliance" -- Requires localization
L["TITAN_PALS_MENU_BN"] = "|cff00ffffBattlenet|r" -- Requires localization
L["TITAN_PALS_MENU_CHECKDB"] = "Checking Data Array for errors" -- Requires localization
L["TITAN_PALS_MENU_CHECKDB_FINSIHED"] = "Data Array Check Complete!" -- Requires localization
L["TITAN_PALS_MENU_CLOSE"] = "Close Menu" -- Requires localization
L["TITAN_PALS_MENU_D3"] = "|cff00ffffDiablo III|r" -- Requires localization
L["TITAN_PALS_MENU_HIDE"] = "Hide Button" -- Requires localization
L["TITAN_PALS_MENU_HORDE"] = "Horde" -- Requires localization
L["TITAN_PALS_MENU_INVITE"] = "Inviter"
L["TITAN_PALS_MENU_NEUTRAL"] = "Neutral" -- Requires localization
L["TITAN_PALS_MENU_NORMAL"] = "Normal Friends"
L["TITAN_PALS_MENU_NOTE"] = "Note"
L["TITAN_PALS_MENU_PLAYING"] = "|cff00ffff%s|r is playing %s" -- Requires localization
L["TITAN_PALS_MENU_REALID"] = "|cff00ffffRealID Pals|r"
L["TITAN_PALS_MENU_REMOVE"] = "Supprimer"
L["TITAN_PALS_MENU_REMOVE_ALT"] = "Remove Alt"
L["TITAN_PALS_MENU_REMOVE_ALTDATA"] = "Remove Alt From Database"
L["TITAN_PALS_MENU_REMOVEDATA"] = "Database Utilitys"
L["TITAN_PALS_MENU_REMOVE_FRIEND"] = "Remove Friend"
L["TITAN_PALS_MENU_REMOVE_FRIENDDATA"] = "Remove Friend From Database"
L["TITAN_PALS_MENU_S2"] = "|cff00ffffStarcraft II|r" -- Requires localization
L["TITAN_PALS_MENU_SC2"] = "|cff00ffffStarcraft II|r" -- Requires localization
L["TITAN_PALS_MENU_SETTINGS"] = "Paramètres"
L["TITAN_PALS_MENU_SUB_REMOVE"] = "Remove "
L["TITAN_PALS_MENU_SYNC"] = "Synchronisation"
L["TITAN_PALS_MENU_SYNCALTS"] = "Sync Alts to Friends List"
L["TITAN_PALS_MENU_SYNCALTSFRIENDS"] = "Sync Alts Friends to Friends List"
L["TITAN_PALS_MENU_TEXT"] = "Titan Pals"
L["TITAN_PALS_MENU_WHISPER"] = "Chuchoter"
L["TITAN_PALS_MENU_WOW"] = "|cff00ffffWorld of Warcraft|r" -- Requires localization
L["TITAN_PALS_MENU_WTCG"] = "|cff00ffffHearthstone|r" -- Requires localization
L["TITAN_PALS_REAL_TOOLTIP"] = "|cff00ffffRealID|r Pals Info"
L["TITAN_PALS_STATIC_ADDNOTE"] = "Please enter friends note for %s :" -- Requires localization
L["TITAN_PALS_STATICARRAY_FIXED2"] = "TitanPalsStatisArray.|cffeda55f%s|r fixed!" -- Requires localization
L["TITAN_PALS_SYNC_ALTS"] = "Adding Alt %s to firends list" -- Requires localization
L["TITAN_PALS_SYNC_ALTS_LABEL"] = "Alt's" -- Requires localization
L["TITAN_PALS_SYNC_FINISHED"] = "Sync %s finished!" -- Requires localization
L["TITAN_PALS_SYNC_FRIENDS"] = "Adding %s to friends list" -- Requires localization
L["TITAN_PALS_SYNC_FRIENDS_LABEL"] = "Alt's Friends" -- Requires localization
L["TITAN_PALS_TOOLTIP"] = "Online Pals Info"
L["TITAN_PALS_TOOLTIP_BN"] = "BN" -- Requires localization
L["TITAN_PALS_TOOLTIP_D3"] = "D3" -- Requires localization
L["TITAN_PALS_TOOLTIP_EMPTY"] = "Your Pals List Is Currently Empty!!!"
L["TITAN_PALS_TOOLTIP_IGNORE"] = "Currently Ignored Info"
L["TITAN_PALS_TOOLTIP_IGNORE_EMPTY"] = "  Your ignore list is currently empty."
L["TITAN_PALS_TOOLTIP_MEM"] = "Utilisation de la mémoire"
L["TITAN_PALS_TOOLTIP_NO_OFF"] = "  No Offline Pals"
L["TITAN_PALS_TOOLTIP_NOPALS"] = "Pas d'amis connectés"
L["TITAN_PALS_TOOLTIP_NOREALID_FRIENDS"] = "You have no |cff00ffffRealID Pals|r!!!"
L["TITAN_PALS_TOOLTIP_NOREALPALS"] = "  No |cff00ffffRealID|r Pals Currently Online"
L["TITAN_PALS_TOOLTIP_OFF"] = "Info amis hors-ligne"
L["TITAN_PALS_TOOLTIP_OFFLINE"] = "Hors-ligne"
L["TITAN_PALS_TOOLTIP_OFFLINE_IGNORED"] = "<Ignored>"
L["TITAN_PALS_TOOLTIP_S2"] = "S2" -- Requires localization
L["TITAN_PALS_TOOLTIP_SC2"] = "S2" -- Requires localization
L["TITAN_PALS_TOOLTIP_WOW"] = "WoW" -- Requires localization
L["TITAN_PALS_TOOLTIP_WTCG"] = "HS" -- Requires localization
L["TITAN_PALS_TRIAL_ACCOUNT"] = [=[|cffff0000This feather is disabled on trial accounts,

Trial accounts can not add |r|cff00ffffRealID|r|cffff0000 Friends.|r]=] -- Requires localization
