local L = LibStub( "AceLocale-3.0" ):NewLocale( "TitanPals", "esES" );
if ( not L ) then return end

L["TITAN_PALS_ABOUT"] = "About" -- Needs review
L["TITAN_PALS_ABOUT_AUTHOR"] = "|cffffd700Author :|r |cffff8c00 %s|r" -- Needs review
L["TITAN_PALS_ABOUT_CATEGORY"] = "|cffffd700Category :|r |cffff8c00 %s|r" -- Needs review
L["TITAN_PALS_ABOUT_DESC"] = "Addon for Titan Panel, This addon displays your friends list sorted by connection status on Titan Panel" -- Needs review
L["TITAN_PALS_ABOUT_EMAIL"] = "|cffffd700Email :|r |cffff8c00 %s|r" -- Needs review
L["TITAN_PALS_ABOUT_MEMORY"] = "|cffffd700Memory Usage :|r |cffffffff%skb|r" -- Needs review
L["TITAN_PALS_ABOUT_TRANSLATION"] = "|cffffd700Translations :|r |cffff8c00 %s|r" -- Needs review
L["TITAN_PALS_ABOUT_VERSION"] = "|cffffd700Version :|r |cff20ff20 r%s|r" -- Needs review
L["TITAN_PALS_ABOUT_WEBSITE"] = "|cffffd700Website :|r |cff00ccff%s|r" -- Needs review
L["TITAN_PALS_ADDON_LABEL"] = "Titan [|cffeda55fPals|r]"
L["TITAN_PALS_ADDON_UPDATE"] = "Titan Pals has just been updated to %s!" -- Needs review
L["TITAN_PALS_ARRAY"] = "%s fixed!" -- Needs review
L["TITAN_PALS_ARRAY_FIXED"] = "TitanPals[|cffeda55f\"%s\"|r] fixed!" -- Needs review
L["TITAN_PALS_ARRAY_UPDATE"] = "Titan Pals Data Array has just been Updated to %s!" -- Needs review
L["TITAN_PALS_BUTTON_LABEL"] = "Pals : " -- Needs review
L["TITAN_PALS_CONFIG_ADDAUTHOR"] = "|cff00ff00%s|r Has been added to the Author's list" -- Requires localization
L["TITAN_PALS_CONFIG_ALTS"] = "Alt" -- Needs review
L["TITAN_PALS_CONFIG_ALTS_DESC"] = "This will set this character alt status" -- Needs review
L["TITAN_PALS_CONFIG_AWAY"] = "Away" -- Needs review
L["TITAN_PALS_CONFIG_BANNER"] = "Display Load Banner" -- Requires localization
L["TITAN_PALS_CONFIG_BANNER_DESC"] = "This will display addon information and memory usage when the addon is loaded in the chat frame." -- Requires localization
L["TITAN_PALS_CONFIG_BANNER_FORMAT"] = "%s |cff00ff00r%s|r Loaded, using |cff00ff00%s|r of memory." -- Requires localization
L["TITAN_PALS_CONFIG_BATTLENET"] = "Battlenet" -- Requires localization
L["TITAN_PALS_CONFIG_BUSY"] = "Busy" -- Needs review
L["TITAN_PALS_CONFIG_CLASS"] = "Class" -- Needs review
L["TITAN_PALS_CONFIG_CLASS_DESC"] = "This will set this characters class" -- Needs review
L["TITAN_PALS_CONFIG_CUSTOM_REALID_TOOLTIP"] = "Add a |cff00ffffRealID|r Tooltip Format :" -- Needs review
L["TITAN_PALS_CONFIG_CUSTOM_REALID_TOOLTIP_DESC"] = "Here you can add custom formats for the tooltip display" -- Needs review
L["TITAN_PALS_CONFIG_CUSTOM_REALID_TOOLTIP_INST"] = [=[|cff20ff20You can use :|r
     |cff20ff20!cn|r for Character Name
     |cff20ff20!fn|r for Full Name
     |cff20ff20!sn|r for Sir Name
     |cff20ff20!ln|r for Last Name
     |cff20ff20!rn|r for Realm Name
     |cff20ff20!gn|r for Game Client|r]=] -- Needs review
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP"] = "Add a Tooltip Format :" -- Needs review
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_DESC"] = "Here you can add custom formats for the tooltip display" -- Needs review
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_INST"] = [=[|cff20ff20You can use :|r
     |cff20ff20!s|r for Status
     |cff20ff20!l|r for Level
     |cff20ff20!p|r for Player
     |cff20ff20!z|r for Zone
     |cff20ff20!n|r for Note
     |cff20ff20!c|r for Class
     |cff20ff20!uc|r for Colored Players Name

 |cffff9900The RIGHT side must be seperated from the LEFT with a ~ (tilda).|r]=] -- Needs review
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_REALID"] = "|cff00ffffRealID|r Tooltip Format :" -- Needs review
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_REALID_DESC"] = "A few |cff00ffffRealID|r presets for changeing the name format of |cff00ffffRealID Pals|r that show up in the tooltip." -- Needs review
L["TITAN_PALS_CONFIG_DEBUG"] = "Debug" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_DESC"] = "Enables Debug Mode" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_EVENT"] = "Events" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_EVENT_DESC"] = "Displays event as they happen in the chat frame" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_HEADER"] = "Debugging" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_HEADER_PRINT"] = "Print Functions" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_LOGEVENT"] = "Log Event" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_LOGEVENT_DESC"] = "This will log the events as they happen for later veiwing" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_PRINT_ALTS"] = "Print Alts" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_PRINT_ALTS_DESC"] = "This will print a list of your alts to the chat frame" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_PRINT_FRIENDS"] = "Print Friends" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_PRINT_FRIENDS_DESC"] = "This will print a list of your friends to the chat frame" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_REMAUTH"] = "Remove Author" -- Requires localization
L["TITAN_PALS_CONFIG_DEBUG_REMAUTH_DESC"] = "Removes |cffff0000%s|r as a Addon Author" -- Requires localization
L["TITAN_PALS_CONFIG_DIABLO"] = "Diablo III" -- Requires localization
L["TITAN_PALS_CONFIG_DND"] = "DND" -- Needs review
L["TITAN_PALS_CONFIG_DNDLABEL"] = "Do Not Disturb" -- Needs review
L["TITAN_PALS_CONFIG_DND_TYPES"] = "Busy Display Style" -- Needs review
L["TITAN_PALS_CONFIG_DND_TYPES_DESC"] = "The style that you would like Pals that are DND / Busy to show up as." -- Needs review
L["TITAN_PALS_CONFIG_DND_TYPES_INST"] = [=[
|cff20ff20This allow you to change the default text of <DND> in the tooltip, your choices are :|r
     |cffff8c00<DND>|r
     |cffff8c00<Busy>|r
     |cffff8c00[DND]|r
     |cffff8c00[Busy]|r
     |cffff8c00(DND)|r
     |cffff8c00(Busy)|r]=] -- Needs review
L["TITAN_PALS_CONFIG_EDITCHAR"] = "Edit Character" -- Needs review
L["TITAN_PALS_CONFIG_EDITCHAR_DESC"] = "This will edit the characters info" -- Needs review
L["TITAN_PALS_CONFIG_FRIEND"] = "Character" -- Needs review
L["TITAN_PALS_CONFIG_FRIEND_DESC"] = "Select character to edit" -- Needs review
L["TITAN_PALS_CONFIG_FRIENDEDIT"] = "Friend Edit" -- Needs review
L["TITAN_PALS_CONFIG_HEADER_ADD"] = "New Formats" -- Needs review
L["TITAN_PALS_CONFIG_HEADER_REALID"] = "|cff00ffffRealID|r" -- Needs review
L["TITAN_PALS_CONFIG_HEADER_REALID_ADD"] = "New |cff00ffffRealID|r Formats" -- Needs review
L["TITAN_PALS_CONFIG_HEADER_REALID_REMOVE"] = "Remove |cff00ffffRealID|r Formats" -- Needs review
L["TITAN_PALS_CONFIG_HEADER_REMOVE"] = "Remove Formats" -- Needs review
L["TITAN_PALS_CONFIG_HEADER_SETTINGS"] = "Settings" -- Needs review
L["TITAN_PALS_CONFIG_HEADER_STATUS"] = "Status Types" -- Needs review
L["TITAN_PALS_CONFIG_HEADER_TOOLTIP"] = "Tooltip Format" -- Needs review
L["TITAN_PALS_CONFIG_HEARTHSTONE"] = "Hearthstone" -- Requires localization
L["TITAN_PALS_CONFIG_LEVEL"] = "Level" -- Needs review
L["TITAN_PALS_CONFIG_LEVEL_DESC"] = "This will set this characters level" -- Needs review
L["TITAN_PALS_CONFIG_NO"] = "No" -- Needs review
L["TITAN_PALS_CONFIG_NOALTS"] = "Remove Alts from ToolTip" -- Needs review
L["TITAN_PALS_CONFIG_NOALTS_DESC"] = "This will keep alts from showing up on the tooltip." -- Needs review
L["TITAN_PALS_CONFIG_NONE"] = "None" -- Needs review
L["TITAN_PALS_CONFIG_PRESETS"] = "Tooltip Presets :" -- Needs review
L["TITAN_PALS_CONFIG_PRESETS_DESC"] = "A few diffrent preset format for the tooltip display" -- Needs review
L["TITAN_PALS_CONFIG_PRESETS_PREVIEW"] = "To preview, mouse over Titan [|cffeda55fPals|r] button." -- Needs review
L["TITAN_PALS_CONFIG_REALID_NOTE1_OFF"] = "|cffff2020Disabling|r This will prevent any |cff00ffffRealID Pals|r info from showing on the tooltip." -- Needs review
L["TITAN_PALS_CONFIG_REALID_NOTE1_ON"] = "|cff20ff20Enabling|r this will show |cff00ffffRealID Pals|r info in tooltip." -- Needs review
L["TITAN_PALS_CONFIG_REALID_ONOFF"] = "Show |cff00ffffRealID Pals|r in Tooltip" -- Needs review
L["TITAN_PALS_CONFIG_REALID_ONOFF_DESC"] = "This will show |cff00ffffRealID Pals|r info in tooltip." -- Needs review
L["TITAN_PALS_CONFIG_REALID_PRESETS"] = "|cff00ffffRealID|r Tooltip Presets :" -- Needs review
L["TITAN_PALS_CONFIG_REALID_PRESETS_DESC"] = "A few diffrent preset format for the |cff00ffffRealID|r part of the tooltip" -- Needs review
L["TITAN_PALS_CONFIG_REALID_REMOVE"] = "Remove |cff00ffffRealID|r" -- Needs review
L["TITAN_PALS_CONFIG_REALID_REMOVE_DESC"] = "This will remove the selected |cff00ffffRealID|r format from the presets tooltip table" -- Needs review
L["TITAN_PALS_CONFIG_REALID_REMOVE_PRESETS_DESC"] = "Please select a |cff00ffffRealID|r format to remove!" -- Needs review
L["TITAN_PALS_CONFIG_REALID_WARNING"] = "When realID is enabled this code replaces the !uc or !p part of the tooltip code" -- Requires localization
L["TITAN_PALS_CONFIG_REMAUTHOR"] = "|cffff0000%s|r Has been removed from the Author's list" -- Requires localization
L["TITAN_PALS_CONFIG_REMOVE"] = "Remove" -- Needs review
L["TITAN_PALS_CONFIG_REMOVE_DESC"] = "This will remove the selected format from the presets tooltip table" -- Needs review
L["TITAN_PALS_CONFIG_REMOVE_PRESETS_DESC"] = "Please select the format to remove!" -- Needs review
L["TITAN_PALS_CONFIG_SELECT"] = "Select" -- Needs review
L["TITAN_PALS_CONFIG_SHOWIGNORED"] = "Show Currently Ignored" -- Needs review
L["TITAN_PALS_CONFIG_SHOWIGNORED_DESC"] = "This will show anyone you might have Ignored on the tooltip." -- Needs review
L["TITAN_PALS_CONFIG_SHOWMEM"] = "Show Memory Usage" -- Needs review
L["TITAN_PALS_CONFIG_SHOWMEM_DESC"] = "This will show you the amount of memory that TitamPals is using on the Tooltip." -- Needs review
L["TITAN_PALS_CONFIG_SHOWOFFLINE"] = "Show Offline Pals" -- Needs review
L["TITAN_PALS_CONFIG_SHOWOFFLINE_DESC"] = "This will show some detailed info on the tooltip about your friends." -- Needs review
L["TITAN_PALS_CONFIG_STARCRAFT"] = "Starcraft II" -- Requires localization
L["TITAN_PALS_CONFIG_STATUS"] = "Catagory Select" -- Needs review
L["TITAN_PALS_CONFIG_STATUS_DESC"] = "Selects a catgory to edit" -- Needs review
L["TITAN_PALS_CONFIG_STATUS_TYPES"] = "Status Display Style" -- Needs review
L["TITAN_PALS_CONFIG_STATUS_TYPES_DESC"] = "The style that you would like Pals that are away to show up as." -- Needs review
L["TITAN_PALS_CONFIG_STATUS_TYPES_INST"] = [=[
|cff20ff20This allow you to change the default text of <Away> in the tooltip, your choices are :|r
     |cffff8c00<Away>|r
     |cffff8c00<AFK>|r
     |cffff8c00<A>|r
     |cffff8c00(Away)|r
     |cffff8c00(AFK)|r
     |cffff8c00(A)|r
     |cffff8c00[Away]|r
     |cffff8c00[AFK]|r
     |cffff8c00[A]|r]=] -- Needs review
L["TITAN_PALS_CONFIG_UNKNOWN"] = "Unknown" -- Needs review
L["TITAN_PALS_CONFIG_WARCRAFT"] = "World of Warcraft" -- Requires localization
L["TITAN_PALS_CONFIG_YES"] = "Yes" -- Needs review
L["TITAN_PALS_FACTION_ERROR"] = "Sorry %s is from the %s, you can only invite people from the %s." -- Needs review
L["TITAN_PALS_GAME_ERROR"] = "Sorry %s is playing %s, you can only invite people that are playing %s." -- Requires localization
L["TITAN_PALS_HELP_ADDAUTH"] = "  |cffff0000/%s AddAuth|r - Add a AddOn Author to the Data Array" -- Requires localization
L["TITAN_PALS_HELP_CMD_UNKNOWN"] = "Invalid Command!" -- Requires localization
L["TITAN_PALS_HELP_ERROR"] = "Sorry but |cffff0000%s|r is not a valid command!" -- Needs review
L["TITAN_PALS_HELP_LINE1"] = "Usage:" -- Needs review
L["TITAN_PALS_HELP_LINE2"] = "  |cff00ff00/%s help|r - Shows this menu" -- Needs review
L["TITAN_PALS_HELP_LINE3"] = "  |cff00ff00/%s [ options | config ]|r - Opens options panel" -- Needs review
L["TITAN_PALS_HELP_LINE4"] = "  |cff00ff00/%s reset|r - Resets addon to defaults and reloads interface" -- Needs review
L["TITAN_PALS_HELP_LINE5"] = "  |cff00ff00/%s checkDB|r - Checks Data Array for errors" -- Needs review
L["TITAN_PALS_HELP_REMAUTH"] = "  |cffff0000/%s RemAuth|r - Remove a AddOn Author from the Data Array" -- Requires localization
L["TITAN_PALS_LOCALSARRAY_FIXED2"] = "TitanPalsLocals.|cffeda55f%s|r fixed!" -- Requires localization
L["TITAN_PALS_MENU_ALLIANCE"] = "|cff4d4dffAlliance|r" -- Needs review
L["TITAN_PALS_MENU_BN"] = "|cff00ffffBattlenet|r" -- Requires localization
L["TITAN_PALS_MENU_CHECKDB"] = "Checking Data Array for errors" -- Needs review
L["TITAN_PALS_MENU_CHECKDB_FINSIHED"] = "Data Array Check Complete!" -- Needs review
L["TITAN_PALS_MENU_CLOSE"] = "Close" -- Needs review
L["TITAN_PALS_MENU_D3"] = "|cff00ffffDiablo III|r" -- Requires localization
L["TITAN_PALS_MENU_HIDE"] = "Hide Button" -- Requires localization
L["TITAN_PALS_MENU_HORDE"] = "|cffff4d4dHorde|r" -- Needs review
L["TITAN_PALS_MENU_INVITE"] = "Invite" -- Needs review
L["TITAN_PALS_MENU_NEUTRAL"] = "Neutral" -- Requires localization
L["TITAN_PALS_MENU_NORMAL"] = "Normal Friends" -- Needs review
L["TITAN_PALS_MENU_NOTE"] = "Note" -- Needs review
L["TITAN_PALS_MENU_PLAYING"] = "|cff00ffff%s|r is playing %s" -- Requires localization
L["TITAN_PALS_MENU_REALID"] = "|cff00ffffRealID Pals|r" -- Needs review
L["TITAN_PALS_MENU_REMOVE"] = "Remove" -- Needs review
L["TITAN_PALS_MENU_REMOVE_ALT"] = "Remove Alt" -- Needs review
L["TITAN_PALS_MENU_REMOVE_ALTDATA"] = "Remove Alt From Database" -- Needs review
L["TITAN_PALS_MENU_REMOVEDATA"] = "Database Utilitys" -- Needs review
L["TITAN_PALS_MENU_REMOVE_FRIEND"] = "Remove Friend" -- Needs review
L["TITAN_PALS_MENU_REMOVE_FRIENDDATA"] = "Remove Friend From Database" -- Needs review
L["TITAN_PALS_MENU_S2"] = "|cff00ffffStarcraft II|r" -- Requires localization
L["TITAN_PALS_MENU_SC2"] = "|cff00ffffStarcraft II|r" -- Requires localization
L["TITAN_PALS_MENU_SETTINGS"] = "Settings" -- Needs review
L["TITAN_PALS_MENU_SUB_REMOVE"] = "Remove %s" -- Needs review
L["TITAN_PALS_MENU_SYNC"] = "Syncing" -- Needs review
L["TITAN_PALS_MENU_SYNCALTS"] = "Sync Alts to Friends List" -- Needs review
L["TITAN_PALS_MENU_SYNCALTSFRIENDS"] = "Sync Alts Friends to Friends List" -- Needs review
L["TITAN_PALS_MENU_TEXT"] = "Titan Pals" -- Needs review
L["TITAN_PALS_MENU_WHISPER"] = "Whisper" -- Needs review
L["TITAN_PALS_MENU_WOW"] = "|cff00ffffWorld of Warcraft|r" -- Requires localization
L["TITAN_PALS_MENU_WTCG"] = "|cff00ffffHearthstone|r" -- Requires localization
L["TITAN_PALS_REAL_TOOLTIP"] = "|cff00ffffRealID|r Pals Info" -- Needs review
L["TITAN_PALS_STATIC_ADDNOTE"] = "Please enter friends note for %s :" -- Needs review
L["TITAN_PALS_STATICARRAY_FIXED2"] = "TitanpalsStatisArray[|cffeda55f\"%s\"|r] fixed!" -- Needs review
L["TITAN_PALS_SYNC_ALTS"] = "Adding Alt %s to firends list" -- Needs review
L["TITAN_PALS_SYNC_ALTS_LABEL"] = "Alt's" -- Needs review
L["TITAN_PALS_SYNC_FINISHED"] = "Sync %s finished!" -- Needs review
L["TITAN_PALS_SYNC_FRIENDS"] = "Adding %s to friends list" -- Needs review
L["TITAN_PALS_SYNC_FRIENDS_LABEL"] = "Alt's Friends" -- Needs review
L["TITAN_PALS_TOOLTIP"] = "Online Pals Info" -- Needs review
L["TITAN_PALS_TOOLTIP_BN"] = "BN" -- Requires localization
L["TITAN_PALS_TOOLTIP_D3"] = "D3" -- Requires localization
L["TITAN_PALS_TOOLTIP_EMPTY"] = "  Your Do Not Have Any Pals!!!" -- Needs review
L["TITAN_PALS_TOOLTIP_IGNORE"] = "Currently Ignored Info" -- Needs review
L["TITAN_PALS_TOOLTIP_IGNORE_EMPTY"] = "  Your ignore list is currently empty." -- Needs review
L["TITAN_PALS_TOOLTIP_MEM"] = "Memory Usage" -- Needs review
L["TITAN_PALS_TOOLTIP_NO_OFF"] = "  No Offline Pals" -- Needs review
L["TITAN_PALS_TOOLTIP_NOPALS"] = "  No Pals Currently Online" -- Needs review
L["TITAN_PALS_TOOLTIP_NOREALID_FRIENDS"] = "You have no |cff00ffffRealID Pals|r!!!" -- Needs review
L["TITAN_PALS_TOOLTIP_NOREALPALS"] = "  No |cff00ffffRealID|r Pals Currently Online" -- Needs review
L["TITAN_PALS_TOOLTIP_OFF"] = "Offline Pals Info" -- Needs review
L["TITAN_PALS_TOOLTIP_OFFLINE"] = "Offline" -- Needs review
L["TITAN_PALS_TOOLTIP_OFFLINE_IGNORED"] = "Ignored" -- Needs review
L["TITAN_PALS_TOOLTIP_S2"] = "S2" -- Requires localization
L["TITAN_PALS_TOOLTIP_SC2"] = "S2" -- Requires localization
L["TITAN_PALS_TOOLTIP_WOW"] = "WoW" -- Requires localization
L["TITAN_PALS_TOOLTIP_WTCG"] = "HS" -- Requires localization
L["TITAN_PALS_TRIAL_ACCOUNT"] = [=[|cffff0000This feather is disabled on trial accounts,

Trial accounts can not add |r|cff00ffffRealID|r|cffff0000 Friends.|r]=] -- Requires localization
