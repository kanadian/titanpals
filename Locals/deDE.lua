local L = LibStub( "AceLocale-3.0" ):NewLocale( "TitanPals", "deDE" );
if ( not L ) then return; end

L["TITAN_PALS_ABOUT"] = "About"
L["TITAN_PALS_ABOUT_AUTHOR"] = "|cffffd700Author :|r |cffff8c00 %s|r"
L["TITAN_PALS_ABOUT_CATEGORY"] = "|cffffd700Category :|r |cffff8c00 %s|r"
L["TITAN_PALS_ABOUT_DESC"] = "Addon for Titan Panel, This addon displays your friends list sorted by connection status on Titan Panel"
L["TITAN_PALS_ABOUT_EMAIL"] = "|cffffd700Email :|r |cffff8c00 %s|r"
L["TITAN_PALS_ABOUT_MEMORY"] = "|cffffd700Memory Usage :|r |cffffffff%skb|r"
L["TITAN_PALS_ABOUT_TRANSLATION"] = "|cffffd700Translations :|r |cffff8c00 %s|r"
L["TITAN_PALS_ABOUT_VERSION"] = "|cffffd700Version :|r |cff20ff20 r%s|r"
L["TITAN_PALS_ABOUT_WEBSITE"] = "|cffffd700Website :|r |cff00ccff%s|r"
L["TITAN_PALS_ADDON_LABEL"] = "Titan [|cffeda55fPals|r]"
L["TITAN_PALS_ADDON_UPDATE"] = "Titan Pals has just been updated to %s!"
L["TITAN_PALS_ARRAY"] = "|cffeda55f%s|r fixed!"
L["TITAN_PALS_ARRAY_FIXED"] = "TitanPals.|cffeda55f%s|r fixed!"
L["TITAN_PALS_ARRAY_UPDATE"] = "Titan Pals Data Array has just been Updated to %s!"
L["TITAN_PALS_BUTTON_LABEL"] = "Freunde: "
L["TITAN_PALS_CONFIG_ADDAUTHOR"] = "|cff00ff00%s|r Has been added to the Author's list"
L["TITAN_PALS_CONFIG_ALTS"] = "Alt"
L["TITAN_PALS_CONFIG_ALTS_DESC"] = "This will set this character alt status"
L["TITAN_PALS_CONFIG_AWAY"] = "Away"
L["TITAN_PALS_CONFIG_BANNER"] = "Display Load Banner"
L["TITAN_PALS_CONFIG_BANNER_DESC"] = "This will display addon information and memory usage when the addon is loaded in the chat frame."
L["TITAN_PALS_CONFIG_BANNER_FORMAT"] = "%s |cff00ff00r%s|r Loaded, using |cff00ff00%s|r of memory."
L["TITAN_PALS_CONFIG_BATTLENET"] = "Battlenet" -- Requires localization
L["TITAN_PALS_CONFIG_BUSY"] = "Busy"
L["TITAN_PALS_CONFIG_CLASS"] = "Class"
L["TITAN_PALS_CONFIG_CLASS_DESC"] = "This will set this characters class"
L["TITAN_PALS_CONFIG_CUSTOM_REALID_TOOLTIP"] = "Füge ein |cff00ffffRealID|r Tooltip Format hinzu :"
L["TITAN_PALS_CONFIG_CUSTOM_REALID_TOOLTIP_DESC"] = "Hier kannst du selbstdefinierte Tooltip Formate hinzufügen"
L["TITAN_PALS_CONFIG_CUSTOM_REALID_TOOLTIP_INST"] = [=[
|cff20ff20Benutze :|r
     |cff20ff20!cn|r für Name des Chars
     |cff20ff20!fn|r für vollen Namen
     |cff20ff20!sn|r für Vornamen
     |cff20ff20!ln|r für Nachnamen
     |cff20ff20!rn|r für Realm Namen
     |cff20ff20!gn|r für Game Client|r]=]
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP"] = "Füge ein Tooltip Format hinzu"
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_DESC"] = "Hier kannst du eigene Formate für das Tooltip-Display hinzufügen"
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_INST"] = [=[
|cff20ff20Benutze :|r
     |cff20ff20!s|r für Status
     |cff20ff20!l|r für Level
     |cff20ff20!p|r für Spieler
     |cff20ff20!z|r für Zone
     |cff20ff20!n|r für Bemerkung
     |cff20ff20!c|r für Klasse
     |cff20ff20!uc|r für farbige Spielernamen

|cffff9900Die RECHTE Seite muss von der LINKEN Seite mit ~ (Tilde) getrennt werden.|r]=]
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_REALID"] = "|cff00ffffRealID|r Tooltip Format :"
L["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_REALID_DESC"] = "Ein paar |cff00ffffRealID|r Voreinstellungen um den Namen von |cff00ffffRealID|r Freunden anzupassen"
L["TITAN_PALS_CONFIG_DEBUG"] = "|cff808080Debug Mode|r"
L["TITAN_PALS_CONFIG_DEBUG_DESC"] = "This enables debug mode"
L["TITAN_PALS_CONFIG_DEBUG_EVENT"] = "Events"
L["TITAN_PALS_CONFIG_DEBUG_EVENT_DESC"] = "Displays event as they happen in the chat frame"
L["TITAN_PALS_CONFIG_DEBUG_HEADER"] = "Debugging"
L["TITAN_PALS_CONFIG_DEBUG_HEADER_PRINT"] = "Print Functions"
L["TITAN_PALS_CONFIG_DEBUG_LOGEVENT"] = "Log Event"
L["TITAN_PALS_CONFIG_DEBUG_LOGEVENT_DESC"] = "This will log the events as they happen for later veiwing"
L["TITAN_PALS_CONFIG_DEBUG_PRINT_ALTS"] = "Print Alts"
L["TITAN_PALS_CONFIG_DEBUG_PRINT_ALTS_DESC"] = "This will print a list of your alts to the chat frame"
L["TITAN_PALS_CONFIG_DEBUG_PRINT_FRIENDS"] = "Print Friends"
L["TITAN_PALS_CONFIG_DEBUG_PRINT_FRIENDS_DESC"] = "This will print a list of your friends to the chat frame"
L["TITAN_PALS_CONFIG_DEBUG_REMAUTH"] = "Remove Author"
L["TITAN_PALS_CONFIG_DEBUG_REMAUTH_DESC"] = "Removes |cffff0000%s|r as a Addon Author"
L["TITAN_PALS_CONFIG_DIABLO"] = "Diablo III" -- Requires localization
L["TITAN_PALS_CONFIG_DND"] = "DND"
L["TITAN_PALS_CONFIG_DNDLABEL"] = "Do Not Disturb"
L["TITAN_PALS_CONFIG_DND_TYPES"] = "Busy Display Style"
L["TITAN_PALS_CONFIG_DND_TYPES_DESC"] = "The style that you would like Pals that are DND / Busy to show up as."
L["TITAN_PALS_CONFIG_DND_TYPES_INST"] = [=[

|cff20ff20This allow you to change the default text of <DND> in the tooltip, your choices are :|r
     |cffff8c00<DND>|r
     |cffff8c00<Busy>|r
     |cffff8c00[DND]|r
     |cffff8c00[Busy]|r
     |cffff8c00(DND)|r
     |cffff8c00(Busy)|r]=]
L["TITAN_PALS_CONFIG_EDITCHAR"] = "Edit Character"
L["TITAN_PALS_CONFIG_EDITCHAR_DESC"] = "This will edit the characters info"
L["TITAN_PALS_CONFIG_FRIEND"] = "Character"
L["TITAN_PALS_CONFIG_FRIEND_DESC"] = "Select character to edit"
L["TITAN_PALS_CONFIG_FRIENDEDIT"] = "Friend Edit"
L["TITAN_PALS_CONFIG_HEADER_ADD"] = "Neue Formate"
L["TITAN_PALS_CONFIG_HEADER_REALID"] = "|cff00ffffRealID Format|r"
L["TITAN_PALS_CONFIG_HEADER_REALID_ADD"] = "Neue |cff00ffffRealID|r Formate"
L["TITAN_PALS_CONFIG_HEADER_REALID_REMOVE"] = "Entferne |cff00ffffRealID|r Formate"
L["TITAN_PALS_CONFIG_HEADER_REMOVE"] = "Formate entfernen"
L["TITAN_PALS_CONFIG_HEADER_SETTINGS"] = "Settings"
L["TITAN_PALS_CONFIG_HEADER_STATUS"] = "Status Types"
L["TITAN_PALS_CONFIG_HEADER_TOOLTIP"] = "Tooltip Format"
L["TITAN_PALS_CONFIG_HEARTHSTONE"] = "Hearthstone" -- Requires localization
L["TITAN_PALS_CONFIG_LEVEL"] = "Level"
L["TITAN_PALS_CONFIG_LEVEL_DESC"] = "This will set this characters level"
L["TITAN_PALS_CONFIG_NO"] = "No"
L["TITAN_PALS_CONFIG_NOALTS"] = "Entferne alternative Chars vom ToolTip"
L["TITAN_PALS_CONFIG_NOALTS_DESC"] = "Verhindert das alternative Chars im ToolTip angezeigt werden."
L["TITAN_PALS_CONFIG_NONE"] = "None"
L["TITAN_PALS_CONFIG_PRESETS"] = "Tooltip Voreinstellungen :"
L["TITAN_PALS_CONFIG_PRESETS_DESC"] = "Eine Auswahl an Listenformatierungen für die Tooltip-Anzeige"
L["TITAN_PALS_CONFIG_PRESETS_PREVIEW"] = "Für Vorschau bitte die Maus über die Titan [|cffeda55fPals|r] Schaltfläche bewegen."
L["TITAN_PALS_CONFIG_REALID_NOTE1_OFF"] = " |cffff2020Deaktivieren|r dieser Option wird verhindern, das irgendeine |cff00ffffRealID Freunde|r Info im Tooltip angezeigt."
L["TITAN_PALS_CONFIG_REALID_NOTE1_ON"] = "|cff20ff20Aktivieren|r dieser Option wird die |cff00ffffRealID Freunde|r Info im Tooltip anzeigen."
L["TITAN_PALS_CONFIG_REALID_ONOFF"] = "Zeige die |cff00ffffRealID|r im Tooltip"
L["TITAN_PALS_CONFIG_REALID_ONOFF_DESC"] = "Diese Option wird die |cff00ffffRealID Freunde|r Info im Tooltip anzeigen."
L["TITAN_PALS_CONFIG_REALID_PRESETS"] = "|cff00ffffRealID|r Tooltip Voreinstellungen :"
L["TITAN_PALS_CONFIG_REALID_PRESETS_DESC"] = "Besondere Voreinstellungen für den |cff00ffffRealID|r Teil des Tooltips"
L["TITAN_PALS_CONFIG_REALID_REMOVE"] = "Entferne |cff00ffffRealID|r"
L["TITAN_PALS_CONFIG_REALID_REMOVE_DESC"] = "Dies wird das ausgewählte |cff00ffffRealID|r Format aus den Voreinstellungen löschen"
L["TITAN_PALS_CONFIG_REALID_REMOVE_PRESETS_DESC"] = "Bitte wähle ein |cff00ffffRealID|r Format zum Entfernen!"
L["TITAN_PALS_CONFIG_REALID_WARNING"] = "When realID is enabled this code replaces the !uc or !p part of the tooltip code" -- Requires localization
L["TITAN_PALS_CONFIG_REMAUTHOR"] = "|cffff0000%s|r Has been removed from the Author's list"
L["TITAN_PALS_CONFIG_REMOVE"] = "Entferne"
L["TITAN_PALS_CONFIG_REMOVE_DESC"] = "Dies wird das ausgewählte Format aus den Voreinstellungen entfernen"
L["TITAN_PALS_CONFIG_REMOVE_PRESETS_DESC"] = "Bitte wähle das zu entfernende Format!"
L["TITAN_PALS_CONFIG_SELECT"] = "Select"
L["TITAN_PALS_CONFIG_SHOWIGNORED"] = "Zeige zurzeit Ignoriert"
L["TITAN_PALS_CONFIG_SHOWIGNORED_DESC"] = "Zeigt alle Spieler die von euch derzeit ignoriert werden im Tooltip an."
L["TITAN_PALS_CONFIG_SHOWMEM"] = "Zeige Speicherverbrauch"
L["TITAN_PALS_CONFIG_SHOWMEM_DESC"] = "Zeigt den Speicherverbrauch von TitanPals an."
L["TITAN_PALS_CONFIG_SHOWOFFLINE"] = "Zeige Offline Freunde"
L["TITAN_PALS_CONFIG_SHOWOFFLINE_DESC"] = "Zeigt detaillierte Infos eurer Freunde im ToolTip an."
L["TITAN_PALS_CONFIG_STARCRAFT"] = "Starcraft II" -- Requires localization
L["TITAN_PALS_CONFIG_STATUS"] = "Catagory Select"
L["TITAN_PALS_CONFIG_STATUS_DESC"] = "Selects a catgory to edit"
L["TITAN_PALS_CONFIG_STATUS_TYPES"] = "Status Anzeigestil"
L["TITAN_PALS_CONFIG_STATUS_TYPES_DESC"] = "Der Anzeigestil der Statusanzeige für Freunde die gerade nicht verfügbar (AFK) sind"
L["TITAN_PALS_CONFIG_STATUS_TYPES_INST"] = [=[

|cff20ff20Erlaubt eine Ã„nderung des Default Texts von <nicht verfügbar> in the tooltip, you choices are :|r
     |cffff8c00<Nicht verfügbar>|r
     |cffff8c00<AFK>|r
     |cffff8c00<A>|r
     |cffff8c00(Nicht verfügbar)|r
     |cffff8c00(AFK)|r
     |cffff8c00(A)|r
     |cffff8c00[Nicht verfügbar]|r
     |cffff8c00[AFK]|r
     |cffff8c00[A]|r]=]
L["TITAN_PALS_CONFIG_UNKNOWN"] = "Unbekannt"
L["TITAN_PALS_CONFIG_WARCRAFT"] = "World of Warcraft" -- Requires localization
L["TITAN_PALS_CONFIG_YES"] = "Yes"
L["TITAN_PALS_FACTION_ERROR"] = "Sorry %s is from the %s, you can only invite people from the %s."
L["TITAN_PALS_GAME_ERROR"] = "Sorry %s is playing %s, you can only invite people that are playing %s." -- Requires localization
L["TITAN_PALS_HELP_ADDAUTH"] = "  |cffff0000/%s AddAuth|r - Add a AddOn Author to the Data Array"
L["TITAN_PALS_HELP_CMD_UNKNOWN"] = "Invalid Command!"
L["TITAN_PALS_HELP_ERROR"] = "Sorry but |cffff0000%s|r is not a valid command!"
L["TITAN_PALS_HELP_LINE1"] = "Usage:"
L["TITAN_PALS_HELP_LINE2"] = "  |cff00ff00/%s help|r - Shows this menu"
L["TITAN_PALS_HELP_LINE3"] = "  |cff00ff00/%s [ options | config ]|r - Opens options panel"
L["TITAN_PALS_HELP_LINE4"] = "  |cff00ff00/%s reset [ cmd ] [ pram ]|r - Reset Utilities"
L["TITAN_PALS_HELP_LINE5"] = "  |cff00ff00/%s checkDB|r - Checks Data Array for errors"
L["TITAN_PALS_HELP_REMAUTH"] = "  |cffff0000/%s RemAuth|r - Remove a AddOn Author from the Data Array"
L["TITAN_PALS_LOCALSARRAY_FIXED2"] = "TitanPalsLocals.|cffeda55f%s|r fixed!"
L["TITAN_PALS_MENU_ALLIANCE"] = "|cff4d4dffAlliance|r"
L["TITAN_PALS_MENU_BN"] = "|cff00ffffBattlenet|r" -- Requires localization
L["TITAN_PALS_MENU_CHECKDB"] = "Checking Data Array for errors"
L["TITAN_PALS_MENU_CHECKDB_FINSIHED"] = "Data Array Check Complete!"
L["TITAN_PALS_MENU_CLOSE"] = "Close"
L["TITAN_PALS_MENU_D3"] = "|cff00ffffDiablo III|r" -- Requires localization
L["TITAN_PALS_MENU_HIDE"] = "Hide Button" -- Requires localization
L["TITAN_PALS_MENU_HORDE"] = "|cffff4d4dHorde|r"
L["TITAN_PALS_MENU_INVITE"] = "Einladen"
L["TITAN_PALS_MENU_NEUTRAL"] = "Neutral" -- Requires localization
L["TITAN_PALS_MENU_NORMAL"] = "Normale Freunde"
L["TITAN_PALS_MENU_NOTE"] = "Hinweis"
L["TITAN_PALS_MENU_PLAYING"] = "|cff00ffff%s|r is playing %s" -- Requires localization
L["TITAN_PALS_MENU_REALID"] = "|cff00ffffRealID Freunde|r"
L["TITAN_PALS_MENU_REMOVE"] = "Entfernen"
L["TITAN_PALS_MENU_REMOVE_ALT"] = "Alternativen Char entfernen"
L["TITAN_PALS_MENU_REMOVE_ALTDATA"] = "Alternativen Char aus Datenbank entfernen"
L["TITAN_PALS_MENU_REMOVEDATA"] = "Werkzeuge Datenbank"
L["TITAN_PALS_MENU_REMOVE_FRIEND"] = "Freund entfernen"
L["TITAN_PALS_MENU_REMOVE_FRIENDDATA"] = "Freund aus Datenbank entfernen"
L["TITAN_PALS_MENU_S2"] = "|cff00ffffStarcraft II|r" -- Requires localization
L["TITAN_PALS_MENU_SC2"] = "|cff00ffffStarcraft II|r" -- Requires localization
L["TITAN_PALS_MENU_SETTINGS"] = "Einstellungen"
L["TITAN_PALS_MENU_SUB_REMOVE"] = "Entferne %s"
L["TITAN_PALS_MENU_SYNC"] = "Synchronisieren"
L["TITAN_PALS_MENU_SYNCALTS"] = "Synchronisiere alternative Chars mit Freundesliste"
L["TITAN_PALS_MENU_SYNCALTSFRIENDS"] = "Synchronisiere Freunde der alternativen Chars mit Freundesliste"
L["TITAN_PALS_MENU_TEXT"] = "Titan Pals"
L["TITAN_PALS_MENU_WHISPER"] = "Flüstern"
L["TITAN_PALS_MENU_WOW"] = "|cff00ffffWorld of Warcraft|r" -- Requires localization
L["TITAN_PALS_MENU_WTCG"] = "|cff00ffffHearthstone|r" -- Requires localization
L["TITAN_PALS_REAL_TOOLTIP"] = "|cff00ffffRealID|r Pals Info"
L["TITAN_PALS_STATIC_ADDNOTE"] = "Please enter friends note for %s :"
L["TITAN_PALS_STATICARRAY_FIXED2"] = "TitanPalsStatisArray.|cffeda55f%s|r fixed!"
L["TITAN_PALS_SYNC_ALTS"] = "Adding Alt %s to firends list"
L["TITAN_PALS_SYNC_ALTS_LABEL"] = "Alt's"
L["TITAN_PALS_SYNC_FINISHED"] = "Sync %s finished!"
L["TITAN_PALS_SYNC_FRIENDS"] = "Adding %s to friends list"
L["TITAN_PALS_SYNC_FRIENDS_LABEL"] = "Alt's Friends"
L["TITAN_PALS_TOOLTIP"] = "Info Freunde Online"
L["TITAN_PALS_TOOLTIP_BN"] = "BN" -- Requires localization
L["TITAN_PALS_TOOLTIP_D3"] = "D3" -- Requires localization
L["TITAN_PALS_TOOLTIP_EMPTY"] = "Deine Freundesliste ist Zur zeit leer!!!"
L["TITAN_PALS_TOOLTIP_IGNORE"] = "Info zurzeit Ignoriert"
L["TITAN_PALS_TOOLTIP_IGNORE_EMPTY"] = "Deine Ignorieren-Liste ist zur Zeit leer."
L["TITAN_PALS_TOOLTIP_MEM"] = "Speicherverbrauch"
L["TITAN_PALS_TOOLTIP_NO_OFF"] = "Keine Freunde offline"
L["TITAN_PALS_TOOLTIP_NOPALS"] = "Im Moment keine Freunde Online"
L["TITAN_PALS_TOOLTIP_NOREALID_FRIENDS"] = "Du hast keine |cff00ffffRealID Freunde|r!!!"
L["TITAN_PALS_TOOLTIP_NOREALPALS"] = "  Kein RealID Freund zur Zeit online"
L["TITAN_PALS_TOOLTIP_OFF"] = "Info Freunde Offline"
L["TITAN_PALS_TOOLTIP_OFFLINE"] = "Offline" -- Needs review
L["TITAN_PALS_TOOLTIP_OFFLINE_IGNORED"] = "Ignoriert"
L["TITAN_PALS_TOOLTIP_S2"] = "S2" -- Requires localization
L["TITAN_PALS_TOOLTIP_SC2"] = "S2" -- Requires localization
L["TITAN_PALS_TOOLTIP_WOW"] = "WoW" -- Requires localization
L["TITAN_PALS_TOOLTIP_WTCG"] = "HS" -- Requires localization
L["TITAN_PALS_TRIAL_ACCOUNT"] = [=[|cffff0000This feather is disabled on trial accounts,

Trial accounts can not add |r|cff00ffffRealID|r|cffff0000 Friends.|r]=]
