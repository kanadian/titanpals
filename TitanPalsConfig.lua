-- **************************************************************************
--   TitanPalsConfig.lua
--
--   By: KanadiaN
--        (Lindarena@Laughing Skull)
-- **************************************************************************

local L = LibStub( "AceLocale-3.0" ):GetLocale( "Titan", true );
local LB = LibStub( "AceLocale-3.0" ):GetLocale( "TitanPals", true );
local Config = LibStub( "AceConfig-3.0" );
local Dialog = LibStub( "AceConfigDialog-3.0" );

-- **************************************************************************
-- NAME : BNET_CLIENT_*
-- DESC : Helping WoW to see the Launcher as a Client
-- **************************************************************************
_G["BNET_CLIENT_APP"] = "App";
_G["BNET_CLIENT_BN"] = "BN";

-- **************************************************************************
-- NAME : Core Vars
-- DESC : These are here so they do not get changed like the locales would
-- **************************************************************************

local PlugInName = "TitanPals";
LB["TITAN_PALS_CORE_ID"] = "Pals";
LB["TITAN_PALS_CORE_VERSION"] = tonumber( GetAddOnMetadata( PlugInName, "Version" ) );
LB["TITAN_PALS_CORE_DTAG"] = "|cffff0000<TitanPals Debug>|r %s";
LB["TITAN_PALS_CORE_ITAG"] = "|cffff9900<TitanPals>|r %s";
LB["TITAN_PALS_CORE_ETAG"] = "|cffff9900<TitanPals Error>|r %s";
LB["TITAN_PALS_CORE_DBVER"] = tonumber( 4 );
LB["TITAN_PALS_CORE_MINLEVEL"] = tonumber( 0 );
LB["TITAN_PALS_CORE_MAXLEVEL"] = GetMaxPlayerLevel();
LB["TITAN_PALS_CORE_ARTWORK"] = "Interface\\FriendsFrame\\UI-Toast-ChatInviteIcon";

-- **************************************************************************
-- NAME : TitanPals_ConfigUtils(a)
-- DESC : Pull information from .toc for config dialog
-- **************************************************************************
local function TitanPals_ConfigUtils( a )
     if ( a == "GetAuthor" ) then
          return GetAddOnMetadata( PlugInName, "Authors" );
     elseif ( a == "GetTranslation" ) then
          return GetAddOnMetadata( PlugInName, "X-Translation" );
     elseif ( a == "GetCategory" ) then
          return GetAddOnMetadata( PlugInName, "X-Category" );
     elseif ( a == "GetEmail" ) then
          return GetAddOnMetadata( PlugInName, "X-Email" );
     elseif ( a == "GetWebsite" ) then
          return GetAddOnMetadata( PlugInName, "X-Website" );
     elseif ( a == "GetVersion" ) then
          return tostring( GetAddOnMetadata( PlugInName, "Version" ) );
     end
end

-- **************************************************************************
-- NAME : TitanPals_InitGUI();
-- DESC : Open Config Dialog
-- **************************************************************************
function TitanPals_InitGUI()
     Dialog:Open( PlugInName );
end

-- **************************************************************************
-- NAME : PalsOpt
-- DESC : This is the root of the config dialog
-- **************************************************************************
local PalsOpt = {
     type = "group",
     name = LB["TITAN_PALS_ADDON_LABEL"],
     args = {
          confgendesc = {
               type = "description", order = 1,
               name = LB["TITAN_PALS_ABOUT_DESC"],
               cmdHidden = true
          },
          aboutspacer = {
               type = "description", order = 2,
               name = "\n",
               cmdHidden = true
          },
	  cat1 = {
               type = "group", order = 3,
               name = LB["TITAN_PALS_ABOUT"],
               args = {
                    confversiondesc = {
                         type = "description", order = 1,
                         name = LB["TITAN_PALS_ABOUT_VERSION"]:format( TitanPals_ConfigUtils( "GetVersion" ) ),
                         cmdHidden = true
                    },
                    confauthordesc = {
                         type = "description", order = 2,
                         name = LB["TITAN_PALS_ABOUT_AUTHOR"]:format( TitanPals_ConfigUtils( "GetAuthor" ) ),
                         cmdHidden = true
                    },
                    conflangauthordesc = {
                         type = "description", order = 3,
                         name = LB["TITAN_PALS_ABOUT_TRANSLATION"]:format( TitanPals_ConfigUtils( "GetTranslation" ) ),
                         cmdHidden = true
                    },
                    confcatdesc = {
                         type = "description", order = 4,
                         name = LB["TITAN_PALS_ABOUT_CATEGORY"]:format( TitanPals_ConfigUtils( "GetCategory" ) ),
                         cmdHidden = true
                    },
                    confemaildesc = {
                         type = "description", order = 5,
                         name = LB["TITAN_PALS_ABOUT_EMAIL"]:format( TitanPals_ConfigUtils( "GetEmail" ) ),
                         cmdHidden = true
                    },
                    confwebsitedesc = {
                         type = "description", order = 6,
                         name = LB["TITAN_PALS_ABOUT_WEBSITE"]:format( TitanPals_ConfigUtils( "GetWebsite" ) ),
                         cmdHidden = true
                    },
                    confmemorydesc = {
                         type = "description", order = 7,
                         name = function()
                              UpdateAddOnMemoryUsage();
                              mem = floor( GetAddOnMemoryUsage( PlugInName ) );
                              return LB["TITAN_PALS_ABOUT_MEMORY"]:format( mem );
                         end,
                         cmdHidden = true,
                    },
               },
	  },
	  cat2 = {
	       type = "group", order = 4,
	       name = LB["TITAN_PALS_CONFIG_HEADER_SETTINGS"],
	       args = {
                    tooltipshowignored = {
                         type = "toggle", order = 1,
                         name = LB["TITAN_PALS_CONFIG_SHOWIGNORED"],
                         desc = LB["TITAN_PALS_CONFIG_SHOWIGNORED_DESC"],
                         get = function() return TitanPals.Settings.ShowIgnored; end,
                         set = function( _, value )
                              TitanPals.Settings.ShowIgnored = value;
                              TitanPanelButton_UpdateButton( LB["TITAN_PALS_CORE_ID"] );
                         end,
                    },
                    tooltipshowmem = {
                         type = "toggle", order = 2,
                         name = LB["TITAN_PALS_CONFIG_SHOWMEM"],
                         desc = LB["TITAN_PALS_CONFIG_SHOWMEM_DESC"]:format( LB["TITAN_PALS_ADDON_LABEL"] ),
                         get = function() return TitanPals.Settings.ShowMem; end,
                         set = function( _, value ) TitanPals.Settings.ShowMem = value; end,
                    },
		    tooltipshowoffline = {
                         type = "toggle", order = 3,
                         name = LB["TITAN_PALS_CONFIG_SHOWOFFLINE"],
                         desc = LB["TITAN_PALS_CONFIG_SHOWOFFLINE_DESC"],
                         get = function() return TitanPals.Settings.ShowOffline; end,
                         set = function( _, value ) TitanPals.Settings.ShowOffline = value; end,
                    },
                    tooltipnoalts = {
                         type = "toggle", order = 4,
                         name = LB["TITAN_PALS_CONFIG_NOALTS"],
                         desc = LB["TITAN_PALS_CONFIG_NOALTS_DESC"],
                         get = function() return TitanPals.Settings.NoAlts; end,
                         set = function( _, value ) TitanPals.Settings.NoAlts = value; end,
                    },
                    tooltipbannor = {
                         type = "toggle", order = 5,
                         name = LB["TITAN_PALS_CONFIG_BANNER"],
                         desc = LB["TITAN_PALS_CONFIG_BANNER_DESC"],
                         get = function() return TitanPals.Settings.DisplayB; end,
                         set = function( _, value ) TitanPals.Settings.DisplayB = value; end,
                    },
                    debugmode = {
                         type = "toggle", order = 6,
                         name = LB["TITAN_PALS_CONFIG_DEBUG"],
                         desc = LB["TITAN_PALS_CONFIG_DEBUG_DESC"],
                         hidden = function() return not TitanPals_CoreUtils( "Author", ( "%s@%s" ):format( TitanPalsProfile.Player, TitanPalsProfile.Realm ) ) end,
                         get = function() return TitanPalsDebug.State; end,
                         set = function( _, value ) TitanPalsDebug.State = value; end,
                    },
		    --[[
                    tooltipmessages = {
                         type = "group", order = 7, inline = true,
                         name = "Tooltip Messages",
                         args = {
                              shownopals = {
                                   type = "toggle", order = 1,
                                   name = "No Pals",
                                   desc = "Hide the \"No Pals Currently Online\" message.",
                                   get = function() return TitanPals.Settings.ShowNoPals; end,
                                   set = function( _, value ) TitanPals.Settings.ShowNoPals = value; end
                              },
                              shownorealid = {
                                   type = "toggle", order = 2,
                                   name = "No RealID",
                                   desc = "Hide the \"No RealID Pals Currently Online\" message.",
                                   get = function() return TitanPals.Settings.ShowNoRealID; end,
                                   set = function( _, value ) TitanPals.Settings.ShowNoRealID = value; end
                              },
                         },
                    },
		    ]]--
               },
	  },
	  cat3 = {
               type = "group", order = 5,
               name = LB["TITAN_PALS_CONFIG_HEADER_TOOLTIP"],
               args = {
	            tooltippresets = {
                         type = "select", order = 1, width = "full",
                         name = LB["TITAN_PALS_CONFIG_PRESETS"],
                         desc = LB["TITAN_PALS_CONFIG_PRESETS_DESC"],
                         get = function() return TitanPals.Settings.TFormat; end,
                         set = function( _, value ) TitanPals.Settings.TFormat = value; end,
                         values = function()
                              local preList = {};
                              local value;
                              for _, value in pairs( TitanPalsStaticArrays.presets ) do
                                   if ( value ~= TitanPals.Settings.TFormat ) then
                                        preList[value] = value;
                                   else
                                        preList[value] = ( "|cffffff9a%s|r" ):format( value );
                                   end
                              end
                              return preList;
                         end
                    },
                    nulloption1 = {
                         type = "description", order = 2,
                         name = LB["TITAN_PALS_CONFIG_PRESETS_PREVIEW"],
                         cmdHidden = true,
                    },
               },
	  },
	  cat4 = {
               type = "group", order = 6,
               name = LB["TITAN_PALS_CONFIG_HEADER_ADD"],
               args = {
                    tooltipaddcustom = {
			 type = "input", order = 1, width = "full",
			 name = LB["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP"],
			 desc = LB["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_DESC"],
			 get = function() return end,
			 set = function( _, value ) table.insert( TitanPalsStaticArrays.presets, value ); end,
		    },
                    nulloptionct = {
			 type = "description", order = 2,
			 name = LB["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_INST"],
			 cmdHidden = true;
                    },
               },
	  },
          cat5 = {
               type = "group", order = 7,
               name = LB["TITAN_PALS_CONFIG_HEADER_REMOVE"],
	       args = {
	            tooltippresetsremove = {
                         type = "select", order = 1, width = "full",
                         name = LB["TITAN_PALS_CONFIG_PRESETS"],
                         desc = LB["TITAN_PALS_CONFIG_REMOVE_PRESETS_DESC"],
                         get = function() return TitanPalsDebug.FormatToRemove; end,
                         set = function( _, value ) 
                              TitanPalsDebug.FormatToRemove = value;
			 end,
                         values = function()
                              local preList = {};
                              local value;
                              for _, value in pairs( TitanPalsStaticArrays.presets ) do
                                   if ( value ~= TitanPalsDebug.FormatToRemove ) then
                                        preList[value] = value;
                                   else
                                        preList[value] = ( "|cffffff9a%s|r" ):format( value );
                                   end
                              end
                              return preList;
                         end
                    },
		    tooltippresetremovebutton = {
                         type = "execute", order = 2,
			 name = LB["TITAN_PALS_CONFIG_REMOVE"],
			 desc = LB["TITAN_PALS_CONFIG_REMOVE_DESC"],
                         disabled = function()
                              if ( TitanPalsDebug.FormatToRemove ~= LB["TITAN_PALS_CONFIG_NONE"] ) then
                                   return false;
                              else
                                   return true;
                              end
                         end,
                         func = function()
                              if ( TitanPalsDebug.FormatToRemove == LB["TITAN_PALS_CONFIG_NONE"] ) then return; end
                              -- Need to add a check to see if the TFormat is about to be removed and if so change the TFormat to default
                              local key, value
			      local found = "NotSet";
                              for key, value in pairs( TitanPalsStaticArrays.presets ) do
                                   if ( TitanPals.Settings.TFormat == TitanPalsDebug.FormatToRemove and value ~= TitanPalsDebug.FormatToRemove and key ~= 1 and found == "NotSet" ) then 
                                        TitanPals.Settings.TFormat = value;
                                        found = "Set";
                                   end
                                   if ( value == TitanPalsDebug.FormatToRemove ) then
                                        table.remove( TitanPalsStaticArrays.presets, key );
                                        TitanPalsDebug.FormatToRemove = LB["TITAN_PALS_CONFIG_NONE"];
                                        break;
                                   end
                              end
                         end,
		    },
               },
          },
	  cat6 = {
	       type = "group", order = 8,
               name = LB["TITAN_PALS_CONFIG_HEADER_STATUS"],
               args = {
                    selectt = {
                         type = "select", order = 1,
                         name = LB["TITAN_PALS_CONFIG_STATUS"],
                         desc = LB["TITAN_PALS_CONFIG_STATUS_DESC"],
                         get = function() return TitanPals.Settings.dDefault;  end,
                         set = function( _, value ) TitanPals.Settings.dDefault = value; end,
                         values = function()
                              local db = { ( "<%s>" ):format( LB["TITAN_PALS_CONFIG_AWAY"] ), ( "<%s>" ):format( LB["TITAN_PALS_CONFIG_DND"] ), };
                              local preList = {};
                              local value;
                              for _, value in pairs( db ) do
                                   if ( value ~= TitanPals.Settings.dDefalut ) then
                                        preList[value] = value;
                                   else
                                        preList[value] = ( "|cffffff9a%s|r" ):format( value );
                                   end
                              end
                              return preList;
                         end,
                    },
                    scat1 = {
                         type = "group", order = 2, inline = true,
                         name = LB["TITAN_PALS_CONFIG_BUSY"],
                         hidden = function()
                              if ( TitanPals.Settings.dDefault == ( "<%s>" ):format( LB["TITAN_PALS_CONFIG_AWAY"] ) ) then
                                   return false;
                              else
                                   return true;
                              end
                         end,
                         args = {
                              statustypes = {
                                   type = "select", order = 1, width = "full",
                                   name = LB["TITAN_PALS_CONFIG_STATUS_TYPES"],
                                   desc = LB["TITAN_PALS_CONFIG_STATUS_TYPES_DESC"],
                                   get = function() return TitanPals.Settings.SFormat; end,
                                   set = function( _, value ) TitanPals.Settings.SFormat = value; end,
                                   values = function()
                                        local preList = {};
                                        local value;
                                        for _, value in pairs( TitanPalsStaticArrays.sTypes ) do
                                             if ( value ~= TitanPals.Settings.SFormat ) then
                                                  preList[value] = value;
                                             else
                                                  preList[value] = ( "|cffffff9a%s|r" ):format( value );
                                             end
                                        end
                                        return preList;
                                   end
                              },
                              nulloptionct = {
                                   type = "description", order = 2,
                                   name = LB["TITAN_PALS_CONFIG_STATUS_TYPES_INST"],
                                   cmdHidden = true
                              },
                         },
                    },
                    scat2 = {
                         type = "group", order = 3, inline = true,
                         name = LB["TITAN_PALS_CONFIG_DNDLABEL"],
                         hidden = function()
                              if ( TitanPals.Settings.dDefault == ( "<%s>" ):format( LB["TITAN_PALS_CONFIG_DND"] ) ) then
                                   return false;
                              else
                                   return true;
                              end
                         end,
                         args = {
                              statustypes = {
                                   type = "select", order = 1, width = "full",
                                   name = LB["TITAN_PALS_CONFIG_DND_TYPES"],
                                   desc = LB["TITAN_PALS_CONFIG_DND_TYPES_DESC"],
                                   get = function() return TitanPals.Settings.DFormat; end,
                                   set = function( _, value ) TitanPals.Settings.DFormat = value; end,
                                   values = function()
                                        local preList = {};
                                        local value;
                                        for _, value in pairs( TitanPalsStaticArrays.dTypes ) do
                                             if ( value ~= TitanPals.Settings.DFormat ) then
                                                  preList[value] = value;
                                             else
                                                  preList[value] = ( "|cffffff9a%s|r" ):format( value );
                                             end
                                        end
                                        return preList;
                                   end
                              },
                              nulloptionct = {
                                   type = "description", order = 2,
                                   name = LB["TITAN_PALS_CONFIG_DND_TYPES_INST"],
                                   cmdHidden = true
                              },
                         },
                    },
               },
          },
          cat7 = {
	       type = "group", order = 9,
	       name =  LB["TITAN_PALS_CONFIG_HEADER_REALID"];
	       args = {
                    realidonoroff = {
                         type = "toggle", order = 1, width = "full",
			 name = LB["TITAN_PALS_CONFIG_REALID_ONOFF"],
			 desc = LB["TITAN_PALS_CONFIG_REALID_ONOFF_DESC"],
                         disabled = function() return IsTrialAccount(); end,
			 get = function() return TitanPals.Settings.ShowBNTooltip; end,
			 set = function( _, value ) 
                              TitanPals.Settings.ShowBNTooltip = value;
                              TitanPanelButton_UpdateButton( LB["TITAN_PALS_CORE_ID"] );
                         end,
		    },
		    tooltipacustomrealid = {
			 type = "select", order = 2, width = "full",
			 name = LB["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_REALID"],
			 desc = LB["TITAN_PALS_CONFIG_CUSTOM_TOOLTIP_REALID_DESC"],
                         hidden = function() return not TitanPals.Settings.ShowBNTooltip end,
			 get = function() return TitanPals.Settings.RealIDFormat end,
			 set = function( _, value ) TitanPals.Settings.RealIDFormat = value; end,
                         values = function()
                              local preList = {};
                              local value;
                              for _, value in pairs( TitanPalsStaticArrays.realidformats ) do
                                   if ( value ~= TitanPals.Settings.RealIDFormat ) then
                                        preList[value] = value;
                                   else
                                        preList[value] = ( "|cffffff9a%s|r" ):format( value );
                                   end
                              end
                              return preList;
                         end
		    },
                    trialerror = {
                         type = "description", order = 3,
                         name = LB["TITAN_PALS_TRIAL_ACCOUNT"],
                         hidden = function() return not IsTrialAccount(); end,
                         cmdHidden = true,
                    },

                    nulloption1 = {
                         type = "description", order = 4,
                         name = "\n",
                         hidden = function() return not TitanPals.Settings.ShowBNTooltip end,
                         cmdHidden = true,
                    },
                    confrealwarning = {
                         type = "description", order = 5,
                         name = LB["TITAN_PALS_CONFIG_REALID_WARNING"],
                         hidden = function() return not TitanPals.Settings.ShowBNTooltip end,
                         cmdHidden = true,
                    },
                    nulloption2 = {
                         type = "description", order = 6,
                         name = "\n",
                         hidden = function() return not TitanPals.Settings.ShowBNTooltip end,
                         cmdHidden = true,
                    },
                    confrealinondesc = {
                         type = "description", order = 7,
                         name = LB["TITAN_PALS_CONFIG_REALID_NOTE1_ON"],
                         hidden = function() return not TitanPals.Settings.ShowBNTooltip end,
                         cmdHidden = true,
                    },
                    nulloption3 = {
                         type = "description", order = 8,
                         name = "\n",
                         hidden = function() return not TitanPals.Settings.ShowBNTooltip end,
                         cmdHidden = true,
                    },
                    confrealidoffdesc = {
                         type = "description", order = 9,
                         name = LB["TITAN_PALS_CONFIG_REALID_NOTE1_OFF"],
                         hidden = function() return not TitanPals.Settings.ShowBNTooltip end,
                         cmdHidden = true,
                    },
               },
	  },
	  cat8 = {
               type = "group", order = 10,
               name = LB["TITAN_PALS_CONFIG_HEADER_REALID_ADD"],
               hidden = function() return not TitanPals.Settings.ShowBNTooltip end,
               args = {
                    tooltipaddrealidcustom = {
			 type = "input", order = 1, width = "full",
			 name = LB["TITAN_PALS_CONFIG_CUSTOM_REALID_TOOLTIP"],
			 desc = LB["TITAN_PALS_CONFIG_CUSTOM_REALID_TOOLTIP_DESC"],
			 get = function() return end,
			 set = function( _, value ) table.insert( TitanPalsStaticArrays.realidformats, value ); end,
		    },
                    nulloptionct = {
			 type = "description", order = 2,
			 name = LB["TITAN_PALS_CONFIG_CUSTOM_REALID_TOOLTIP_INST"],
			 cmdHidden = true,
                    },
               },
	  },
          cat9 = {
               type = "group", order = 11,
               name = LB["TITAN_PALS_CONFIG_HEADER_REALID_REMOVE"],
               hidden = function() return not TitanPals.Settings.ShowBNTooltip end,
	       args = {
	            tooltippresetsremove = {
                         type = "select", order = 1, width = "full",
                         name = LB["TITAN_PALS_CONFIG_REALID_PRESETS"],
                         desc = LB["TITAN_PALS_CONFIG_REALID_REMOVE_PRESETS_DESC"],
                         get = function() return TitanPalsDebug.FormatToRemove; end,
                         set = function( _, value ) 
                              TitanPalsDebug.FormatToRemove = value;
			 end,
                         values = function()
                              local preList = {};
                              local value;
                              for _, value in pairs( TitanPalsStaticArrays.realidformats ) do
                                   if ( value ~= TitanPalsDebug.FormatToRemove ) then
                                        preList[value] = value;
                                   else
                                        preList[value] = ( "|cffffff9a%s|r" ):format( value );
                                   end
                              end
                              return preList;
                         end,
                    },
		    tooltippresetremovebutton = {
                         type = "execute", order = 2,
			 name = LB["TITAN_PALS_CONFIG_REALID_REMOVE"],
			 desc = LB["TITAN_PALS_CONFIG_REALID_REMOVE_DESC"],
                         disabled = function()
                              if ( TitanPalsDebug.FormatToRemove ~= LB["TITAN_PALS_CONFIG_NONE"] ) then
                                   return false;
                              else
                                   return true;
                              end
                         end,
                         func = function()
                              if ( TitanPalsDebug.FormatToRemove == LB["TITAN_PALS_CONFIG_NONE"] ) then return; end
                              local key, value
			      local found = "NotSet";
                              for key, value in pairs( TitanPalsStaticArrays.realidformats ) do
                                   if ( TitanPals.Settings.RealIDFormat == TitanPalsDebug.FormatToRemove and value ~= TitanPalsDebug.FormatToRemove and key ~= 1 and found == "NotSet" ) then 
                                        TitanPals.Settings.RealIDFormat = value;
                                        found = "Set";
                                   end
                                   if ( value == TitanPalsDebug.FormatToRemove ) then
                                        table.remove( TitanPalsStaticArrays.realidformats, key );
                                        TitanPalsDebug.FormatToRemove = LB["TITAN_PALS_CONFIG_NONE"];
                                        break;
                                   end
                              end
                         end,
		    },
               },
          },
          cat10 = {
               type = "group", order = 12,
               name = LB["TITAN_PALS_CONFIG_FRIENDEDIT"],
               hidden = function()
                    eCheck = TitanPals_CoreUtils( "checkEdit", "_false" );
                    return not eCheck;
               end,
               args = {
                    toonName = {
                         type = "select", order = 1, -- width = "full",
                         name = LB["TITAN_PALS_CONFIG_FRIEND"],
                         desc = LB["TITAN_PALS_CONFIG_FRIEND_DESC"],
                         get = function() return TitanPals.toonEdit.toonName; end,
                         set = function( _, value ) TitanPals.toonEdit.toonName = value; end,
                         values = function()
                              local list, labels = {}, {};
                              local _, labels = TitanPals_CoreUtils( "checkEdit", "_true" );
                              tinsert( labels, { name = LB["TITAN_PALS_CONFIG_SELECT"], value = LB["TITAN_PALS_CONFIG_SELECT"], } )
                              for _, v in pairs( labels ) do
                                   list[v.value] = v.name;
                              end
                              return list;
			 end,
                    },
                    toonClass = {
                         type = "select", order = 2, -- width = "full",
                         name = LB["TITAN_PALS_CONFIG_CLASS"],
                         desc = LB["TITAN_PALS_CONFIG_CLASS_DESC"],
                         get = function() return TitanPals.toonEdit.toonClass; end,
                         set = function( _, value ) TitanPals.toonEdit.toonClass = value; end,
                         disabled = function()
                              if ( TitanPals.toonEdit.toonName ~= LB["TITAN_PALS_CONFIG_SELECT"] ) then
                                   return false;
                              else
                                   return true;
                              end
                         end,
                         values = function()
                              local list, labels = {}, {};
                              local cClass, fClass, lClass, sClass;
                              for name, class in pairs( TitanPalsLocals.ToEnglish ) do
                                   lClass = TitanPalsLocals.LocalClass[class];
                                   fClass = TitanPalsLocals.ColorClass[class];
                                   cClass = fClass:format( lClass )
                                   tinsert( labels, { color = cClass, class = lClass, } )                                   
                              end
                              tinsert( labels, { color = LB["TITAN_PALS_CONFIG_UNKNOWN"], class = LB["TITAN_PALS_CONFIG_UNKNOWN"] } );
                              for _, v in pairs( labels ) do
                                   list[v.class] = v.color;
                              end
                              return list;
			 end,
                    },
                    toonLevel = {
                         type = "range", order = 3,
                         name = LB["TITAN_PALS_CONFIG_LEVEL"],
                         desc = LB["TITAN_PALS_CONFIG_LEVEL_DESC"],
                         get = function() return TitanPals.toonEdit.toonLevel; end,
                         set = function( _, value ) TitanPals.toonEdit.toonLevel = tonumber( value ); end,
			 min = LB["TITAN_PALS_CORE_MINLEVEL"],
			 max = LB["TITAN_PALS_CORE_MAXLEVEL"],
                         step = 1,
                         disabled = function()
                              if ( TitanPals.toonEdit.toonClass ~= LB["TITAN_PALS_CONFIG_UNKNOWN"] ) then
                                   return false;
                              else
                                   return true;
                              end
                         end,
                    },
                    toonAlt = {
                         type = "select", order = 4, -- width = "full",
                         name = LB["TITAN_PALS_CONFIG_ALTS"],
                         desc = LB["TITAN_PALS_CONFIG_ALTS_DESC"],
                         get = function() return TitanPals.toonEdit.toonAlt; end,
                         set = function( _, value ) TitanPals.toonEdit.toonAlt = value; end,
                         disabled = function()
                              if ( TitanPals.toonEdit.toonLevel ~= 0 ) then
                                   return false;
                              else
                                   return true;
                              end
                         end,
                         values = function()
                              local list = {};
                              local labels = { { name = LB["TITAN_PALS_CONFIG_SELECT"], value = LB["TITAN_PALS_CONFIG_SELECT"], }, { name = LB["TITAN_PALS_CONFIG_YES"], value = LB["TITAN_PALS_CONFIG_YES"], }, { name = LB["TITAN_PALS_CONFIG_NO"], value = LB["TITAN_PALS_CONFIG_NO"], }, };
                              for _, v in pairs( labels ) do
                                   list[v.name] = v.value;
                              end
                              return list;
			 end,
                    },
                    toonSpacer = {
                         type = "description", order = 5,
                         name = "\n",
                         cmdHidden = true,
                    },
                    toonSet = {
                         type = "execute", order = 6,
                         name = LB["TITAN_PALS_CONFIG_EDITCHAR"],
                         desc = LB["TITAN_PALS_CONFIG_EDITCHAR_DESC"],
                         disabled = function()
                              local dStatus
                              if ( TitanPals.toonEdit.toonName ~= LB["TITAN_PALS_CONFIG_SELECT"] and TitanPals.toonEdit.toonClass ~= LB["TITAN_PALS_CONFIG_UNKNOWN"] and TitanPals.toonEdit.toonName ~= "0" and TitanPals.toonEdit.toonAlt ~= LB["TITAN_PALS_CONFIG_SELECT"] ) then
                                   dStatsus = false;
                              else
                                   dStatus = true;
                              end
                              return dStatus;
                         end,
                         func = function()
                              if ( TitanPals.toonEdit.toonName ~= LB["TITAN_PALS_CONFIG_SELECT"] ) then
                                   if ( TitanPals.toonEdit.toonClass ~= LB["TITAN_PALS_CONFIG_UNKNOWN"] and TitanPals.toonEdit.toonLevel ~= 0 ) then
                                        local name = TitanPals.toonEdit.toonName;
                                        local class = TitanPals.toonEdit.toonClass;
                                        local level = TitanPals.toonEdit.toonLevel;
                                        TitanPalsLists[( "%s:%s" ):format( TitanPalsProfile.Faction, TitanPalsProfile.Realm )][name].Class = class;
                                        TitanPalsLists[( "%s:%s" ):format( TitanPalsProfile.Faction, TitanPalsProfile.Realm )][name].Level = tonumber( level );
                                        if ( TitanPals.toonEdit.toonAlt == LB["TITAN_PALS_CONFIG_YES"] ) then
                                             TitanPalsAlts[( "%s:%s" ):format( TitanPalsProfile.Faction, TitanPalsProfile.Realm )][name] = class;
                                        end
                                        TitanPals.toonEdit = {
                                             toonName = LB["TITAN_PALS_CONFIG_SELECT"],
                                             toonClass = LB["TITAN_PALS_CONFIG_UNKNOWN"],
                                             toonLevel = tonumber( 0 ),
                                             toonAlt = LB["TITAN_PALS_CONFIG_SELECT"],
                                        };
                                        collectgarbage( "collect" );
                                   end
                              end
                         end,
                    },
               },
          },
          cat11 = {
               type = "group", order = 13,
               name = LB["TITAN_PALS_CONFIG_DEBUG_HEADER"],
               hidden = function() return not TitanPalsDebug.State; end,
               args = {
                    event = {
                         type = "toggle", order = 1,
                         name = LB["TITAN_PALS_CONFIG_DEBUG_EVENT"],
                         desc = LB["TITAN_PALS_CONFIG_DEBUG_EVENT_DESC"],
                         get = function() return TitanPalsDebug.Event; end,
                         set = function( _, value ) TitanPalsDebug.Event = value; end,
                    },
                    logevent = {
                         type = "toggle", order = 2,
                         name = LB["TITAN_PALS_CONFIG_DEBUG_LOGEVENT"],
                         desc = LB["TITAN_PALS_CONFIG_DEBUG_LOGEVENT_DESC"],
                         get = function() return TitanPalsDebug.LogEvent; end,
                         set = function( _, value ) TitanPalsDebug.LogEvent = value; end,
                    },
                    playfriends = {
                         type = "execute", order = 3,
                         name = LB["TITAN_PALS_CONFIG_DEBUG_PRINT_FRIENDS"],
                         desc = LB["TITAN_PALS_CONFIG_DEBUG_PRINT_FRIENDS_DESC"],
                         func = function()
                              TitanPals_CoreUtils( "playFriends" );
                         end,
                    },
                    playalts = {
                         type = "execute", order = 4,
                         name = LB["TITAN_PALS_CONFIG_DEBUG_PRINT_ALTS"],
                         desc = LB["TITAN_PALS_CONFIG_DEBUG_PRINT_ALTS_DESC"],
                         func = function()
                              TitanPals_CoreUtils( "playAlts" );
                         end,
                    },
                    remauth = {
                         type = "execute", order = 5,
                         name = LB["TITAN_PALS_CONFIG_DEBUG_REMAUTH"],
                         hidden = function()
                              if ( getn( TitanPals.AddAuth ) == 0 ) then
                                   return true;
                              else
                                   return false;
                              end
                         end,
                         desc = function()
                              local author = ( "%s@%s" ):format( TitanPalsProfile.Player, TitanPalsProfile.Realm )
                              return LB["TITAN_PALS_CONFIG_DEBUG_REMAUTH_DESC"]:format( author )
                         end,
                         func = function()
                              TitanPals_CoreUtils( "RemAuth" );
                              TitanPalsDebug.State = false;
                              TitanPalsDebug.Event = false;
                              TitanPalsDebug.LogEvent = false;
                         end,
                    },
               },
          },
     },
}


-- **************************************************************************
-- NAME : TitanPals_AddNewPreset( value )
-- DESC : This will add a custom tooltip preset
-- **************************************************************************
function TitanPals_AddNewPreset( value )
     local found
     for _,i in pairs( TitanPalsStaticArrays.presets ) do
          if i.name == presetname or i.value == presetvalue then
               found = true; break;
          end
     end	
     if ( not found ) then table.insert( TitanPalsStaticArrays.presets, { name = presetname, value = presetvalue } ); end
end

-- **************************************************************************
-- NAME : TitanPanelPals_AddNewRealIDPreset()
-- DESC : This will add a custom tooltip preset for RealID Display
-- **************************************************************************
function TitanPanelPals_AddNewRealIDPreset( presetname, presetvalue )
     local found
     for _, i in pairs( TitanPalsStaticArrays.realidformats ) do
          if ( i.name == presetname or i.value == presetvalue ) then
               found = true;
               break;
          end
     end	
     if ( not found ) then table.insert( TitanPalsStaticArrays.realidformats, { name = presetname, value = presetvalue } ); end
end

-- **************************************************************************
-- NAME : TitanPanelPalsButton_OnLoad()
-- DESC : Registers the plugin upon it loading
-- **************************************************************************
function TitanPanelPalsButton_OnLoad( self )
     self.registry = {
          id = LB["TITAN_PALS_CORE_ID"],
          builtIn = false,
          version = TITAN_VERSION,
          menuText = LB["TITAN_PALS_MENU_TEXT"],
          buttonTextFunction = "TitanPanelPalsButton_GetButtonText";
	  tooltipTitle = LB["TITAN_PALS_TOOLTIP"],
          tooltipCustomFunction = TitanPanelPals_SetTooltip;
          icon = LB["TITAN_PALS_CORE_ARTWORK"],
          iconWidth = 16,
          category = "Information",
          controlVariables = {
               ShowIcon = true,
               ShowLabelText = true,
          },
          savedVariables = {
	       ShowIcon = 1,
	       ShowLabelText = 1,
          }
     };
     self:RegisterEvent( "ADDON_LOADED" );
     self:RegisterEvent( "PLAYER_ENTERING_WORLD" );
     self:RegisterEvent( "BN_FRIEND_ACCOUNT_ONLINE" );
     self:RegisterEvent( "BN_FRIEND_ACCOUNT_OFFLINE" );
     self:RegisterEvent( "FRIENDLIST_UPDATE" );
     self:RegisterEvent( "IGNORELIST_UPDATE" );


     -- Titan Pals Config
     Config:RegisterOptionsTable( PlugInName, PalsOpt );
     -- AceConfigDialog:AddToBlizOptions( PlugInName, LB["TITAN_PALS_CORE_ID"], L["TITAN_PANEL"] );
     -- TitanPals_Init( "init" );
end

-- **************************************************************************
-- NAME : SLASH_TITANPALS*
-- DESC : Slash command setup
-- **************************************************************************
SLASH_TITANPALS1 = "/titanpals";
SLASH_TITANPALS2 = "/tp";
SlashCmdList["TITANPALS"] = function( msg )
     TitanPals_CommandHandler( "titanpals", msg );
end

-- **************************************************************************
-- NAME : TitanPals_ShowHelp( cmd )
-- DESC : Slash command help display
-- **************************************************************************
function TitanPals_ShowHelp( cmd )
     TitanPals_DebugUtils( "info", "help", LB["TITAN_PALS_HELP_LINE1"] );
     TitanPals_DebugUtils( "info", "help", LB["TITAN_PALS_HELP_LINE2"]:format( cmd ) );
     TitanPals_DebugUtils( "info", "help", LB["TITAN_PALS_HELP_LINE3"]:format( cmd ) );
     TitanPals_DebugUtils( "info", "help", LB["TITAN_PALS_HELP_LINE4"]:format( cmd ) );
     TitanPals_DebugUtils( "info", "help", LB["TITAN_PALS_HELP_LINE5"]:format( cmd ) );
     local auth = TitanPals_CoreUtils( "Author", ( "%s@%s" ):format( TitanPalsProfile.Player, TitanPalsProfile.Realm ) )
     if ( auth ) then
          TitanPals_DebugUtils( "info", "help", LB["TITAN_PALS_HELP_ADDAUTH"]:format( cmd ) );
          TitanPals_DebugUtils( "info", "help", LB["TITAN_PALS_HELP_REMAUTH"]:format( cmd ) );
     end
end

-- **************************************************************************
-- NAME : TitanPals_CommandHandler()
-- DESC : Slash command handler
-- **************************************************************************
function TitanPals_CommandHandler( slash, command )
     local cmd1, cmd2, pram1, pram2
     _, _, cmd1, pram1 = strfind( command, "^([^ ]+) (.+)$" );
     if ( not cmd1 ) then cmd1 = command;
     else cmd1 = string.lower( cmd1 ); end
     if ( not cmd1 ) then cmd1 = ""; end
     if ( not pram1 ) then pram1 = ""; end
     if ( cmd1 == "" or cmd1 == "help" ) then
          TitanPals_ShowHelp( slash );
     elseif ( cmd1 == "options" or cmd1 == "config" ) then
          TitanPals_InitGUI();
     elseif ( cmd1 == "reset" ) then
          _ , _, cmd2, pram2 = strfind( pram1, "^([^ ]+) (.+)$" );
          if ( not cmd2 ) then cmd2 = pram1;
          else cmd2 = string.lower( cmd2 ); end
          if ( cmd2 == "all" or cmd2 == "settings" ) then rLoad = true;
          else rLoad = false; end
          TitanPals_ResetUtils( rLoad, cmd1, cmd2, pram2 );
     elseif ( cmd1 == "checkDB" or cmd1 == "checkdb" ) then
          TitanPals_ResetUtils( false, cmd1 );
     elseif ( cmd1 == "AddAuth" or cmd1 == "addauth" ) then
          TitanPals_CoreUtils( cmd1 );
     elseif ( cmd1 == "RemAuth" or cmd1 == "remauth" ) then
          TitanPals_CoreUtils( cmd1 );
     elseif ( cmd1 == "ver" or cmd1 == "version" ) then
          TitanPals_DebugUtils( "info", "VerCall", ( "Your are using addon version |cff00ff00%s|r" ):format( TitanPals.Settings.Version ) );
	  TitanPals_DebugUtils( "info", "VerCall", ( "Data Array version |cff00ff00%s|r" ):format( TitanPals.Settings.ArrayVersion ) );
	  TitanPals_DebugUtils( "info", "VerCall", ( "Update addon version |cff00ff00%s|r" ):format( LB["TITAN_PALS_CORE_VERSION"] ) );
	  TitanPals_DebugUtils( "info", "VerCall", ( "Update data array version |cff00ff00%s|r" ):format( LB["TITAN_PALS_CORE_DBVER"] ) );
     else
          TitanPals_DebugUtils( "error", "CommandHandler", LB["TITAN_PALS_HELP_ERROR"]:format( cmd1 ) );
     end
end