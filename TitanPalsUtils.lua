-- **************************************************************************
--   TitanPalsUtils.lua
--
--   By: KanadiaN
--        (Lindarena@Laughing Skull)
-- **************************************************************************

local L = LibStub( "AceLocale-3.0" ):GetLocale( "Titan", true );
local LB = LibStub( "AceLocale-3.0" ):GetLocale( "TitanPals", true );

-- **************************************************************************
-- NAME : TitanPals_CoreUtils( b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12 )
-- DESC : This makes everything happen
-- **************************************************************************
function TitanPals_CoreUtils( b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12 )
     local palsDB = ( "%s:%s" ):format( TitanPalsProfile.Faction, TitanPalsProfile.Realm )
     local b = { [1] = b1, [2] = b2, [3] = b3, [4] = b4, [5] = b5, [6] = b6, [7] = b7, [8] = b8, [9] = b9, [10] = b10, [11] = b11, [12] = b12, };
     if ( TitanPalsInit and TitanPalsDebug.State ) then
          local tdLine
          for i = 1, 12 do
               if ( i == 1 ) then
                    if ( b[i] ) then tdLine = ( "[|cffff9900%s|r]" ):format( b[i] ); end
               else
                    if ( b[i] ) then tdLine = ( "%s[|cffff9900%s|r]" ):format( tdLine, b[i] ); end
               end
          end
          TitanPalsDebug.dLine = ( "Recieved : %s" ):format( tdLine );
	  TitanPals_DebugUtils( "bug", "CoreUtils", TitanPalsDebug.dLine );
     end
     if ( b[1] == "DisplayB" ) then
          --[b1    ]
          --[Action]
          UpdateAddOnMemoryUsage();
          TitanPalsDebug.dLine = ( "%s kb" ):format( floor( GetAddOnMemoryUsage( "TitanPals" ) ) );
          TitanPals_DebugUtils( "info", "load", LB["TITAN_PALS_CONFIG_BANNER_FORMAT"]:format( LB["TITAN_PALS_ADDON_LABEL"], LB["TITAN_PALS_CORE_VERSION"], TitanPalsDebug.dLine ) );
     elseif ( b[1] == "MakeSafe" ) then
          --[b1    ]
          --[Action]
          local text
          if ( b[2] == true ) then text = "true";
          elseif ( b[2] == false ) then text = "false";
          elseif ( b[2] == nil ) then text = "nil";
          else text = b[2]; end
          return text;
     elseif ( b[1] == "playFriends" ) then
          --[b1    ]
          --[Action]
          local pTable = TitanPalsLists[palsDB];
          table.foreach( pTable, function( Name, Data )
                    local n = TitanPals_ColorUtils( "GetCClass", Data.Class, Name );
                    local l = TitanPals_ColorUtils( "GetCText", "hlight", ( "%s" ):format( Data.Level ) );
                    TitanPals_DebugUtils( "info", b[1], ( "(%s) %s" ):format( l, n ) );
               end
          )
     elseif ( b[1] == "playAlts" ) then
          --[b1    ]
          --[Action]
	  local pTable = TitanPalsAlts[palsDB];
          table.foreach( pTable, function( Name, Class )
                    local n = TitanPals_ColorUtils( "GetCClass", Class, Name );
	            local c = TitanPals_ColorUtils( "GetCClass", Class, ( "%s" ):format( Class ) );
                    TitanPals_DebugUtils( "info", b[1], ( "(%s) %s" ):format( c, n ) );
               end
          )
     elseif ( b[1] == "Author" ) then
           local Authors = {
               ["Lindarena@Laughing Skull"] = true,
               ["Stâbsâlot@Laughing Skull"] = true,
               ["Hilyna@Laughing Skull"] = true,
               ["Nagdand@Laughing Skull"] = true,
               ["Holycurves@Laughing Skull"] = true,
               ["Holytree@Laughing Skull"] = true,
               ["Acatra@Laughing Skull"] = true,
               ["Nâsh@Laughing Skull"] = true,
               ["Wenling@Laughing Skull"] = true,
               ["Koton@Laughing Skull"] = true,
          }
          for i = 1, getn( TitanPals.AddAuth ) do
               if ( not Authors[TitanPals.AddAuth[i]] ) then
                    Authors[TitanPals.AddAuth[i]] = true;
               else
                    tremove( TitanPals.AddAuth, i );
                    TitanPals_DebugUtils( "bug", "AddAuth", "Can Not Add Author Already Exists!" )
               end
          end
          return Authors[b[2]];
     elseif ( b[1] == "AddAuth" or b[1] == "addauth" ) then
          b[2] = ( "%s@%s" ):format( TitanPalsProfile.Player, TitanPalsProfile.Realm );
          TitanPals_DebugUtils( "info", "CoreUtils", LB["TITAN_PALS_CONFIG_ADDAUTHOR"]:format( b[2] ) );
          tinsert( TitanPals.AddAuth, b[2] );

     elseif ( b[1] == "RemAuth" or b[1] == "remauth" ) then
          b[2] = ( "%s@%s" ):format( TitanPalsProfile.Player, TitanPalsProfile.Realm );
          for i = 1, getn( TitanPals.AddAuth ) do
               if ( b[2] == TitanPals.AddAuth[i] ) then
                    TitanPals_DebugUtils( "info", "CoreUtils", LB["TITAN_PALS_CONFIG_REMAUTHOR"]:format( b[2] ) );
                    tremove( TitanPals.AddAuth, i );
               end
          end
     elseif ( b[1] == "getLocals" ) then
          local localClass = {};
          local sex, gender, gType
          for i = 1, 2 do
               if ( i == 1 ) then sex = true; gType = "TRUE";
               elseif ( i == 2 ) then sex = false; gType = "FALSE"; end
               FillLocalizedClassList( localClass, sex );
               for token, localName in pairs( localClass ) do
                    -- local color = RAID_CLASS_COLORS[token];
                    local color = C_ClassColor.GetClassColor( token );
                    local colorCode = ( "%02x%02x%02x" ):format( ( color.r * 255 ), ( color.g * 255 ), ( color.b * 255 ) );
                    local richText = ( "|cff%s%%s|r" ):format( colorCode );
                    gender = ( "%s_%s" ):format( token, gType );
                    TitanPalsLocals.ColorClass[gender] = richText;
                    TitanPalsLocals.LocalClass[gender] = localName;
                    TitanPalsLocals.ToEnglish[localName] = gender;
               end
          end
          TitanPals_GameClients();
     elseif ( b[1] == "checkEdit" ) then
          local tReturn = {};
          local nEdit = false;
          local fList = TitanPalsLists[palsDB];
          for n, v in pairs( fList ) do
               if ( v.Level == 0 or v.Class == LB["TITAN_PALS_CONFIG_UNKNOWN"] ) then
                    tinsert( tReturn, { name = n, value = n, } );
                    nEdit = true;
               end
          end
          if ( b[2] == "_true" ) then return nEdit, tReturn;
          else return nEdit; end
     end
end

-- **************************************************************************
-- NAME : TitanPals_ToolTipUtils()
-- DESC : Build the data for the mouseover tooltip
-- **************************************************************************
function TitanPals_ToolTipUtils( t1, t2, t3 )
     local t = { [1] = t1, [2] = t2, [3] = t3 };
     if ( TitanPalsInit and TitanPalsDebug.State ) then
          local tdLine
          for i = 1, 3 do
               if ( i == 1 ) then
                    if ( t[i] ) then tdLine = ( "[|cffff9900%s|r]" ):format( t[i] ); end
               else
                    if ( t[i] and type( t[i] ) == "string" ) then tdLine = ( "%s[|cffff9900%s|r]" ):format( tdLine, t[i] ); end
               end
          end
          TitanPalsDebug.dLine = ( "Recieved : %s" ):format( tdLine );
	  TitanPals_DebugUtils( "bug", "ToolTipUtils", TitanPalsDebug.dLine );
     end
     if ( t[1] == "ToolTip" ) then
          -- [b1    ][b2    ][b3        ]
          -- [Action][Format][Table Data]
         if ( t[3].pType == "bnet" ) then
               if ( t[3].isAFK or t[3].isDND  ) then
                    if ( t[3].isAFK ) then t[2] = string.gsub( t[2], "!s", TitanPals_ColorUtils( "GetCText", "sgray", TitanPals.Settings.SFormat ) ); end
                    if ( t[3].isDND ) then t[2] = string.gsub( t[2], "!s", TitanPals_ColorUtils( "GetCText", "sgray", TitanPals.Settings.DFormat ) ); end
               else
                    t[2] = string.gsub( t[2], "!s", " " );
               end
          elseif ( t[3].pType == "pals" ) then
               if ( t[3].state == ( "<%s>" ):format( LB["TITAN_PALS_CONFIG_AWAY"] ) or t[3].state == ( "<%s>" ):format( LB["TITAN_PALS_CONFIG_DND"] ) ) then
                    if ( t[3].state == ( "<%s>" ):format( LB["TITAN_PALS_CONFIG_AWAY"] ) ) then t[2] = string.gsub( t[2], "!s", TitanPals_ColorUtils( "GetCText", "sgray", TitanPals.Settings.SFormat ) ); end
                    if ( t[3].state == ( "<%s>" ):format( LB["TITAN_PALS_CONFIG_DND"] ) ) then t[2] = string.gsub( t[2], "!s", TitanPals_ColorUtils( "GetCText", "sgray", TitanPals.Settings.DFormat ) ); end
               else
                    t[2] = string.gsub( t[2], "!s", " " );
               end
          end
          local GetCType = TitanPals_DataUtils( "Type", t[3].client );
          local cCheck = TitanPals_DataUtils( "cCheck", t[3].level )
          if ( cCheck ) then
               t[2] = string.gsub( t[2], "!l", TitanPals_ColorUtils( "GetCText", "orange", LB[ ( "TITAN_PALS_TOOLTIP_%s" ):format( t[3].level ) ] ) );
          elseif ( not cCheck ) then
               t[2] = string.gsub( t[2], "!l", TitanPals_ColorUtils( "GetCText", "orange", t[3].level ) );
          end
          if ( string.find( t[2], "!p" ) ) then
               if ( t[3].pType == "pals" ) then
                    t[2] = string.gsub( t[2], "!p", TitanPals_ColorUtils( GetCType, t[3].class, t[3].toonName ) );
               elseif ( t[3].pType == "bnet" ) then
                    if ( t[3].client == BNET_CLIENT_WOW ) then
                         t[2] = string.gsub( t[2], "!p", TitanPals_ColorUtils( GetCType, t[3].class, t[3].realIDName ) );
                    else
                         t[2] = string.gsub( t[2], "!p", TitanPals_ColorUtils( GetCType, t[3].client, t[3].realIDName ) );
                    end
               end
          end
          t[2] = string.gsub( t[2], "!c", TitanPals_ColorUtils( GetCType, t[3].class, t[3].class ) );
          if ( t[3].note ) then
               t[2] = string.gsub( t[2], "!n", TitanPals_ColorUtils( "GetCText", "gray", t[3].note ) );
          else
               t[2] = string.gsub( t[2], "!n", "" );
          end
          if ( t[3].zone ) then
               t[2] = string.gsub( t[2], "!z", TitanPals_ColorUtils( "GetCText", "sgray", t[3].zone ) );
          else
               t[2] = string.gsub( t[2], "!z", TitanPals_ColorUtils( "GetCText", "gray", t[12] ) );
          end
          if ( string.find( t[2], "!uc" ) ) then
               if ( t[3].pType == "pals" ) then
                    t[2] = string.gsub( t[2], "!uc", TitanPals_ColorUtils( GetCType, t[3].class, t[3].toonName ) );
               elseif ( t[3].pType == "bnet" ) then
                    t[2] = string.gsub( t[2], "!uc", TitanPals_ColorUtils( GetCType, t[3].class, t[3].realIDName ) );
               end
          end
          local left, right = string.match( t[2], "(.*)~(.*)" );
          return left, right;
     elseif ( t[1] == "BattleNet" ) then
          -- [b1    ][b2    ][b3        ]
          -- [Action][Format][Table Data]
	  local GetCType = TitanPals_DataUtils( "Type", t[3].client );
          t[2] = string.gsub( t[2], "!cn", TitanPals_ColorUtils( GetCType, t[3].class, t[3].toonName ) );
          t[2] = string.gsub( t[2], "!rn", TitanPals_ColorUtils( GetCType, t[3].class, t[3].realmName ) );
          t[2] = string.gsub( t[2], "!fn", TitanPals_ColorUtils( GetCType, t[3].class, t[3].accountName ) );
          t[2] = string.gsub( t[2], "!gn", TitanPals_ColorUtils( "GetCText", "gray", t[3].client ) );
          return t[2];
     end
end

-- **************************************************************************
-- NAME : TitanPals_DataUtils()
-- DESC : Build an array on online Pals
-- **************************************************************************
function TitanPals_DataUtils( d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15 )
     local d = { [1] = d1, [2] = d2, [3] = d3, [4] = d4, [5] = d5, [6] = d6, [7] = d7, [8] = d8, [9] = d9, [10] = d10, [11] = d11, [12] = d12, [13] = d13, [14] = d14, [15] = d15,  };
     local palsDB = ( "%s:%s" ):format( TitanPalsProfile.Faction, TitanPalsProfile.Realm );
     local function _getInfo( ID )
          local fInfo = C_BattleNet.GetFriendAccountInfo( ID );
          local gInfo = fInfo.gameAccountInfo
          -- fInfo.gameAccountInfo = nil;
	  return fInfo, gInfo;
     end
     -- Make sure the AddOn is propley setup for use
     TitanPals_SetUser();
     if ( d[1] == "Update" ) then
          -- Lets build a database to use for the menu
          local NP, NPO = C_FriendList.GetNumFriends(), C_FriendList.GetNumOnlineFriends();
          local BNT, BNO, BNF, BNFO = BNGetNumFriends();
          tempDB = {};
          tempDB.NP = NP;
          tempDB.NPO = NPO;
          tempDB.BNT = BNT;
          tempDB.BNO = BNO;
          -- Friends
          if ( tempDB.NPO > 0 ) then
               tempDB.PALS = {};
               for i = 1, tempDB.NPO do
                    local name, level, class, area, connected, state, note = GetFriendInfo( i );
                    if ( not tempDB.PALS[name] ) then tempDB.PALS[name] = {}; end
                    if ( not TitanPalsLists[palsDB][name] ) then
                         TitanPalsLists[palsDB][name] = {};
                    end
                    tempDB.PALS[name].toonName = name;
                    tempDB.PALS[name].level = tonumber( level );
                    tempDB.PALS[name].client = BNET_CLIENT_WOW
                    tempDB.PALS[name].class = class;
                    tempDB.PALS[name].zone = area;
                    tempDB.PALS[name].connected = connected;
                    tempDB.PALS[name].state = state;
                    tempDB.PALS[name].note = note;
                    tempDB.PALS[name].pType = "pals";
                    TitanPalsLists[palsDB][name].Level = tonumber( level );
                    TitanPalsLists[palsDB][name].Class = class;
                    if ( not note ) then
                         TitanPalsLists[palsDB][name].Note = "na";
                    else
                         TitanPalsLists[palsDB][name].Note = note;
                    end
               end
          end
          -- RealID
          if ( TitanPals.Settings.ShowBNTooltip and tempDB.BNO > 0 ) then
               local bn = {};
               tempDB.BN = {};
               local tName
               for h = 1, tempDB.BNO do
                    -- local presenceID, presenceName, battleTag, isBattleTagPresence, toonName, toonID, client, isOnline, lastOnline, isAFK, isDND, messageText, noteText, isRIDFriend, messageTime, canSoR, isReferAFriend, canSummonFriend = BNGetFriendInfo( id );
                    -- local pID, pName, pTag, _, _, tID, _, _, _, isAFK, isDND, _, noteText, _, _, _ = BNGetFriendInfo( h );
                    local friend, game = _getInfo( h );
                    -- TitanPalsDebug.Temp[ friend.accountName ] = friend;
                    if ( not tempDB.BN[friend.accountName] ) then tempDB.BN[friend.accountName] = {}; end
                    if ( not tempDB.pID ) then tempDB.pID = {}; end
                    tempDB.pID[friend.accountName] = friend.bnetAccountID;
		    if ( game.clientProgram ~= BNET_CLIENT_CLNT and game.clientProgram ~= BNET_CLIENT_APP ) then
                         -- local _, toonName, client, realmName, _, faction, race, class, guild, zone, level, gameText = BNGetGameAccountInfo( tID );
                         tName = BNet_GetValidatedCharacterName( game.characterName, friend.battleTag, game.clientProgram );
                         if ( tName ~= nil ) then
                              if ( not tempDB.BN[friend.accountName][tName] ) then tempDB.BN[friend.accountName][tName] = {}; end
                              if ( game.clientProgram == BNET_CLIENT_WOW ) then
                                   TitanPals_DataUtils( "BNadd", friend.accountName, tName, game.clientProgram, game.realmName, game.factionName, game.raceName, game.className, game.areaName, game.characterLevel, friend.isAFK, friend.isDND, friend.note );
                              elseif ( game.clientProgram == BNET_CLIENT_SC2 ) then
                                   TitanPals_DataUtils( "BNadd", friend.accountName, tName, game.clientProgram, LB["TITAN_PALS_CONFIG_STARCRAFT"], game.factionName, game.raceName, BNET_CLIENT_SC2, LB["TITAN_PALS_CONFIG_STARCRAFT"], BNET_CLIENT_SC2, friend.isAFK, friend.isDND, friend.note );
                              elseif ( game.clientProgram == BNET_CLIENT_D3 ) then
                                   TitanPals_DataUtils( "BNadd", friend.accountName, tName, game.clientProgram, LB["TITAN_PALS_CONFIG_DIABLO"], game.factionName, game.raceName, BNET_CLIENT_D3, LB["TITAN_PALS_CONFIG_DIABLO"], BNET_CLIENT_D3, friend.isAFK, friend.isDND, friend.note );
                              elseif ( game.clientProgram == BNET_CLIENT_WTCG ) then
                                   TitanPals_DataUtils( "BNadd", friend.accountName, tName, game.clientProgram, LB["TITAN_PALS_CONFIG_HEARTHSTONE"], game.factionName, game.raceName, BNET_CLIENT_WTCG, LB["TITAN_PALS_CONFIG_HEARTHSTONE"], BNET_CLIENT_WTCG, friend.isAFK, friend.isDND, friend.note );
                              else
                                   TitanPals_DataUtils( "BNadd", friend.accountName, tName, BNET_CLIENT_BN, LB["TITAN_PALS_CONFIG_BATTLENET"], BNET_CLIENT_BN, BNET_CLIENT_BN, BNET_CLIENT_BN, BNET_CLIENT_BN, LB["TITAN_PALS_CONFIG_BATTLENET"], BNET_CLIENT_BN, friend.isAFK, friend.isDND, friend.note );
                              end
                         end
                    end
               end

               table.foreach( tempDB.BN, function( pal, toon )
                         local count = TitanPals_DataUtils( "tCount", toon );
                         table.foreach( toon, function( cur, data )
                                   -- print( count, " --- ", cur, " --- ", data.tName );
                                   if ( count <= 1 ) then 
                                        bn[pal] = {};
                                        bn[pal][data.toonName] = data;
                                   elseif ( count >= 2 ) then
                                        if ( data.client ~= BNET_CLIENT_BN ) then 
                                             -- print( data.client, " ~= ", BNET_CLIENT_BN )
                                             bn[pal] = {};
                                             bn[pal][data.toonName] = data;
                                        end
                                   end
                              end
                         )
                    end
               )
               tempDB.BN = bn;
          end
          bn = nil;
          if ( TitanPalsDebug.State ) then TitanPalsDebug.Temp.temp = tempDB; end
          return tempDB;
     elseif ( d[1] == "BNadd" ) then
          -- "BNadd", friend.accountName, tName, game.clientProgram, game.realmName, game.factionName, game.raceName, game.className, game.areaName, game.characterLevel, friend.isAFK, friend.isDND, friend.note
          if ( d[3] ) then tempDB.BN[d[2]][d[3]].toonName = d[3]; end
          if ( d[4] ) then tempDB.BN[d[2]][d[3]].client = d[4]; end
          if ( d[5] ) then tempDB.BN[d[2]][d[3]].realmName = d[5]; end
          if ( d[6] ) then tempDB.BN[d[2]][d[3]].faction = d[6]; end
          if ( d[7] ) then tempDB.BN[d[2]][d[3]].race = d[7]; end
          if ( d[8] ) then tempDB.BN[d[2]][d[3]].class = d[8]; end
          -- if ( d[9] ) then tempDB.BN[d[2]][d[3]].guild = d[9]; end
          if ( d[9] ) then tempDB.BN[d[2]][d[3]].zone = d[9]; end
          if ( d[10] ) then tempDB.BN[d[2]][d[3]].level = d[10]; end
          tempDB.BN[d[2]][d[3]].isALT = false;
          if ( d[11] ) then tempDB.BN[d[2]][d[3]].isAFK = d[11]; end
          if ( d[12] ) then tempDB.BN[d[2]][d[3]].isDND = d[12]; end
          if ( d[13] ) then tempDB.BN[d[2]][d[3]].note = d[13]; end
          tempDB.BN[d[2]][d[3]].pType = "bnet";
     elseif ( d[1] == "tCount" ) then
          -- [b1    ]]d2   ]
          -- [Action][Table]
          local tCount = 0;
          table.foreach( d[2], function( Key, Data )
                    tCount = tCount + 1;
               end
          )
          return tCount;
     elseif ( d[1] == "Type" ) then
          local gType
          if ( d[2] == BNET_CLIENT_SC2 or d[2] == BNET_CLIENT_D3 or d[2] == BNET_CLIENT_WTCG or d[2] == BNET_CLIENT_BN or d[2] == BNET_CLIENT_APP ) then
               gType = "GetCClient";
          else
               gType = "GetCClass";
	  end
          return gType;
     elseif ( d[1] == "cCheck" ) then
          cCheck = false;
          if ( d[2] == BNET_CLIENT_SC2 or d[2] == BNET_CLIENT_D3 or d[2] == BNET_CLIENT_WTCG or d[2] == BNET_CLIENT_BN ) then
               cCheck = true;
          else
               cCheck = false;
	  end
          return cCheck;
     end
end

-- **************************************************************************
-- NAME : TitanPals_DebugUtils( d1, d2, d3, d4, d5 )
-- DESC : For debug messaging and print messages to chat frame
-- **************************************************************************
function TitanPals_DebugUtils( d1, d2, d3, d4, d5 )
     local d = { [1] = d1, [2] = d2, [3] = d3, [4] = d4, [5] = d5, };
     --[d1    ][d2      ][d3   ]
     --[Action][Function][error]
     if ( d[1] == "info" ) then
          DEFAULT_CHAT_FRAME:AddMessage( LB["TITAN_PALS_CORE_ITAG"]:format( d[3] ) );
     elseif ( d[1] == "error" ) then
          DEFAULT_CHAT_FRAME:AddMessage( LB["TITAN_PALS_CORE_ETAG"]:format( d[3] ) );
     elseif ( d[1] == "bug" and TitanPalsDebug.State ) then
          local fError = ( "[%s] %s" ):format( TitanPals_ColorUtils( "GetCText", "gold", d[2] ), d[3] );
          DEFAULT_CHAT_FRAME:AddMessage( LB["TITAN_PALS_CORE_DTAG"]:format( fError ) );
          if ( TitanPalsDebug.LogEvent ) then tinsert( TitanPalsDebug.Log, d[3] ); end
     end
end

-- **************************************************************************
-- NAME : TitanPals_ResetUtils( r1, r2, r3, r4, r5 )
-- DESC : Diffrent ways to fix / reset Data Array
-- **************************************************************************
function TitanPals_ResetUtils( r1, r2, r3, r4, r5 )
     local r = { [1] = r1, [2] = r2, [3] = r3, [4] = r4, [5] = r5, };
     --[r1    ][r2    ][r3  ][r4   ]
     --[reload][action][pram][value]
     if ( r[2] == "reset" ) then
          if ( r[3] == "settings" ) then
               TitanPalsInit = nil;
               TitanPals = nil;
          elseif ( r[3] == "locals" and r[4] == "LocalClass" or r[4] == "ToEnglish" or r[4] == "ColorClass" ) then
               TitanPalsLocals[r[4]] = {};
               TitanPals_CoreUtils( "getLocals" );
          elseif ( r[3] == "array" and r[4] == "dTypes" or r[4] == "sTypes" or r[4] == "presets" or r[4] == "realidformats" ) then
               TitanPalsInit = false;
               TitanPalsStaticArrays[r[4]] = nil;
               r[1] = true;
          elseif ( r[3] == "all" ) then
               if ( r[4] ) then
                    if ( r[4] == "locals" ) then 
                         wipe( TitanPalsLocals );
                         TitanPalsLocals.LocalClass = {};
                         TitanPalsLocals.ColorClass = {};
                         TitanPalsLocals.ToEnglish = {};
                         TitanPals_CoreUtils( "getLocals" );
                         r[1] = false;
                    elseif ( r[4] == "array" ) then
                         TitanPalsInit = false;
                         TitanPalsStaticArrays = {};
                         r[1] = true;
                    elseif ( b[4] == "lists" ) then
                         TitanPalsLists = {};
                    elseif ( b[4] == "alts" ) then
                         TitanPalsAlts = {};
                    end
               elseif ( not r[4] ) then
                    TitanPalsInit = nil;
                    TitanPals = nil;
                    TitanPalsLists = nil;
                    TitanPalsAlts = nil;
                    TitanPalsStaticArrays = nil;
                    TitanPalsLocals = nil;
               end
          else 
               TitanPals_DebugUtils( "error", "CommandHandler", LB["TITAN_PALS_HELP_CMD_UNKNOWN"] );
          end
     elseif ( r[2] == "checkDB" or r[2] == "checkdb" ) then
          TitanPals_Init( "checkDB" );
          TitanPanelButton_UpdateButton( LB["TITAN_PALS_CORE_ID"] );
          TitanPanelButton_UpdateTooltip( self );
     end
     if ( r[1] ) then ReloadUI(); end
end

-- **************************************************************************
-- NAME : TitanPals_ColorUtils( c1, c2, c3, c4, c5 )
-- DESC : for coloring the tooltip and messages
-- **************************************************************************
function TitanPals_ColorUtils( c1, c2, c3, c4, c5 )
     local c = { [1] = c1, [2] = c2, [3] = c3, [4] = c4, [5] = c5, };
     if ( c[1] == "GetCText" ) then
          --[c1    ][c2   ][c3  ]
          --[Action][Color][Text]
          local color = strupper( c[2] )
          local colorCode
          local colorCache = {
               GREEN = "|cff00ff00%s|r",
               BLUE = "|cff00ffff%s|r",
               GRAY = "|cff808080%s|r",
               SGRAY = "|cff98afc7%s|r",
               HLIGHT = "|cffffffff%s|r",
               NORMAL = "|cffffd200%s|r",
               ORANGE = "|cffff9900%s|r",
               RED = "|cffff0000%s|r",
               GOLD = "|cffff9900%s|r",
          }
          colorCode = colorCache[color]:format( c[3] );
          return colorCode;
     elseif ( c[1] == "GetCClass" ) then
          --[c1    ][c2   ][c3    ]
          --[Action][Class][Player]
          local classEng, localClass, colorCode;
          colorCode = "|cff808080%s|r";
          if ( c[2] == LB["TITAN_PALS_CONFIG_UNKNOWN"] ) then 
               colorCode = "|cff808080%s|r";
          else
               classEng = TitanPalsLocals.ToEnglish[c[2]];
               localClass = TitanPalsLocals.LocalClass[classEng];
               colorCode = TitanPalsLocals.ColorClass[classEng];
          end
          return colorCode:format( c[3] );
     elseif ( c[1] == "GetCClient" ) then
          --[c1    ][c2    ][c3    ]
          --[Action][Client][Player]
          local classEng, localClass, colorCode;
          colorCode = "|cff808080%s|r";
          if ( c[2] == LB["TITAN_PALS_CONFIG_UNKNOWN"] ) then 
               colorCode = "|cff808080%s|r";
          else
               classEng = TitanPalsLocals.ClientEnglish[c[2]];
               localClass = TitanPalsLocals.LocalClient[classEng];
               colorCode = TitanPalsLocals.ColorClient[classEng];
          end
          return colorCode:format( c[3] );
     elseif ( c[1] == "GetCFText" ) then
          --[c1    ][c2     ][c3        ]
          --[Action][Faction][Faction ID]
          if ( c[3] == 0 ) then
               colorCode = ( "|cffff4d4d%s|r" ):format( c[2] ); -- Horde
          elseif ( c[3] == 1 ) then
               colorCode = ( "|cff4d4dff%s|r" ):format( c[2] ); -- Alliance
          elseif ( c[3] == 2 ) then 
               colorCode = ( "|cffffff00%s|r" ):format( c[2] ); -- Neutral
          end
          return colorCode;
     end
end

-- **************************************************************************
-- NAME : TitanPals_GameClient()
-- DESC : This may or may not get used
-- **************************************************************************

function TitanPals_GameClients()
     local gender;
     local richText = "|cff00ffff%s|r"
     local cTable = { BNET_CLIENT_WOW, BNET_CLIENT_SC2, BNET_CLIENT_D3, BNET_CLIENT_WTCG, BNET_CLIENT_BN }
     table.foreach( cTable, function( Key, Data )
               gender = ( "%s_%s" ):format( Data, Data );
               TitanPalsLocals.ColorClient[gender] = richText;
               TitanPalsLocals.LocalClient[gender] = LB[ ( "TITAN_PALS_TOOLTIP_%s" ):format( strupper( Data ) ) ];
               TitanPalsLocals.ClientEnglish[Data] = gender;
          end
     )
end

-- **************************************************************************
-- NAME : TitanPanelPals_StaticPopup( notesAdd )
-- DESC : Dialog for custom Notes
-- **************************************************************************

function TitanPanelPals_StaticPopup( notesAdd )
     StaticPopupDialogs["TITAN_PALS_CUSTOM_NOTE"] = {
          text = LB["TITAN_PALS_STATIC_ADDNOTE"],
          button1 = TEXT( ACCEPT ),
          button2 = TEXT( CANCEL ),
          hasEditBox = 1,
          maxLetters = 250,
          OnAccept = function( self )
               local pNote = self.editBox:GetText();
               if ( notesAdd.Type == "Normal" ) then
                    SetFriendNotes( notesAdd.Player, pNote )
               elseif ( notesAdd.Type == "BNet" ) then
                    BNSetFriendNote( notesAdd.pID, pNote );
               end
          end,
          OnShow = function( self )
               self.editBox:SetFocus();
          end,
          OnHide = function( self )
               self.editBox:SetText( "" );
          end,
          EditBoxOnEnterPressed = function( self )
               local parent = self:GetParent();
               local pNote = parent.editBox:GetText();
               self:GetParent():Hide();
          end,
          EditBoxOnEscapePressed = function( self )
               self:GetParent():Hide();
          end,
          timeout = 0,
          exclusive = 1,
          whileDead = 1,
          hideOnEscape = 1
     };
     StaticPopup_Show( "TITAN_PALS_CUSTOM_NOTE", notesAdd.PlayerColored );
end
