-- **************************************************************************
--   TitanPals.lua
--
--   By: KanadiaN
--        (Lindarena@Laughing Skull)
-- **************************************************************************

local L = LibStub( "AceLocale-3.0" ):GetLocale( "Titan", true );
local LB = LibStub( "AceLocale-3.0" ):GetLocale( "TitanPals", true );

-- **************************************************************************
-- NAME : TitanPals_Init( state )
-- DESC : For first time addon use
-- **************************************************************************
function TitanPals_Init( state )
     if ( state == "checkDB" ) then
          checkDB = true;
          TitanPalsInit = false;
     elseif ( state == "init" ) then
          checkDB = false;
     end
     if ( type( TitanPals ) ~= "table" ) then TitanPals = {}; end
     if ( TitanPalsInit ~= true ) then
          if ( checkDB ) then TitanPals_DebugUtils( "info", "reset", LB["TITAN_PALS_MENU_CHECKDB"] ); end
	  if ( not TitanPalsLists or TitanPalsLists == nil ) then
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_ARRAY"]:format( "TitanPalsLists" ) ); end
               TitanPalsLists = {};
               if ( TitanPals.Lists ) then
                    TitanPalsLists = TitanPals.Lists;
               end
          end
          if ( not TitanPalsAlts or TitanPalsAlts == nil ) then
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_ARRAY"]:format( "TitanPalsAlts" ) ); end
               TitanPalsAlts = {};
               if ( TitanPals.Alts ) then
                    TitanPalsAlts = TitanPals.Alts;
               end
          end
	  if ( not TitanPalsStaticArrays or TitanPalsStaticArrays == nil ) then
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_ARRAY"]:format( "TitanPalsStaticArrays" ) ); end
               TitanPalsStaticArrays = {};
          end
	  if ( not TitanPalsStaticArrays.presets or TitanPalsStaticArrays.presets == nil ) then
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_STATICARRAY_FIXED2"]:format( "presets" ) ); end
               TitanPalsStaticArrays.presets = {
                    LB["TITAN_PALS_CONFIG_NONE"],
                    "!s (!l !c) !p ~ !n !z",
                    "!s (!l) !uc ~ !n !z",
                    "!s (!l) !p - !c !z ~ !n",
                    "!s (!l) !c - !p !z ~ !n",
                    "!s (!l) !uc !z ~ !n",
                    "!s !p (!l) - !c !n ~ !z",
                    "!s !z (!l) - !c !n ~ !p",
                    "!s !c (!l) !z !n ~ !p",
               };
          end
	  if ( not TitanPalsStaticArrays.sTypes or TitanPalsStaticArrays.sTypes == nil ) then
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_STATICARRAY_FIXED2"]:format( "sTypes" ) ); end
               TitanPalsStaticArrays.sTypes = {
                    ( "<%s>" ):format( LB["TITAN_PALS_CONFIG_AWAY"] ),
                    "<AFK>",
                    "<A>",
                    ( "(%s)" ):format( LB["TITAN_PALS_CONFIG_AWAY"] ),
                    "(AFK)",
                    "(A)",
                    ( "[%s]" ):format( LB["TITAN_PALS_CONFIG_AWAY"] ),
                    "[AFK]",
                    "[A]",
               };
          end
          if ( not TitanPalsStaticArrays.dTypes or TitanPalsStaticArrays.dTypes == nil ) then
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_STATICARRAY_FIXED2"]:format( "dTypes" ) ); end
               TitanPalsStaticArrays.dTypes = {
                    ( "<%s>" ):format( LB["TITAN_PALS_CONFIG_DND"] ),
                    ( "<%s>" ):format( LB["TITAN_PALS_CONFIG_BUSY"] ),
                    ( "[%s]" ):format( LB["TITAN_PALS_CONFIG_DND"] ),
                    ( "[%s]" ):format( LB["TITAN_PALS_CONFIG_BUSY"] ),
                    ( "(%s)" ):format( LB["TITAN_PALS_CONFIG_DND"] ),
                    ( "(%s)" ):format( LB["TITAN_PALS_CONFIG_BUSY"] ),
               };
          end
	  if ( not TitanPalsStaticArrays.realidformats or TitanPalsStaticArrays.realidformats == nil ) then
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_STATICARRAY_FIXED2"]:format( "realidformats" ) ); end
               TitanPalsStaticArrays.realidformats = {
                    LB["TITAN_PALS_CONFIG_NONE"],
                    "!cn",
                    "!cn - !rn",
                    "!fn/!cn",
                    "!fn/!cn - !rn",
                    "!fn ( !cn )",
                    "!cn ( !fn )",
               };
          end
	  if ( not TitanPalsLocals or TitanPalsLocals == nil ) then
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_ARRAY"]:format( "TitanPalsLocals" ) ); end
               TitanPalsLocals = {};
          end
	  if ( not TitanPalsLocals.LocalClass or TitanPalsLocals.LocalClass == nil ) then
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_LOCALSARRAY_FIXED2"]:format( "LocalClass" ) ); end
               TitanPalsLocals.LocalClass = {};
          end
	  if ( not TitanPalsLocals.ColorClass or TitanPalsLocals.ColorClass == nil ) then
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_LOCALSARRAY_FIXED2"]:format( "ColorClass" ) ); end
               TitanPalsLocals.ColorClass = {};
          end
	  if ( not TitanPalsLocals.ToEnglish or TitanPalsLocals.ToEnglish == nil ) then
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_LOCALSARRAY_FIXED2"]:format( "ToEnglish" ) ); end
               TitanPalsLocals.ToEnglish = {};
          end
	  if ( not TitanPalsLocals.LocalClient or TitanPalsLocals.LocalClient == nil ) then
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_LOCALSARRAY_FIXED2"]:format( "LocalClient" ) ); end
               TitanPalsLocals.LocalClient = {};
          end
	  if ( not TitanPalsLocals.ColorClient or TitanPalsLocals.ColorClient == nil ) then
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_LOCALSARRAY_FIXED2"]:format( "ColorClinet" ) ); end
               TitanPalsLocals.ColorClient = {};
          end
	  if ( not TitanPalsLocals.ClientEnglish or TitanPalsLocals.ClientEnglish == nil ) then
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_LOCALSARRAY_FIXED2"]:format( "ClientEnglish" ) ); end
               TitanPalsLocals.ClientEnglish = {};
          end
          if ( not TitanPalsDebug or TitanPalsDebug == nil ) then
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_ARRAY"]:format( "TitanPalsDebug" ) ); end
               TitanPalsDebug = {
                    State = false,
                    Event = false,
                    LogEvent = false,
                    dLine = "",
                    FormatToRemove = LB["TITAN_PALS_CONFIG_NONE"], 
                    Temp = {},
               };
          end
          if ( not TitanPals.Settings or TitanPals.Settings == nil ) then
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_ARRAY_FIXED"]:format( "Settings" ) ); end
               TitanPals.Settings = {
                    ShowIgnored = false,
                    ShowMem = true,
	            ShowBNTooltip = true,
                    UpDateDB = true,
                    DisplayB = true,
                    ShowMem = true,
                    NoAlts = true,
                    ShowNoPals = true,
                    ShowNoRealID = true,
                    TFormat = "!s (!l !c) !p ~ !n !z",
                    SFormat = ( "<%s>" ):format( LB["TITAN_PALS_CONFIG_AWAY"] ),
                    DFormat = ( "<%s>" ):format( LB["TITAN_PALS_CONFIG_BUSY"] ),
                    dDefault = ( "<%s>" ):format( LB["TITAN_PALS_CONFIG_AWAY"] ),
                    RealIDFormat = "!cn",
                    Version = LB["TITAN_PALS_CORE_VERSION"],
                    ArrayVersion = LB["TITAN_PALS_CORE_DBVER"],
               };
          end
          if ( not TitanPals.AddAuth or TitanPals.AddAuth == nil ) then
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_ARRAY_FIXED"]:format( "AddAuth" ) ); end
               TitanPals.AddAuth = {};
          end
          if ( not TitanPals.notesAdd or TitanPals.notesAdd == nil ) then 
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_ARRAY_FIXED"]:format( "notesAdd" ) ); end
               TitanPals.notesAdd = {};
          end
          if ( not TitanPals.toonEdit or TitanPals.toonEdit == nil ) then
               if ( checkDB ) then TitanPals_DebugUtils( "info", "Init", LB["TITAN_PALS_ARRAY_FIXED"]:format( "toonEdit" ) ); end
               TitanPals.toonEdit = {
                    toonName = LB["TITAN_PALS_CONFIG_SELECT"],
                    toonClass = LB["TITAN_PALS_CONFIG_UNKNOWN"],
                    toonLevel = 0,
                    toonAlt = LB["TITAN_PALS_CONFIG_SELECT"],
               };
          end
          TitanPalsInit = true;
          if ( checkDB ) then TitanPals_DebugUtils( "info", "reset", LB["TITAN_PALS_MENU_CHECKDB_FINSIHED"] ); end
     end
     if ( not TitanPalsProfile or TitanPals.Profile == nil ) then
          TitanPalsProfile = {};
     end
     TitanPalsProfile = {
          Player = LB["TITAN_PALS_CONFIG_UNKNOWN"],
          Faction = LB["TITAN_PALS_CONFIG_UNKNOWN"],
          Realm = LB["TITAN_PALS_CONFIG_UNKNOWN"],
          Level = LB["TITAN_PALS_CONFIG_UNKNOWN"],
          Class = LB["TITAN_PALS_CONFIG_UNKNOWN"],
     };
     if ( LB["TITAN_PALS_CORE_DBVER"] > TitanPals.Settings.ArrayVersion ) then
          if ( LB["TITAN_PALS_CORE_DBVER"] == 4 ) then
               TitanPals.Settings.ShowNoPals = true;
               TitanPals.Settings.ShowNoRealID = true;
          end
          TitanPals.Settings.ArrayVersion = LB["TITAN_PALS_CORE_DBVER"];
     end
     TitanPals_CoreUtils( "getLocals" );
     TitanPalsDebug.FormatToRemove = LB["TITAN_PALS_CONFIG_NONE"];
end

-- **************************************************************************
-- NAME : TitanPanelPalsButton_OnEvent()
-- DESC : Event Handler
-- **************************************************************************
function TitanPanelPalsButton_OnEvent( self, event, ... )
     -- if ( TitanPalsInit and TitanPalsDebug.Event ) then TitanPals_DebugUtils( "bug", "OnEvent", ( "[%s]" ):format ( TitanPals_ColorUtils( "GetCText", "gray", event ) ) ); end
     if ( event == "ADDON_LOADED" and ... == "TitanPals" ) then
          if ( not TitanPalsInst ) then
               TitanPals_Init( "init" );
          end
          if ( not BNFeaturesEnabled() ) then TitanPals.Settings.ShowBNTooltip = false; end
     elseif ( event == "PLAYER_ENTERING_WORLD" ) then
          if ( TitanPals.Settings.DisplayB ) then TitanPals_CoreUtils( "DisplayB" ); end
          TitanPals_SetUser();
     elseif ( event == "BN_FRIEND_ACCOUNT_ONLINE" or event == "BN_FRIEND_ACCOUNT_OFFLINE" or event == "FRIENDLIST_UPDATE" or event == "IGNORELIST_UPDATE" )then
          TitanPanelButton_UpdateButton( LB["TITAN_PALS_CORE_ID"] );
          TitanPanelButton_UpdateTooltip( self );
     end
end

-- **************************************************************************
-- NAME : TitanPanelPalsButton_GetButtonText()
-- DESC : Build Button Text
-- DEBUG : /script TitanPanelPalsButton_GetButtonText()
-- **************************************************************************
function TitanPanelPalsButton_GetButtonText()
     local NumIgnore = C_FriendList.GetNumIgnores();
     local NumPals, NumPalsOnline = C_FriendList.GetNumFriends(), C_FriendList.GetNumOnlineFriends();
     local _, BNetCount = BNGetNumFriends();
     local buttonRichText
     buttonRichText = TitanPals_ColorUtils( "GetCText", "green", tonumber( NumPalsOnline ) );
     if ( TitanPals.Settings.ShowBNTooltip ) then
          buttonRichText = ( "%s/%s" ):format( buttonRichText, TitanPals_ColorUtils( "GetCText", "blue", BNetCount ) );
     end
     if ( TitanPals.Settings.ShowIgnored ) then 
          buttonRichText = ( "%s/%s" ):format( buttonRichText, TitanPals_ColorUtils( "GetCText", "red", NumIgnore ) );
     end
     return LB["TITAN_PALS_BUTTON_LABEL"], buttonRichText;
end

-- **************************************************************************
-- NAME : TitanPanelPalsButton_OnClick()
-- DESC : Toggles Friends Dialog
-- **************************************************************************
function TitanPanelPalsButton_OnClick( self, button )
     if ( button == "LeftButton" ) then
          ToggleFriendsFrame( 1 );
          --[[
          if ( FriendsFrame:IsShown() ) then
               HideUIPanel( FriendsFrame );
          else
               ShowUIPanel( FriendsFrame );
          end
          ]]--
     else
          TitanPanelButton_OnClick( self, button );
     end
end

-- **************************************************************************
-- NAME : TitanPals_SetUser()
-- DESC : Sets up the User for use with the data array
-- **************************************************************************
function TitanPals_SetUser()
     local Faction = UnitFactionGroup( "player" );
     local Realm = GetRealmName();
     local Player = GetUnitName( "player" );
     local Level = UnitLevel( "player" );
     local Class = UnitClass( "player" );
     if ( not TitanPalsProfile.Player or TitanPalsProfile.Player ~= Player ) then TitanPalsProfile.Player = Player; end
     if ( not TitanPalsProfile.Faction or TitanPalsProfile.Faction ~= Faction ) then TitanPalsProfile.Faction = Faction; end
     if ( not TitanPalsProfile.Level or TitanPalsProfile.Level ~= Level ) then TitanPalsProfile.Level = tonumber( Level ); end
     if ( not TitanPalsProfile.Class or TitanPalsProfile.Class ~= Class ) then TitanPalsProfile.Class = Class; end
     if ( not TitanPalsProfile.Realm or TitanPalsProfile.Realm ~= Realm ) then TitanPalsProfile.Realm = Realm; end
     if ( not TitanPalsLists[( "%s:%s" ):format( Faction, Realm )] ) then TitanPalsLists[( "%s:%s" ):format( Faction, Realm )] = {}; end
     if ( not TitanPalsLists[( "%s:%s" ):format( Faction, Realm )][Player] ) then TitanPalsLists[( "%s:%s" ):format( Faction, Realm )][Player] = {}; end
     if ( TitanPalsLists[( "%s:%s" ):format( Faction, Realm )][Player] ) then
          TitanPalsLists[( "%s:%s" ):format( Faction, Realm )][Player].Class = Class;
          TitanPalsLists[( "%s:%s" ):format( Faction, Realm )][Player].Level = tonumber( Level );
          TitanPalsLists[( "%s:%s" ):format( Faction, Realm )][Player].Note = "na";
     end
     if ( not TitanPalsAlts[( "%s:%s" ):format( Faction, Realm )] ) then TitanPalsAlts[( "%s:%s" ):format( Faction, Realm )] = {}; end
     if ( not TitanPalsAlts[( "%s:%s" ):format( Faction, Realm )][Player] ) then TitanPalsAlts[( "%s:%s" ):format( Faction, Realm )][Player] = Class; end
end

-- **************************************************************************
-- NAME : TitanPals_SyncAlts()
-- DESC : This adds your alts to your friends list for things like mailing
-- **************************************************************************
function TitanPals_SyncAlts()
     -- Alts to Pals List Syncing
     local np = GetNumFriends();
     local aIndex = TitanPalsAlts[( "%s:%s" ):format( TitanPalsProfile.Faction, TitanPalsProfile.Realm )];
     local fI, palsn, palsl, palsc, palsa, palsco, palss, palsno;
     local syncType = LB["TITAN_PALS_SYNC_ALTS_LABEL"];
     for n, v in pairs( aIndex ) do
          local tp = 0;
          local fI;
          for fI=1, np do
               palsn = GetFriendInfo( fI );
               if ( n == palsn ) then tp = 1; end
          end
          if ( tp == 0 and n ~= TitanPalsProfile.Player ) then
               TitanPals_DebugUtils( "info", syncType, LB["TITAN_PALS_SYNC_ALTS"]:format( n ) );
               AddFriend( n ); 
          end
     end
     TitanPals_DebugUtils( "info", syncType, LB["TITAN_PALS_SYNC_FINISHED"]:format( syncType ) );
end

-- **************************************************************************
-- NAME : TitanPals_SyncAlts()
-- DESC : This syncs your alts friend to your friends list
-- **************************************************************************

function TitanPals_SyncAltsFriends()
     local np = GetNumFriends();
     local iTable = TitanPalsLists[( "%s:%s" ):format( TitanPalsProfile.Faction, TitanPalsProfile.Realm )];
     local palsn, palsl, palsc, palsa, palsco, palss, palsno;
     local syncType = LB["TITAN_PALS_SYNC_FRIENDS_LABEL"];
     for n, v in pairs( iTable ) do
          if ( n ~= TitanPalsProfile.Player ) then
               local tp = 0;
               local fI;
               for fI = 1, np do
                    palsn = GetFriendInfo( fI );
                    if ( n == palsn ) then tp = 1; end
               end
               if ( tp == 0 and n ~= TitanPalsProfile.Player ) then
                    TitanPals_DebugUtils( "info", syncType, LB["TITAN_PALS_SYNC_FRIENDS"]:format( n ) );
                    AddFriend( n );
               end
	  end
     end
     TitanPals_DebugUtils( "info", syncType, LB["TITAN_PALS_SYNC_FINISHED"]:format( syncType ) );
end
