-- **************************************************************************
--   TitanPalsRightClick.lua
--
--   By: KanadiaN
--        (Lindarena@Laughing Skull)
-- **************************************************************************

local L = LibStub( "AceLocale-3.0" ):GetLocale( "Titan", true );
local LB = LibStub( "AceLocale-3.0" ):GetLocale( "TitanPals", true );

-- **************************************************************************
-- NAME : TitanPanelRightClickMenu_PreparePalsMenu()
-- DESC : Builds Right Click Interactive Menu
-- **************************************************************************
function TitanPanelRightClickMenu_PreparePalsMenu()
     local tempDB = TitanPals_DataUtils( "Update" );
     TitanPalsDebug.Temp = tempDB;
     local tooltipDB = {};
     local palsDB = ( "%s:%s" ):format( TitanPalsProfile.Faction, TitanPalsProfile.Realm );
     tempDB.aCount = TitanPals_DataUtils( "tCount", TitanPalsAlts[palsDB] );
     tempDB.lCount = TitanPals_DataUtils( "tCount", TitanPalsLists[palsDB] );
     tempDB.player = UnitName( "player" );
     -- Start Menu 2
     if ( TitanPanelRightClickMenu_GetDropdownLevel() == 2 ) then
          -- This is for whispering a friend
          if ( TitanPanelRightClickMenu_GetDropdMenuValue() == LB["TITAN_PALS_MENU_WHISPER"] ) then
               -- Normal Friends
               if ( tempDB.NPO > 0 ) then
                    TitanPanelRightClickMenu_AddTitle( ( "%s %s" ):format( LB["TITAN_PALS_MENU_WHISPER"], LB["TITAN_PALS_MENU_NORMAL"] ), TitanPanelRightClickMenu_GetDropdownLevel() );
                    table.foreach( tempDB.PALS, function( Key, Data )
                              info = {};
                              info.notCheckable = true;
                              info.text = TitanPals_ColorUtils( "GetCClass", Data.class, Data.toonName );
                              info.func = PalsWhisper;
                              info.value = Data.toonName;
                              TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
                         end
                    )
               end
               -- Battle Net Friends
               if ( TitanPals.Settings.ShowBNTooltip and tempDB.BNO > 0 ) then 
                    if ( tempDB.NPO > 0 ) then TitanPanelRightClickMenu_AddSpacer( TitanPanelRightClickMenu_GetDropdownLevel() ); end
                    TitanPanelRightClickMenu_AddTitle( ( "%s %s" ):format( LB["TITAN_PALS_MENU_WHISPER"], LB["TITAN_PALS_MENU_REALID"] ), TitanPanelRightClickMenu_GetDropdownLevel() );
                    table.foreach( tempDB.BN, function( Key, Data )
                              table.foreach( Data, function( Toon, tData )
                                        if ( type( tData ) == "table" ) then
                                             info = {};
                                             info.notCheckable = true;
                                             info.text = TitanPals_ColorUtils( TitanPals_DataUtils( "Type", tData.client ), tData.class, ( "%s ( %s )" ):format( Key, Toon ) );
                                             info.func = PalsWhisper;
                                             info.value = Key;
                                             TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
                                        end
                                   end
                              )
                         end
                    )
               end
          end
          -- This is for group Invites
          if ( TitanPanelRightClickMenu_GetDropdMenuValue() == LB["TITAN_PALS_MENU_INVITE"] ) then
               -- Normal Friends
               if ( tempDB.NPO > 0 ) then
                    TitanPanelRightClickMenu_AddTitle( ( "%s %s" ):format( LB["TITAN_PALS_MENU_INVITE"], LB["TITAN_PALS_MENU_NORMAL"] ), TitanPanelRightClickMenu_GetDropdownLevel() );
                    table.foreach( tempDB.PALS, function( Key, Data )
                              info = {};
                              info.notCheckable = true;
                              info.text = TitanPals_ColorUtils( "GetCClass", Data.class, Data.toonName );
                              info.func = PalsInvite;
                              info.value = Data.toonName;
                              TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
                         end
                    )
               end
               -- Battle Net Friends
               if ( TitanPals.Settings.ShowBNTooltip and tempDB.BNO > 0 ) then
                    if ( tempDB.NPO > 0 ) then TitanPanelRightClickMenu_AddSpacer( TitanPanelRightClickMenu_GetDropdownLevel() ); end
                    TitanPanelRightClickMenu_AddTitle( ( "%s %s" ):format( LB["TITAN_PALS_MENU_INVITE"], LB["TITAN_PALS_MENU_REALID"] ), TitanPanelRightClickMenu_GetDropdownLevel() );
                    table.foreach( tempDB.BN, function( Key, Data )
                              table.foreach( Data, function( Toon, tData )
                                        if ( type( tData ) == "table" ) then
                                             tooltipDB[Toon] = {};
                                             tooltipDB[Toon]["G"] = {};
                                             info = {};
                                             info.notCheckable = true;
                                             if ( tData.client == BNET_CLIENT_WOW ) then
                                                  local array = {
                                                       [LB["TITAN_PALS_MENU_HORDE"]] = 0,
                                                       [LB["TITAN_PALS_MENU_ALLIANCE"]] = 1,
                                                       [LB["TITAN_PALS_MENU_NEUTRAL"]] = 2,
                                                  }
                                                  tooltipDB[Toon]["G"].faction = TitanPals_ColorUtils( "GetCFText", LB[ ( "TITAN_PALS_MENU_%s" ):format( string.upper( TitanPalsProfile.Faction ) ) ], array[TitanPalsProfile.Faction] );
                                                  if ( tData.faction == TitanPalsProfile.Faction ) then
                                                       info.func = PalsInvite;
                                                       info.value = ( "%s-%s" ):format( Toon, tData.realmName );
                                                  else
                                                       tooltipDB[Toon]["B"] = {};
                                                       tooltipDB[Toon]["B"].faction = TitanPals_ColorUtils( "GetCFText", LB[ ( "TITAN_PALS_MENU_%s" ):format( string.upper( tData.faction ) ) ], array[tData.faction] );
                                                       tooltipDB[Toon]["B"].name = TitanPals_ColorUtils( "GetCFText", ( "%s-%s" ):format( Key, tData.realmName ), array[ tData.faction ] );
                                                       info.func = PalsFactionError;
                                                       info.value = ( "%s:%s:%s" ):format( tooltipDB[Toon]["B"].name, tooltipDB[Toon]["B"].faction, tooltipDB[Toon]["G"].faction );
                                                  end
                                                  info.text = TitanPals_ColorUtils( TitanPals_DataUtils( "Type", tData.client ), tData.class, ( "%s ( %s )" ):format( Key, Toon ) );
                                             else
                                                  info.text = LB["TITAN_PALS_MENU_PLAYING"]:format( ( "%s ( %s )" ):format( Key, Toon ), LB[ ( "TITAN_PALS_MENU_%s" ):format( tData.client ) ] );
                                             end
                                             TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
                                        end
                                   end
                              )
                         end
                    )
               end
          end
          -- This is for setting friends note
          if ( TitanPanelRightClickMenu_GetDropdMenuValue() == LB["TITAN_PALS_MENU_NOTE"] ) then
               -- Normal Friends
               if ( tempDB.NPO > 0 ) then
                    TitanPanelRightClickMenu_AddTitle( ( "%s %s" ):format( LB["TITAN_PALS_MENU_NOTE"], LB["TITAN_PALS_MENU_NORMAL"] ), TitanPanelRightClickMenu_GetDropdownLevel() );
                    table.foreach( tempDB.PALS, function( Key, Data )
                              info = {};
                              info.notCheckable = true;
                              info.text = TitanPals_ColorUtils( "GetCClass", Data.class, Data.toonName );
                              info.func = palsNotePopUp;
                              info.value = ( "Normal:pID:%s:%s" ):format( Data.toonName, TitanPals_ColorUtils( "GetCClass", Data.class, Data.toonName ) );
                              TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
                         end
                    )
               end
               -- Battle Net Frineds
               if ( TitanPals.Settings.ShowBNTooltip and tempDB.BNO > 0 ) then
                    if ( tempDB.NPO > 0 ) then TitanPanelRightClickMenu_AddSpacer( TitanPanelRightClickMenu_GetDropdownLevel() ); end
                    TitanPanelRightClickMenu_AddTitle( ( "%s %s" ):format( LB["TITAN_PALS_MENU_NOTE"], LB["TITAN_PALS_MENU_REALID"] ), TitanPanelRightClickMenu_GetDropdownLevel() );
                    table.foreach( tempDB.BN, function( Key, Data )
                              table.foreach( Data, function( Toon, tData )
                                        if ( type( tData ) == "table" ) then
                                             tooltipDB[Toon] = {};
                                             info = {};
                                             info.notCheckable = true;
                                             if ( tData.client == BNET_CLIENT_WOW ) then
                                                  tooltipDB[Toon].bnClient = tData.class;
                                             elseif ( tData.client == BNET_CLIENT_WOW ) then
                                                  if ( tData.client == BNET_CLIENT_SC2 ) then
                                                       tooltipDB[Toon].bnCllient = BNET_CLIENT_SC2;
                                                  elseif ( tData.client == BNET_CLIENT_D3 ) then
                                                       tooltipDB[Toon].bnClient = BNET_CLIENT_D3;
                                                  elseif ( tData.client == BNET_CLIENT_WTCG ) then
                                                       tooltipDB[Toon].bnClient = BNET_CLIENT_WTCG;
                                                  elseif ( tData.client == BNET_CLIENT_BN ) then
                                                       tooltipDB[Toon] = BNET_CLIENT_BN;
                                                  end
                                             end
                                             info.text = TitanPals_ColorUtils( TitanPals_DataUtils( "Type", tData.client ), tData.class, ( "%s ( %s )" ):format( Key, Toon ) );
                                             info.func = palsNotePopUp;
                                             info.value = ( "BNet:%d:%s:%s" ):format( tempDB.pID[Key], Key, TitanPals_ColorUtils( TitanPals_DataUtils( "Type", tData.client ), tData.class, Key ) );
                                             TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
                                        end
                                   end
                              )
                         end
                    )
               end
          end
          -- This is for removing a friend from friends list
          if ( TitanPanelRightClickMenu_GetDropdMenuValue() == ( "|cffff2020%s|r" ):format( LB["TITAN_PALS_MENU_REMOVE"] ) ) then
               -- Remove Normal Friends
               if ( tempDB.NPO > 0 ) then
                    TitanPanelRightClickMenu_AddTitle( ( "%s %s" ):format( LB["TITAN_PALS_MENU_REMOVE"], LB["TITAN_PALS_MENU_NORMAL"] ), TitanPanelRightClickMenu_GetDropdownLevel() );
                    table.foreach( tempDB.PALS, function( Key, Data )
                              info = {};
                              info.notCheckable = true;
                              info.text = TitanPals_ColorUtils( "GetCClass", Data.class, Data.toonName );
                              info.func = palsRemove;
                              info.value = Data.toonName;
                              TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
                         end
                    )
               end
               -- Remove Battle Net Friends
               if ( TitanPals.Settings.ShowBNTooltip and tempDB.BNO > 0 ) then
                    if ( tempDB.NPO > 0 ) then TitanPanelRightClickMenu_AddSpacer( TitanPanelRightClickMenu_GetDropdownLevel() ); end
                    TitanPanelRightClickMenu_AddTitle( ( "%s %s" ):format( LB["TITAN_PALS_MENU_REMOVE"], LB["TITAN_PALS_MENU_REALID"] ), TitanPanelRightClickMenu_GetDropdownLevel() );
                    table.foreach( tempDB.BN, function( Key, Data )
                              table.foreach( Data, function( Toon, tData )
                                        if ( type( tData ) == "table" ) then
                                             tooltipDB[Toon] = {};
                                             tooltipDB[Toon].name = ( "%s ( %s )" ):format( Key, Toon );
                                             info = {};
                                             if ( tData.client == BNET_CLIENT_WOW ) then
                                                  info.text = TitanPals_ColorUtils( "GetCClass", tData.class, tooltipDB[Toon].name );
                                             elseif ( tData.client == BNET_CLIENT_SC2 or tData.client == BNET_CLIENT_D3 or tData.client == BNET_CLIENT_WTCG or tData.client == BNET_CLIENT_BN ) then
                                                  info.text = TitanPals_ColorUtils( "GetCClient", tData.client, tooltipDB[Toon].name );
                                             end
                                             info.func = PalsBNRemove;
                                             info.value = tempDB.pID[Key];
                                             TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
                                        end
                                   end
                              )
                         end
                    )
               end
          end
	  if ( TitanPanelRightClickMenu_GetDropdMenuValue() == LB["TITAN_PALS_MENU_SYNC"] ) then
	       TitanPanelRightClickMenu_AddTitle( LB["TITAN_PALS_MENU_SYNC"], TitanPanelRightClickMenu_GetDropdownLevel() );
               -- Sync Alts To Friends List Setting Option
               info = {};
               info.notCheckable = true;
               info.text = LB["TITAN_PALS_MENU_SYNCALTS"];
               info.func = TitanPals_SyncAlts;
               TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
               -- Sync Alts To Friends List Setting Option
               info = {};
               info.notCheckable = true;
               info.text = LB["TITAN_PALS_MENU_SYNCALTSFRIENDS"];
               info.func = TitanPals_SyncAltsFriends;
               TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
	  end
	  if ( TitanPanelRightClickMenu_GetDropdMenuValue() == LB["TITAN_PALS_MENU_REMOVEDATA"] ) then
               TitanPanelRightClickMenu_AddTitle( LB["TITAN_PALS_MENU_REMOVEDATA"], TitanPanelRightClickMenu_GetDropdownLevel() );
	       -- Outdated Friends
               if ( tempDB.lCount > 1 ) then
                    info = {};
                    info.notCheckable = true;
                    info.text = LB["TITAN_PALS_MENU_REMOVE_FRIEND"];
                    info.hasArrow = 1;
                    TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
               end
	       -- Outdated Alts
               if ( tempDB.aCount > 1 ) then
                    info = {};
                    info.notCheckable = true;
                    info.text = LB["TITAN_PALS_MENU_REMOVE_ALT"];
                    info.hasArrow = 1;
                    TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
               end
	  end
          return;
     end
     -- End Menu 2
     -- Start Menu 3
     if ( TitanPanelRightClickMenu_GetDropdownLevel() == 3 ) then
	  if ( TitanPanelRightClickMenu_GetDropdMenuValue() == LB["TITAN_PALS_MENU_REMOVE_FRIEND"] and tempDB.lCount > 1 ) then
               TitanPanelRightClickMenu_AddTitle( LB["TITAN_PALS_MENU_REMOVE_FRIENDDATA"], TitanPanelRightClickMenu_GetDropdownLevel() );
               table.foreach( TitanPalsLists[palsDB], function( Key, Data )
                         if ( tempDB.player ~= Key ) then
                              info = {};
                              info.notCheckable = true;
                              info.text = LB["TITAN_PALS_MENU_SUB_REMOVE"]:format( TitanPals_ColorUtils( "GetCClass", Data.Class, Key ) );
                              info.value = ( "removeList:%s" ):format( Key );
                              info.func = TitanPals_MenuProssess;
                              TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
                         end
                    end
               )
	  end
	  if ( TitanPanelRightClickMenu_GetDropdMenuValue() == LB["TITAN_PALS_MENU_REMOVE_ALT"] and tempDB.aCount > 1 ) then
	       TitanPanelRightClickMenu_AddTitle( LB["TITAN_PALS_MENU_REMOVE_ALTDATA"], TitanPanelRightClickMenu_GetDropdownLevel() );
               table.foreach( TitanPalsAlts[palsDB], function( Key, Class )
                         if ( tempDB.player ~= Key ) then
                              info = {};
                              info.notCheckable = true;
                              info.text = LB["TITAN_PALS_MENU_SUB_REMOVE"]:format( TitanPals_ColorUtils( "GetCClass", Class, Key ) );
                              info.value = ( "removeAlt:%s" ):format( Key );
                              info.func = TitanPals_MenuProssess;
                              TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
                         end
                    end
               )
	  end
          return;
     end
     -- End Menu 3
     TitanPanelRightClickMenu_AddTitle( TitanPlugins[ LB["TITAN_PALS_CORE_ID"] ].menuText );
     if ( tempDB.NPO > 0 or tempDB.BNO > 0 and TitanPals.Settings.ShowBNTooltip == true ) then
          info = {};
          info.notCheckable = true;
          info.text = LB["TITAN_PALS_MENU_WHISPER"];
          info.hasArrow = 1;
          TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );            -- create the title for the Whisper submenu
          info = {};
          info.notCheckable = true;
          info.text = LB["TITAN_PALS_MENU_INVITE"];
          info.hasArrow = 1;
          TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );            -- create the title for the Invite submenu
          info = {};
          info.notCheckable = true;
          info.text = LB["TITAN_PALS_MENU_NOTE"];
          info.hasArrow = 1;
          TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );            -- create the title for the Note submenu
          TitanPanelRightClickMenu_AddSpacer();
          info = {};
          info.notCheckable = true;
          info.text = ( ( "|cffff2020%s|r" ):format( LB["TITAN_PALS_MENU_REMOVE"] ) );
          info.hasArrow = 1;
          TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );            -- create the title for the Remove submenu
          TitanPanelRightClickMenu_AddSpacer();
     end
     info = {};
     info.notCheckable = true;
     info.text = LB["TITAN_PALS_MENU_SYNC"];
     info.hasArrow = 1;
     TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );                 -- Syncing Alts / Alts Friends
     if ( tempDB.aCount > 1 or tempDB.lCount > 1 ) then
          info = {};
          info.notCheckable = true;
          info.text = LB["TITAN_PALS_MENU_REMOVEDATA"]
          info.hasArrow = 1;
          TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
     end
     TitanPanelRightClickMenu_AddSpacer();             -- Removing Friends From Database
     info = {};
     info.notCheckable = true;
     info.text = LB["TITAN_PALS_MENU_SETTINGS"];
     info.value = LB["TITAN_PALS_MENU_SETTINGS"];
     info.func = function() TitanPals_InitGUI(); end
     TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
     TitanPanelRightClickMenu_AddSpacer();
     -- Standard Menu
     TitanPanelRightClickMenu_AddControlVars( LB["TITAN_PALS_CORE_ID"] );
     -- TitanPanelRightClickMenu_AddToggleIcon( LB["TITAN_PALS_CORE_ID"] );
     -- TitanPanelRightClickMenu_AddToggleLabelText( LB["TITAN_PALS_CORE_ID"] );
     -- default Titan Panel right-click menu options
     TitanPanelRightClickMenu_AddSpacer();
     -- TitanPanelRightClickMenu_AddCommand( LB["TITAN_PALS_MENU_HIDE"], LB["TITAN_PALS_CORE_ID"], TITAN_PANEL_MENU_FUNC_HIDE );
     -- TitanPanelRightClickMenu_AddSpacer();
     local auth = TitanPals_CoreUtils( "Author", ( "%s@%s" ):format( TitanPalsProfile.Player, TitanPalsProfile.Realm ) )
     if ( auth and TitanPalsDebug.State ) then
          info = {};
          local dText = TitanPals_ColorUtils( "GetCText", "RED", "Debug Mode" );
          TitanPanelRightClickMenu_AddCommand( ( "%s (%s)" ):format( LB["TITAN_PALS_MENU_CLOSE"], dText ), LB["TITAN_PALS_CORE_ID"], function() return; end );
     else
          TitanPanelRightClickMenu_AddCommand( LB["TITAN_PALS_MENU_CLOSE"], LB["TITAN_PALS_CORE_ID"], function() return; end );
     end
end

-- **************************************************************************
-- NAME : palsWhisper()
-- DESC : Opens a wisper with the selected person
-- **************************************************************************
function PalsWhisper( self )
     if ( not ChatFrame1EditBox:IsVisible() ) then
          ChatFrame_OpenChat( ( "/w %s" ):format( self.value ) );
     else
          ChatFrame1EditBox:SetText( ( "/w %s" ):format( self.value ) );
     end
end

-- **************************************************************************
-- NAME : BNpalsWhisper()
-- DESC : Opens a wisper with the selected person
-- **************************************************************************
function BNpalsWhisper( self )
     BNSendWhisper( self.value, "" );
end

-- **************************************************************************
-- NAME : PalsRemove( self )
-- DESC : <research>
-- **************************************************************************
function PalsRemove( self )
     RemoveFriend( self.value );
end

-- **************************************************************************
-- NAME : PalsBNRemove( self )
-- DESC : <research>
-- **************************************************************************
function PalsBNRemove( self )
     BNRemoveFriend( self.value );
end

-- **************************************************************************
-- NAME : PalsInvite()
-- DESC : <research>
-- **************************************************************************
function PalsInvite( self )
     InviteUnit( self.value );
end

-- **************************************************************************
-- NAME : PalsNotePopUp()
-- DESC : <research>
-- **************************************************************************
function palsNotePopUp( self )
     local a, b, c, d = string.match( self.value, "(.*):(.*):(.*):(.*)" );
     local array = {
          Type = a,
          pID = b,
          Player = c,
          PlayerColored = d,
     };
     TitanPanelPals_StaticPopup( array )
end

-- **************************************************************************
-- NAME : PalsFactionError()
-- DESC : <research>
-- **************************************************************************
function PalsFactionError( self )
     local a, b, c = string.match( self.value, "(.*):(.*):(.*)" );
     local msg = LB["TITAN_PALS_FACTION_ERROR"]:format( a, b, c );
     TitanPals_DebugUtils( "error", "PalsFactionError", msg );
end

-- **************************************************************************
-- NAME : PalsGameError()
-- DESC : <research>
-- **************************************************************************
function PalsGameError( self )
     local a, b, c = string.match( self.value, "(.*):(.*):(.*)" );
     local msg = LB["TITAN_PALS_GAME_ERROR"]:format( a, b, c );
     TitanPals_DebugUtils( "error", "PalsGameError", msg );
end

-- **************************************************************************
-- NAME : TitanPals_MenuProssess()
-- DESC : <research>
-- **************************************************************************
function TitanPals_MenuProssess( self )
     --[arg1   ]
     --[Command]
     local cmd = self.value;
     local cmd, name = string.match( cmd, "(.*):(.*)" );
     local palsDB = ( "%s:%s" ):format( TitanPalsProfile.Faction, TitanPalsProfile.Realm );
     if ( cmd == "removeAlt" and name ) then
          print( cmd, name );
          TitanPalsAlts[palsDB][name] = nil;
     elseif ( cmd and name ) then
          print( cmd, name );
          TitanPalsLists[palsDB][name] = nil;
     else
          TitanPals_DebugUtils( "info", "MenuProssess", "Menu Prossessing failed." );
     end
end