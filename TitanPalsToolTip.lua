-- **************************************************************************
--   TitanPalsToolTip.lua
--
--   By: KanadiaN
--        (Lindarena@Laughing Skull)
-- **************************************************************************

local L = LibStub( "AceLocale-3.0" ):GetLocale( "Titan", true );
local LB = LibStub( "AceLocale-3.0" ):GetLocale( "TitanPals", true );

-- **************************************************************************
-- NAME : TitanPanelPalsButton_SetTooltipText()
-- DESC : Build Firends Tooltip for Titan Panel
-- DEBUG : /script TitanPanelPals_SetTooltip();
-- **************************************************************************
function TitanPanelPals_SetTooltip()
     local np, op = C_FriendList.GetNumFriends();
     local tempDB = TitanPals_DataUtils( "Update" );
     local tooltipDB = {};
     local right, left, palsDB;
     palsDB = ( "%s:%s" ):format( TitanPalsProfile.Faction, TitanPalsProfile.Realm );
     -- RealID Friends
     if ( TitanPals.Settings.ShowBNTooltip ) then
          if ( tempDB.BNT == 0 ) then
               if ( TitanPals.Settings.ShowNoRealID ) then
                    GameTooltip:AddLine( LB["TITAN_PALS_TOOLTIP"], HIGHLIGHT_FONT_COLOR.r, HIGHLIGHT_FONT_COLOR.g, HIGHLIGHT_FONT_COLOR.b );
                    GameTooltip:AddLine( LB["TITAN_PALS_TOOLTIP_NOREALID_FRIENDS"] );
               end
          else
               if ( TitanPals.Settings.ShowNoRealID ) then
                    GameTooltip:AddLine( LB["TITAN_PALS_REAL_TOOLTIP"]:format( tempDB.BNO ), HIGHLIGHT_FONT_COLOR.r, HIGHLIGHT_FONT_COLOR.g, HIGHLIGHT_FONT_COLOR.b );
                    if ( tempDB.BNO > 0 ) then
                         table.foreach( tempDB.BN, function( Key, Data )
                                   table.foreach( Data, function( Toon, tData )
                                             if ( type( tData ) == "table" ) then
                                                  tData.accountName = Key;
                                                  tData.realIDName = TitanPals_ToolTipUtils( "BattleNet", TitanPals.Settings.RealIDFormat, tData );
                                                  tData.left, tData.right = TitanPals_ToolTipUtils( "ToolTip", TitanPals.Settings.TFormat, tData );
                                                  GameTooltip:AddDoubleLine( tData.left, tData.right );
                                             end
                                        end
                                   )
                              end
                         )
                    else
                         if ( TitanPals.Settings.ShowNoRealID ) then
                              GameTooltip:AddLine( ( " %s" ):format( LB["TITAN_PALS_TOOLTIP_NOREALPALS"] ) );
                         end
                    end
               end
          end
          GameTooltip:AddLine( "\n" );
     end
     -- For showing online friends
     if ( tempDB.NP == 0 or tempDB.NPO == 0 ) then
          if ( TitanPals.Settings.ShowNoPals ) then
               GameTooltip:AddLine( LB["TITAN_PALS_TOOLTIP"]:format( tempDB.NPO ), HIGHLIGHT_FONT_COLOR.r, HIGHLIGHT_FONT_COLOR.g, HIGHLIGHT_FONT_COLOR.b );
               if ( tempDB.NP == 0 ) then
                    GameTooltip:AddLine( LB["TITAN_PALS_TOOLTIP_EMPTY"] );
               elseif ( tempDB.NPO == 0 ) then
                    GameTooltip:AddLine( LB["TITAN_PALS_TOOLTIP_NOPALS"] );
               end
          end
     else
          GameTooltip:AddLine( LB["TITAN_PALS_TOOLTIP"]:format( tempDB.NPO ), HIGHLIGHT_FONT_COLOR.r, HIGHLIGHT_FONT_COLOR.g, HIGHLIGHT_FONT_COLOR.b );
          table.foreach( tempDB.PALS, function( Key, Data )
                    right, left = TitanPals_ToolTipUtils( "ToolTip", TitanPals.Settings.TFormat, Data );
                    GameTooltip:AddDoubleLine( right, left );
               end
          )
     end
     -- For showing offline friends  
     if ( TitanPals.Settings.ShowOffline ) then
          GameTooltip:AddLine( "\n" );
          GameTooltip:AddLine( TitanPals_ColorUtils( "GetCText", "hlight", LB["TITAN_PALS_TOOLTIP_OFF"] ) );
          local pcount = 0;
          local offLine = {};
          local lIndex = TitanPalsLists[palsDB];
          local aIndex = TitanPalsAlts[palsDB];
          local uName = UnitName( "player" );
          table.foreach( lIndex, function( Key, Data )
                    if ( not tempDB.PALS or not tempDB.PALS[Key] ) then
                         if ( uName ~= Key ) then 
                              if ( not offLine[Key] ) then offLine[Key] = {}; end
                              if ( aIndex[Key] ) then
                                   offLine[Key].isALT = true;
                              else
                                   offLine[Key].isALT = false;
                              end
                              offLine[Key].toonName = Key;
                              offLine[Key].pType = "pals";
                              offLine[Key].client = BNET_CLIENT_WOW
                              offLine[Key].level = lIndex[Key].Level
                              offLine[Key].class = lIndex[Key].Class
                              if ( lIndex[Key].Note == "na" ) then
                                   offLine[Key].note = "";
                              else
                                   offLine[Key].note = lIndex[Key].Note;
                              end
                              local ol = TitanPals_ColorUtils( "GetCText", "gray", LB["TITAN_PALS_TOOLTIP_OFFLINE"] );
                              offLine[Key].zone = ol;
                              right, left = TitanPals_ToolTipUtils( "ToolTip", TitanPals.Settings.TFormat, offLine[Key] );
                              if ( TitanPals.Settings.NoAlts and aIndex[Key] ) then 
                                   -- GameTooltip:AddDoubleLine( ( " %s" ):format( right ), left );
                                   pcount = pcount + 1;
                              else
                                   GameTooltip:AddDoubleLine( ( " %s" ):format( right ), left );
                                   pcount = pcount + 1;
                              end
                         end
                    end
               end
          );
          if ( pcount == 0 ) then GameTooltip:AddLine( LB["TITAN_PALS_TOOLTIP_NO_OFF"] ) end
     end
     -- For showing ignored ppl
     if ( TitanPals.Settings.ShowIgnored ) then
          GameTooltip:AddLine( "\n" );
          GameTooltip:AddLine(TitanPals_ColorUtils( "GetCText", "hlight", LB["TITAN_PALS_TOOLTIP_IGNORE"] ) );
          ni = C_FriendList.GetNumIgnores();
          if ( ni == 0 ) then 
               GameTooltip:AddLine( LB["TITAN_PALS_TOOLTIP_IGNORE_EMPTY"] );
          else
               for ofI=1, ni do
                    local tpit = TitanPals_ColorUtils( "GetCText", "red", LB["TITAN_PALS_TOOLTIP_OFFLINE_IGNORED"] );
                    GameTooltip:AddDoubleLine( ( "  %s" ):format( TitanPals_ColorUtils( "GetCText", "normal", GetIgnoreName( ofI ) ) ), tpit );
               end
          end
     end
     -- For showing AddOn memory Usage
     if ( TitanPals.Settings.ShowMem ) then
          GameTooltip:AddLine( "\n" );
          GameTooltip:AddLine( TitanPals_ColorUtils( "GetCText", "hlight", LB["TITAN_PALS_TOOLTIP_MEM"] ) );
	  GameTooltip:AddDoubleLine( ( "  %s" ):format( LB["TITAN_PALS_ADDON_LABEL"] ), TitanPals_ColorUtils( "GetCText", "green", ( "%s kb" ):format( floor( GetAddOnMemoryUsage( "TitanPals" ) ) ) ) )
     end
     if ( not TitanPals.Settings.ShowNoPals and not TitanPals.Settings.ShowNoRealID and not TitanPals.Settings.ShowOffline and not TitanPals.Settings.ShowIgnored and not TitanPals.Settings.ShowMem ) then
          GameTooltip:AddLine( LB["TITAN_PALS_ADDON_LABEL"], HIGHLIGHT_FONT_COLOR.r, HIGHLIGHT_FONT_COLOR.g, HIGHLIGHT_FONT_COLOR.b );
          GameTooltip:AddLine( "Nothing to see here, Move along!" );
     end
     if ( IsShiftKeyDown() and TitanPalsDebug.State ) then
          local iHere = "|TInterface\\ChatFrame\\UI-ChatIcon-ArmoryChat:0:0:0:0:16:16:0:16:0:16:73:177:73|t |cffffffff%s|r";
          local iBusy = "|TInterface\\ChatFrame\\UI-ChatIcon-ArmoryChat-BusyMobile:0:0:0:0:16:16:0:16:0:16|t |cffffffff%s|r";
          local iAway = "|TInterface\\ChatFrame\\UI-ChatIcon-ArmoryChat-AwayMobile:0:0:0:0:16:16:0:16:0:16|t |cffffffff%s|r";

          local iHorde = "|TInterface\\TargetingFrame\\UI-PVP-Horde:16:16:0:0:64:64:0:40:0:40|t |cffffffff%s|r";
          local iAlliance = "|TInterface\\TargetingFrame\\UI-PVP-Alliance:16:16:0:0:64:64:0:40:0:40|t |cffffffff%s|r";
          local iNeutral = "|TInterface\\TargetingFrame\\UI-PVP-FFA:16:16:0:0:64:64:0:40:0:40|t |cffffffff%s|r";

          local tga = "|T%s.tga:16:16:0:0|t";
          local tgaT = "|T%s.tga:16:16:0:0|t |cffffffff%s|r";

          GameTooltip:AddLine( iHere:format( "iHere" ) );
          GameTooltip:AddLine( iBusy:format( "iBusy" ) );
          GameTooltip:AddLine( iAway:format( "iAway" ) );
          GameTooltip:AddLine( "\n" );
          GameTooltip:AddLine( tga:format( FRIENDS_TEXTURE_OFFLINE ) );
          GameTooltip:AddLine( tga:format( FRIENDS_TEXTURE_ONLINE ) );
          GameTooltip:AddLine( tga:format( FRIENDS_TEXTURE_AFK ) );
          GameTooltip:AddLine( tga:format( FRIENDS_TEXTURE_DND ) );
          GameTooltip:AddLine( "\n" );
          GameTooltip:AddLine( tgaT:format( FRIENDS_TEXTURE_OFFLINE, "Offline" ) );
          GameTooltip:AddLine( tgaT:format( FRIENDS_TEXTURE_ONLINE, "Online" ) );
          GameTooltip:AddLine( tgaT:format( FRIENDS_TEXTURE_AFK, "AFK" ) );
          GameTooltip:AddLine( tgaT:format( FRIENDS_TEXTURE_DND, "DND" ) );
          GameTooltip:AddLine( "\n" );
          GameTooltip:AddLine( iHorde:format( "Horde" ) );
          GameTooltip:AddLine( iAlliance:format( "Aliance" ) );
          GameTooltip:AddLine( iNeutral:format( "Neutral" ) );
     end
end