## Interface: 100002
## Title: Titan Panel [|cffeda55fPals|r] |cff00aa006.00.09.100002|r
## Notes: Friends Managment Plugin for Titan Panel
## Authors: KanadiaN
## SavedVariables: TitanPals, TitanPalsInit, TitanPalsStaticArrays, TitanPalsLists, TitanPalsAlts, TitanPalsLocals, TitanPalsDebug, TitanPalsProfile
## OptionalDeps: 
## Dependencies: Titan
## Version: 120
## X-Child-of: Titan
## X-Compatible-With: 100002
## X-Translation: Sandmahn, Rainheart
## X-Category: Interface Enhancements
## X-Website: https://www.curseforge.com/wow/addons/titanpals
## X-Email: bug@kanadian.net

Locals\Locals.xml

TitanPalsConfig.lua
TitanPals.lua
TitanPalsToolTip.lua
TitanPalsRightClick.lua
TitanPalsUtils.lua

TitanPals.xml
